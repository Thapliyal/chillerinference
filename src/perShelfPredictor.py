from shelfPredictor import ShelfPredictor, PredictedShelf, PredictedChiller
from segmentationInference import BottlePredictor
from classificationInference import ClassificationPredictor
from lensCorrector import LensCorrector
from pathlib import Path
import torch
import numpy as np
import cv2
from PIL import Image
from torchvision.transforms import v2
from utils import load_image, pad_pil, pad_numpy, get_sub_image_from_points
from loguru import logger
from transforms import VALIDATION_TRANSFORMS
import pandas as pd
import ast
from utils import Timer
torch.set_float32_matmul_precision('high')


class BrandPredictor:
    def __init__(self, model_dir):
        self.model_dir = Path(model_dir)
        
        self.pet_classifier_path = self.model_dir / 'PETClassification.pth'
        self.can_classifier_path = self.model_dir / 'CanClassification.pth'
        self.rgb_classifier_path = self.model_dir / 'RGBClassification.pth'
        self.tetra_classifier_path = self.model_dir / 'TetraClassification.pth'
        
        self.pet_classifier = ClassificationPredictor(self.pet_classifier_path)
        self.can_classifier = ClassificationPredictor(self.can_classifier_path)
        self.rgb_classifier = ClassificationPredictor(self.rgb_classifier_path)
        self.tetra_classifier = ClassificationPredictor(self.tetra_classifier_path)

        self.ot2model = {}
        self.ot2model['pet'] = self.pet_classifier
        self.ot2model['can'] = self.can_classifier
        self.ot2model['rgb'] = self.rgb_classifier
        self.ot2model['tetra'] = self.tetra_classifier


    def predict(self, image, object_type, debug=False):
        if object_type == 'pet':
            model = self.pet_classifier
        elif object_type == 'can':
            model = self.can_classifier
        elif object_type == 'rgb':
            model = self.rgb_classifier
        elif object_type == 'tetra':
            model = self.tetra_classifier
        else:
            if object_type in ['object']:
                return object_type, 1
            raise ValueError(f"Invalid type: {object_type}")
        category, conf, _ = model.predict(image)
        return category, conf
    
    def predict_batch(self, images, object_types, debug=False, log_timer=True):
        if log_timer:
            t = Timer("BrandPredictor.predict_batch()")
            t.start()
        ot_items_map = {} # it will be ordered since we use python > 3.7
        ot_order_map = {}

        for i, (object_type, image) in enumerate(zip(object_types, images)):
            if object_type not in ot_items_map:
                ot_items_map[object_type] = []
                ot_order_map[object_type] = []
            ot_items_map[object_type].append(image)
            ot_order_map[object_type].append(i)

        idx2results = {}
        for object_type in ot_items_map:
            if object_type in self.ot2model.keys():
                model: ClassificationPredictor = self.ot2model[object_type]
                categories, confidences, __ = model.predict_batch(ot_items_map[object_type])
                for category, confidence, idx in zip(categories, confidences, ot_order_map[object_type] ):
                    idx2results[idx] = (category, confidence)
            else:
                for idx in ot_order_map[object_type]:
                    idx2results[idx] = (object_type, 1)

        
        brand_labels = []
        brand_confidences = []
        for i in range(len(images)):
            category, confidence = idx2results[i]
            brand_labels.append(category)
            brand_confidences.append(confidence)

        if log_timer:
            t.stop()
        return brand_labels, brand_confidences
        

class PerShelfChillerPredictor:
    def __init__(self, shelf_model_path, bottle_model_path, brand_models_dir, volume_model_path, shelf_clip_index=3, 
                 bottle_model_threshold=0.98, save_images_flag=False, fixed_width_transform=False, debug=False):
        if shelf_model_path is None:
            self.shelf_model_path = Path('/media/akshay/datasets/coke/models/old/shelf2024-March28-01/shelf2024-output/model_final.pth')
        else:
            self.shelf_model_path = Path(shelf_model_path)

        self.shelf_predictor = ShelfPredictor(model_path=self.shelf_model_path, clip_index=shelf_clip_index, sort_fn=lambda x: x.min_y, 
                                              fixed_width_transform=fixed_width_transform, debug=debug)

        if bottle_model_path is None:
            self.bottle_model_path = Path('/media/akshay/datasets/coke/models/perShelf-April01/perShelfBottle-output/model_final.pth')
        else:
            self.bottle_model_path = Path(bottle_model_path)
        self.bottle_predictor = BottlePredictor(model_path=self.bottle_model_path, threshold=bottle_model_threshold, debug=debug)

        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.brand_predictor = BrandPredictor(model_dir=brand_models_dir)

        # volume_categories = ['0125', '0135', '0150', '0200', '0250', '0300', '0350', '0400', '0500', '0600', '0750', '1000', '2250']
        self.volume_predictor = ClassificationPredictor(model_path=volume_model_path)

        # self.transform = v2.Compose([
        #         v2.Resize((224, 224)),
        #         v2.ToTensor()
        #     ])
        self.lens_corrector = LensCorrector()
        self.THRESHOLD = 0.4
        self.save_images_flag = save_images_flag
        self.debug = debug
        self.volume_catalog = self._load_volume_catalog()
    
    def _load_volume_catalog(self):
        file_path = Path(__file__).parent.parent/'data'/'volume_catalog.csv'
        frame = pd.read_csv(file_path)
        frame['valid_volumes'] = frame['valid_volumes'].apply(ast.literal_eval)
        return frame

    def to_prodigy(self):
        r = {}
        r['height'] = self.transformed_height
        r['width'] = self.transformed_width
        r['image'] = self.sub_image_path.replace('/media/akshay/datasets', 'http://localhost:8000/datasets')
        r['path'] = self.sub_image_path
        spans = []
        for bottle in self.bottles:
            spans.append(bottle.to_prodigy())
        r['spans'] = spans
        r['answer'] = 'accept'
        return r

    def _get_shelf_output_image(self, orig_image, r_shelves: list[PredictedShelf]):
        shelf_output_image = orig_image.copy()
        shelf_output_image = cv2.cvtColor(shelf_output_image, cv2.COLOR_BGR2RGB)
        for shelf in r_shelves:
            points = [c for c in shelf.corners]
            if self.debug:
                logger.debug(f"{shelf.label}: points: {points}")
            points.append(points[0])
            for p1, p2 in zip(points[:-1], points[1:]):
                cv2.line(shelf_output_image, p1, p2, (0, 255, 0), thickness=3)
                cv2.circle(shelf_output_image, p1, 5, (0, 0, 255), thickness=-1)

        return shelf_output_image
    
    def _correct_volume_labels(self, volume_labels, sku_types, brand_labels, shelf: PredictedShelf, bottle_shelf_height_ratios):
        corrected_volume_labels = []
        bottle_heights = []
        for volume_label, sku_type, brand_label, bottle_ratio in zip(volume_labels, sku_types, brand_labels, bottle_shelf_height_ratios):
            if brand_label in ['pet', 'can', 'rgb', 'tetra']:
                volume_label = ''
                corrected_volume_labels.append(volume_label)
                continue
            bottle_height = bottle_ratio * shelf.shelf_height
            bottle_heights.append(bottle_height)
            if sku_type == 'pet':
                if bottle_height < 17:
                    volume_label = '250'
                elif bottle_height < 26:
                    volume_label = '600'
                else:
                    volume_label = '1500'
            elif sku_type == 'can':
                if volume_label not in ['300', '350']:
                    if brand_label == 'Monster':
                        volume_label = '350'
                    else:
                        volume_label = '300'
            elif sku_type == 'rgb':
                if volume_label != '200':
                    volume_label = '200'
            elif sku_type == 'tetra':
                if volume_label != '135':
                    volume_label = '135'

            elif sku_type == 'object' or sku_type == 'grpobject':
                pass

            else:
                raise ValueError(f"Invalid sku_type: {sku_type}")
            
            logger.debug(f"volumeCheck inputs: shelf_height: {shelf.shelf_height} bottle_shelf_ratio: {bottle_ratio:.2f} sku type: {sku_type} volume: {volume_label}")
            corrected_volume_labels.append(volume_label)
        bottle_height_str = "[" + ", ".join([f"{h:.2f}" for h in bottle_heights]) + "]"
        logger.debug(f"bottle_heights: {bottle_height_str}")
        logger.debug(f"corrected volume_labels: {corrected_volume_labels} predicted: {volume_labels}")
        return corrected_volume_labels
    
    def _correct_volumes_using_catalog(self, volume_labels, sku_types, brand_labels, shelf: PredictedShelf, bottle_shelf_height_ratios):
        corrected_volume_labels = []
        for volume_label, sku_type, brand_label, bottle_ratio in zip(volume_labels, sku_types, brand_labels, bottle_shelf_height_ratios):
            if brand_label in ['pet', 'can', 'rgb', 'tetra']:
                volume_label = ''
                corrected_volume_labels.append(volume_label)
                continue
            volume_label = self._get_correct_volume_from_catalog(sku_type=sku_type, brand_label=brand_label, volume_label=volume_label)
            corrected_volume_labels.append(volume_label)
        logger.debug(f"catalog corrected volume_labels: {corrected_volume_labels} orig: {volume_labels}")
        return corrected_volume_labels
    
    def _get_correct_volume_from_catalog(self, sku_type, brand_label, volume_label):
        df = self.volume_catalog
        result = df.loc[(df['sku_type'] == sku_type) & (df['brand_label'] == brand_label), 'valid_volumes']
        if not result.empty:
            valid_volumes = result.values[0]
        else:
            valid_volumes = None  

        if valid_volumes:
            volume = int(volume_label)
            if volume not in valid_volumes:
                volume = self._pick_closest_volume(volume, valid_volumes)
                volume_label = f"{volume:04d}"
        return volume_label
    
    def _pick_closest_volume(self, curr_volume, valid_volumes):
        distances = [abs(v - curr_volume) for v in valid_volumes]
        index = distances.index(min(distances))
        return valid_volumes[index]

    def _predict_brand_and_volume(self, shelf, detections, image_path):
        sorted_detections = sorted(detections, key=lambda d: d.bbox.x1)
        brand_labels = []
        brand_confidences = []
        volume_labels = []
        volume_confidences = []
        bottle_shelf_height_ratios = []

        sku_types = [d.label for d in sorted_detections]
        sub_images = []
        for i, d in enumerate(sorted_detections):
            sub = get_sub_image_from_points(image=shelf.output_image, points=d.polygon)
            bottle_shelf_height_ratio = sub.shape[0] / shelf.output_image.shape[0]
            bottle_shelf_height_ratios.append(bottle_shelf_height_ratio)
            # sub images are already pil_pad/numpy_pad
            if self.save_images_flag:
                self.save_images(sub, image_path, shelf.label, i)
            sub_images.append(sub)
        #     category, conf = self.brand_predictor.predict(sub, d.label)
        #     brand_labels.append(category)
        #     brand_confidences.append(conf)
        #     if self.debug:
        #         logger.debug(f"{d.label:6s}: {category:10s}: {conf:0.2f}")

        #     vol_category, vol_conf, _ = self.volume_predictor.predict(sub)
        #     volume_labels.append(vol_category)
        #     volume_confidences.append(vol_conf)

        brand_labels, brand_confidences = self.brand_predictor.predict_batch(sub_images, sku_types, debug=self.debug)

        volume_labels, volume_confidences, __ = self.volume_predictor.predict_batch(sub_images, debug=self.debug)
        volume_labels = self._correct_volume_labels(volume_labels, sku_types, brand_labels, shelf, bottle_shelf_height_ratios)
        volume_labels = self._correct_volumes_using_catalog(volume_labels, sku_types, brand_labels, shelf, bottle_shelf_height_ratios)

        brand_str = "[" + ", ".join([f"{b:<8s}" for b in brand_labels]) + "]"
        logger.debug(f"predicted brand_labels     : {brand_str}")
        conf_str = "[" + ", ".join([f"{c:<8.2f}" for c in brand_confidences]) + "]"
        logger.debug(f"predicted brand confidences: {conf_str}")

        filtered_brand_labels, filtered_confidences = [], []
        for detection, brand_label, brand_conf in zip(sorted_detections, brand_labels, brand_confidences):
            if brand_conf < self.THRESHOLD:
                filtered_brand_labels.append(detection.label.upper())
                filtered_confidences.append(1)
            else:
                filtered_brand_labels.append(brand_label)
                filtered_confidences.append(brand_conf)

        filtered_conf_string = "[" + ", ".join([f"{c:0.2f}" for c in filtered_confidences]) + "]"
        vol_conf_string = "[" + ", ".join([f"{c:0.2f}" for c in volume_confidences]) + "]"

        logger.info(f"filtered brand_labels: {brand_labels}")
        logger.info(f"filtered confidences : {filtered_conf_string}")
        logger.info(f"predicted volume_labels     : {volume_labels}") 
        logger.info(f"predicted volume confidences: {vol_conf_string}")

        return filtered_brand_labels, volume_labels, sku_types, brand_confidences
    
    def save_images(self, sub_image, image_path, shelf_label, i):
        output_dir = Path(__file__).parent.parent / "data" / "sub_images"
        if not output_dir.is_dir():
            output_dir.mkdir(parents=True)

        output_path = output_dir / image_path / f"{shelf_label}" / f"{i:02d}.jpg"
        if not output_path.parent.is_dir():
            output_path.parent.mkdir(parents=True)
        # sub_image = pad_numpy(sub_image)
        bgr = cv2.cvtColor(sub_image, cv2.COLOR_RGB2BGR)
        cv2.imwrite(str(output_path), bgr)

    def _populate_bottles(self, shelf, image_path, video_dir, frame_name):
        logger.debug(f"-"*160)
        logger.debug(f"Shelf: {shelf.label}")
        bgr_shelf_output_image = cv2.cvtColor(shelf.output_image, cv2.COLOR_RGB2BGR)
        
        items, output_image = self.bottle_predictor.predict(bgr_shelf_output_image, overlap_margin=0.5, output_image=True, sort_fn=lambda x: x.bbox.x1)
        
        # bottles = [item for item in items if item.label == 'bottle']
        if len(items) > 0:
            bottles = items
            # brand_labels = self._predict(self.brand_classifier, brand_label2id, shelf, bottles, is_sales=False)
            if video_dir is not None and frame_name is not None:
                frame_path = Path(video_dir) / frame_name
            else:
                frame_path = image_path
            brand_labels, volume_labels, sku_types, brand_confidences = self._predict_brand_and_volume(shelf, items, frame_path)
            # volume_labels = self._predict(self.volume_classifier, volume_label2id, shelf, bottles, is_sales=False)
            # sales_labels = self._predict(self.sales_model_classifier, sales_label2id, shelf, bottles, is_sales=True)

            for i, (brand_label, volume_label, bottle, sku_type, brand_conf) in enumerate(zip(brand_labels, volume_labels, bottles, sku_types, brand_confidences)):
                bottle.brand = brand_label
                bottle.sku_type = sku_type
                bottle.brand_confidence = brand_conf
                if brand_label.upper() in ["CAN", "RGB", "TETRA", "PET"]:
                    bottle.volume = ""
                    bottle.label = f"{bottle.brand}"
                else:
                    bottle.volume = volume_label
                    bottle.label = f"{bottle.brand}-{bottle.volume:04}"
                # bottle.row = sales_label
                bottle.update_code()
            shelf.bottles = bottles
            
            shelf.facings = [bottle.label for bottle in shelf.bottles]
            o_height, o_width = shelf.output_image.shape[:2]
            output_image = cv2.rectangle(np.array(output_image), (0, 0), (o_width, o_height), (0, 0, 0), thickness=6)
            shelf.bottle_image = Image.fromarray(output_image)

            logger.opt(colors=True).info(f"{shelf.label}: {shelf.facings} <red> bottle_image: {shelf.bottle_image.size} </red>")
            return shelf
        else:
            logger.debug(f"{shelf.label}: No bottles found")
            shelf.bottles = []
            shelf.facings = []
            o_height, o_width = shelf.output_image.shape[:2]
            output_image = cv2.rectangle(np.array(output_image), (0, 0), (o_width, o_height), (0, 0, 0), thickness=6)
            shelf.bottle_image = output_image
            return shelf

    def predict(self, image_path, shelf_heights, shelf_widths, video_dir=None, frame_name=None):
        t = Timer("perShelfPredictor.predict")
        t.start()
        image = load_image(image_path, bgr=True)
        # This is an RGB image

        # image, _, _ = self.lens_corrector.correct(image)
        orig_image, _, _ = self.lens_corrector.correct(image)
        shelves, shelf_output_image = self.shelf_predictor.predict(image, overlap_margin=0.5, shelf_heights=shelf_heights, shelf_widths=shelf_widths)
        r_shelves = []
        shelf: PredictedShelf
        for shelf in shelves:
            r_shelf = self._populate_bottles(shelf, image_path, video_dir, frame_name)
            r_shelves.append(r_shelf)
        new_shelf_output_image = self._get_shelf_output_image(orig_image, r_shelves)
        chiller = PredictedChiller(r_shelves, shelf_output_image=new_shelf_output_image, image_path=image_path)
        if self.debug:
            shelf_output_images = [shelf.output_image for shelf in r_shelves]
            chiller_input_image = np.concatenate(shelf_output_images, axis=0)
            chiller_input_image = cv2.cvtColor(chiller_input_image, cv2.COLOR_RGB2BGR)
            chiller_input_image_path = Path(__file__).parent.parent / "data" / f"chiller_input_image.jpg"
            cv2.imwrite(str(chiller_input_image_path), chiller_input_image)
            logger.opt(colors=True).debug(f"<red> output_image shape: {chiller.output_image.shape} </red>")
            output_image_path = Path(__file__).parent.parent / "data" / f"chiller_output_image.jpg"
            cv2.imwrite(str(output_image_path), cv2.cvtColor(chiller.output_image, cv2.COLOR_RGB2BGR))
        
        t.stop()
        return chiller
