#! /usr/bin/env python3

from pathlib import Path
from accuracy import Prodigy2Chiller
from tqdm.auto import tqdm
from loguru import logger
import sys
from utils import get_number_of_lines
import gc
from argparse import ArgumentParser


def main():
    parser = ArgumentParser()
    parser.add_argument("-S", "--start-index", help="Start index", type=int, default=None)
    parser.add_argument("-r", "--resume", help="Resume saving subimages from existing files", action="store_true", default=False)
    parser.add_argument("-b", "--bottle-jsonls", help="Bottle jsonl files", nargs='+', default=None)
    parser.add_argument("-s", "--shelf-jsonls", help="Shelf jsonl files", nargs='+', default=None)
    parser.add_argument("-i", "--image-dir", help="Image directory to save subimages", type=str, default=None)
    parser.add_argument("-u", "--undistort", help="Undistort images", action="store_true", default=False)
    args = parser.parse_args()
    start_i = args.start_index
    if start_i is None:
        start_i = 0

    if args.bottle_jsonls is None or args.shelf_jsonls is None or args.image_dir is None:
        if args.bottle_jsonls or args.shelf_jsonls or args.image_dir:
            logger.error("All arguments (bottle-jsonls, shelf-jsonls, image-dir) are required")
            sys.exit(1)

        data_dir = Path(__file__).parent.parent / 'data' 
        jsonl_dir = data_dir / 'accuracy' / 'datasets' / 'Nov-21-accuracy'
        bottle_jsonls = sorted(list(jsonl_dir.glob("*bottle*")))
        shelf_jsonls = sorted(list(jsonl_dir.glob("*shelf*")))
        image_dir = data_dir / 'accuracy' / 'subimages' / 'Nov-21-accuracy'
    else:
        bottle_jsonls = [Path(jsonl) for jsonl in args.bottle_jsonls]   
        shelf_jsonls = [Path(jsonl) for jsonl in args.shelf_jsonls]
        image_dir = Path(args.image_dir)

    num_lines = get_number_of_lines(bottle_jsonls)
    gt_maker = Prodigy2Chiller(undistort=args.undistort)

    if not image_dir.is_dir():
        image_dir.mkdir(parents=True)

    if args.resume:
        existing_files = list(Path(image_dir).rglob("*.jpg"))
        existing_files = [Path(file).stem for file in existing_files]
    else:
        existing_files = None

    logger.info(f"Processing [{num_lines}] GT chiller images and saving to {image_dir}")
    logger.remove()
    logger.add(sys.stdout, level="ERROR")

    count = 0
    sub_image_count = 0
    for gt_i, gt_chiller in tqdm(enumerate(gt_maker.yield_gt_chillers(bottle_jsonls=bottle_jsonls, shelf_jsonls=shelf_jsonls, 
                                                                      start_i=start_i, skip_files=existing_files)), 
                                                                      total=num_lines):
        if gt_chiller is None:
            continue
        prefix = Path(gt_chiller.image_path).stem
        sub_image_count += gt_chiller.save_bottle_sub_images(image_dir, prefix=prefix, gt=True)
        count += 1
        if count % 100 == 0:
            gc.collect()
    gc.collect()
    logger.remove()
    logger.add(sys.stdout, level="DEBUG")
    logger.info(f"Successfully processed [{count}/{num_lines}] lines, extracted sub images: {sub_image_count}")

if __name__ == "__main__":
    main()

