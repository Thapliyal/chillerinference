import streamlit as st
import requests
import json
import base64
import io
from PIL import Image, ImageDraw
import numpy as np
from pathlib import Path
import cv2
import pandas as pd
from pydantic import BaseModel
import re
# from frameSelector.classificationInference import ClassificationPredictor
from shelfPredictor import ShelfPredictor, PredictedShelf
from commonUI import ItemNavigator
from loguru import logger
from shelfSettings import StoreManager, WESTERN_AFTER_MAY_15

STORE_MAPPER_WITHOUT_EXCEPTIONS = StoreManager(raise_exception=False)
# st.set_page_config(page_title="FrameSelection", layout="wide")
st.set_page_config(page_title="ShelfPredictor", layout="centered")


@st.cache_resource
def get_model():
    curr_dir = Path(__file__).parent.parent
    model_path = curr_dir / "models" / "shelf_model" / "model" / "shelf_model.pth"
    model = ShelfPredictor(model_path, clip_index=5, threshold=0.5)
    return model


@st.cache_resource
def get_logger():

    log_file = LOG_DIR / "frameSelectionUI.log"
    logger.add(
        log_file,
        format="{time}|{file}|{line}|{thread}|{level}|{message}",
        colorize=True,
    )
    return logger


####################################################################################################################
# Initializations
####################################################################################################################

BASE_DIR = Path(__file__).parent.parent
LOG_DIR = BASE_DIR / "logs"
DATA_DIR = BASE_DIR / "data"
for dir in [DATA_DIR, LOG_DIR]:
    if not dir.exists():
        dir.mkdir()

logger = get_logger()
predictor = get_model()

def process_image(input_image, item):
    placeholder = st.empty()
    shelf_setting = STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(item)
    if shelf_setting is None:
        shelf_setting = WESTERN_AFTER_MAY_15
    shelves, output_image = predictor.predict(input_image, shelf_heights=shelf_setting.shelf_heights, shelf_widths=shelf_setting.shelf_widths)
    
    shelf: PredictedShelf
    for shelf in shelves:
        draw = ImageDraw.Draw(output_image)
        corners = [(x, y) for x, y in shelf.corners]
        draw.polygon(corners, outline=(255, 0, 0), width=4)
        angle1, angle2 = shelf.get_shelf_angles()
        w1, w2 = shelf.get_shelf_widths()
        st.write(f"{shelf.label}: angle[{angle1:0.2f}: {angle2:0.2f}] width[{w1:0.2f}: {w2:0.2f}]")
    st.image(output_image, use_container_width=True)
    st.markdown("---")


base_dataset_dir = Path("/home/akshay/workspace/labelling/coke/datasets/200DLens/distorted")
dataset_dirs = sorted([d for d in base_dataset_dir.iterdir() if d.is_dir() and d.name[0] != "."])
logger.debug(f"dataset_dirs: {dataset_dirs}")
dataset_dirs = [d for d in dataset_dirs if len(d.stem) > 5 and d.stem[:5] in ["Set23", "Set25"]]

dataset_dir = st.selectbox("Choose a dataset", dataset_dirs)

batch_dirs = sorted([s for s in dataset_dir.iterdir() if s.is_dir() and s.name[0] != "."])
batch_dir = st.selectbox("Choose a batch", batch_dirs)

widget = ItemNavigator(batch_dir, display_image_callback=process_image)
widget.displayUI()

