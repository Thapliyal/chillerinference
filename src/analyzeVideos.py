#!/usr/bin/env python3

from pathlib import Path
from tqdm.auto import tqdm
import cv2
from PIL import Image
from classificationInference import VideoClassification, ClassificationPredictor, StatusErrors, VideoDecodeError
from shelfSettings import StoreManager
import pandas as pd
from loguru import logger
from argparse import ArgumentParser
import os
from ast import literal_eval
import numpy as np
from datetime import datetime
from tqdm.auto import tqdm
from functools import partial
from utils import Timer, get_recent_files_fast, UnableToOpenFile, get_filtered_files
import re
import configparser
import gc
import torch

torch.set_float32_matmul_precision('high')

STORE_MAPPER = StoreManager()
STORE_MAPPER_WITHOUT_EXCEPTIONS = StoreManager(raise_exception=False)

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('-s', '--stores', help='Name of the store(s) to analyze', required=False, nargs='+')
    parser.add_argument('-o', '--output-dir', help='Directory to save images', required=False, default=None)
    parser.add_argument('-f', '--from-date', help='Start date to analyze', required=False, default=None)
    parser.add_argument('-t', '--to-date', help='End date to analyze', required=False, default=None)
    return parser.parse_args()

CLOSED_DOOR = "closedDoor"
GLARE_OR_FOG = "glareOrFog"
NOT_OPEN_ENOUGH = "notOpenEnough"
TOO_OBSTRUCTED = "tooObstructed"

ALL_SKUS_CLEAR = "allSKUsClear"
TOP_3_CLEAR = "top3Clear"
OPEN_TOO_WIDE = "openTooWide"

OPEN_BUT_NOT_TOP3_CLEAR = "openButNotTop3Clear"

ALL_FRAMESELECTOR_CATEGORIES = [CLOSED_DOOR, NOT_OPEN_ENOUGH, OPEN_TOO_WIDE, GLARE_OR_FOG, TOO_OBSTRUCTED, TOP_3_CLEAR, ALL_SKUS_CLEAR]
ALL_CATEGORIES = [CLOSED_DOOR, NOT_OPEN_ENOUGH, OPEN_TOO_WIDE, GLARE_OR_FOG, TOO_OBSTRUCTED, TOP_3_CLEAR, ALL_SKUS_CLEAR]
OPEN_DOOR_CATEGORIES = [TOP_3_CLEAR, GLARE_OR_FOG, TOO_OBSTRUCTED, NOT_OPEN_ENOUGH, OPEN_TOO_WIDE, OPEN_BUT_NOT_TOP3_CLEAR]
FIRST_FRAME_RESTOCKING_CATEGORIES = [GLARE_OR_FOG, TOO_OBSTRUCTED, TOP_3_CLEAR, OPEN_TOO_WIDE, ALL_SKUS_CLEAR]
#
# There is no point keeping categories like NOT_OPEN_ENOUGH and TOO_OBSTRUCTED in VIDEO_SAMPLING_CATEGORIES 
# Only keep categories that make sense assuming the frameSelection works perfectly
#
VIDEO_SAMPLING_CATEGORIES = [ALL_SKUS_CLEAR, TOP_3_CLEAR, OPEN_BUT_NOT_TOP3_CLEAR]
ALL_VIDEO_CATEGORIES = [CLOSED_DOOR, NOT_OPEN_ENOUGH, GLARE_OR_FOG, TOO_OBSTRUCTED, TOP_3_CLEAR, OPEN_BUT_NOT_TOP3_CLEAR]

class VideoManager:
    def __init__(self, site_dir=None, parquet_stats_frame_path=None):
        if site_dir is None:
            data_dir = Path(__file__).parent.parent/'data'
            if Path("/home/akshay/datasets").is_dir():
                site_dir = Path("/home/akshay/siteData")
            else:
                site_dir = Path("/media/akshay/datasets/coke/siteData")
                if not site_dir.is_dir():
                    site_dir = data_dir
        self.site_dir = Path(site_dir)
        if parquet_stats_frame_path is None:
            self.analysis_frame_path = site_dir/ 'analysis' / f'videoStats__{site_dir.name}.parquet'
        else:
            self.analysis_frame_path = Path(parquet_stats_frame_path)
        if not self.analysis_frame_path.is_file():
            raise Exception(f"Unable to open: {self.analysis_frame_path}")
        self.frame, self.mod_time = self.load_frame()
        
    def load_frame(self):
        mod_time = self.analysis_frame_path.stat().st_mtime
        frame = pd.read_parquet(self.analysis_frame_path)
        logger.info(f"Loading video analysis frame: {self.analysis_frame_path} records: {len(frame)} last modified: {datetime.fromtimestamp(mod_time)}")
        return frame, mod_time
    
    def reload_frame(self):
        self.frame, self.mod_time = self.load_frame()
    
    def _has_correct_categories(self, correct_categories, categories):
        if categories is None:
            return False
        for correct_category in correct_categories:
            if correct_category in categories:
                return True
        return False

    def get_filtered_frame(self, from_date=None, to_date=None, site_asset_names=None, region_filter=None, remove_restocking=True, 
                           video_status_categories=None, allowed_categories=None):
        
        filtered = self.frame
        #
        # Filtering based on from and to dates
        #
        before_size = len(filtered)
        if from_date and to_date:
            filtered = filtered[filtered.index >= from_date]
            filtered = filtered[filtered.index <= to_date]
            logger.opt(colors=True).info(f"<green>Filtering from: {from_date} to: {to_date}. size before: {before_size} size after: {len(filtered)}</green>")
        elif from_date and not to_date:
            filtered = filtered[filtered.index >= from_date]
            logger.opt(colors=True).info(f"<green>Filtering from: {from_date} up to the end. size before: {before_size} size after: {len(filtered)}</green>")
        elif not from_date and to_date:
            filtered = filtered[filtered.index <= to_date]
            logger.opt(colors=True).info(f"<green>Filtering from beginning up to: {to_date}. size before: {before_size} size after: {len(filtered)}</green>")
        else:
            # neither from nor to
            logger.opt(colors=True).info(f"<green>Filtering from beginning up to the end. length: {len(filtered)}</green>")

        #
        # Filtering based on video_status categories
        #
        before_size = len(filtered)
        if video_status_categories:
            filtered = filtered[filtered.video_status.isin(video_status_categories)]
            after_size = len(filtered)
            logger.opt(colors=True).info(f"<green>Filtering video categories: {video_status_categories} before: {before_size} after: {after_size}</green>")

        #
        # Filtering based on allowed categories
        #
        before_size = len(filtered)
        if allowed_categories:
            check_fn = partial(self._has_correct_categories, allowed_categories)
            filtered = filtered[filtered.category.apply(check_fn)]
            after_size = len(filtered)
            logger.opt(colors=True).info(f"<green>Filtering categories: {allowed_categories} before: {before_size} after: {after_size}</green>")

        #
        # Filtering based on restocking
        #
        before_size = len(filtered)
        if remove_restocking:
            filtered = filtered[filtered.restocking == False]
            after_size = len(filtered)
            logger.opt(colors=True).info(f"<green>Filtering restocking videos with first frame in: {FIRST_FRAME_RESTOCKING_CATEGORIES}: before: {before_size} after: {after_size}</green>")

        #
        # Filtering based on asset names
        #
        before_size = len(filtered)
        if site_asset_names:
            filtered = filtered[filtered.store_asset_name.isin(site_asset_names)]
            logger.opt(colors=True).info(f"<green>Filtering for site_asset_names: {site_asset_names} size before: {before_size} size after: {len(filtered)}</green>")
            # filtered = filtered[filtered.store_asset_name == site_asset_name]


        #
        # Filtering based on region
        #
        before_size = len(filtered)
        if region_filter:
            # top3 = top3[top3.store_asset_name.apply(lambda x: x[0] in ['V', 'P'])]
            # filtered = filtered[filtered.store_asset_name.apply(STORE_MAPPER.is_asset_of_interest)]
            filtered = filtered[filtered.region.isin(region_filter)]
            logger.opt(colors=True).info(f"<green>Filtering for regions: {region_filter} size before: {before_size} size after: {len(filtered)}</green>")

        return filtered

    def get_video_status(self, file):
        file_str = str(file)
        name = str(file).split(str(self.site_dir))[-1]
        if file_str not in self.frame.file.values:
            logger.info(f"file: {name} status: None")
            return None
        video_status = self.frame[self.frame.file == file_str].video_status
        video_status = video_status.values[0]
        logger.info(f"file: {name} status: {video_status}")
        return video_status
        # logger.info(f"file: {file} status: {video_status}")

        # if not video_status.empty:
        #     return video_status.iloc[0] in [TOP_3_CLEAR]
        # return False


class VideoAnalyzer:
    def __init__(self, model_path, site_dir):
        self.site_dir = Path(site_dir)
        self.stores = sorted(list([store.mac_id for store in STORE_MAPPER.store_mappings if store.enabled]))
        assert self.site_dir, f"Unable to open site_dir: {self.site_dir}"
        self.frame_classifier = ClassificationPredictor(model_path=model_path)
        self.valid_macs = STORE_MAPPER_WITHOUT_EXCEPTIONS.get_mac_ids()

    def _load_image(self, image_path):
        return Image.open(image_path)
    
    def get_video_frames(self, input_video):
        input_video_path = Path(input_video)
        if not input_video_path.is_file():
            logger.error(f"Unable to open: {input_video}")
            raise UnableToOpenFile(f"Unable to open: {input_video_path.name}")
        
        cap = cv2.VideoCapture(str(input_video))
        if not cap.isOpened():
            logger.error(f"Unable to open: {input_video}")
            raise UnableToOpenFile(f"Unable to open: {input_video_path.name}")

        video_frames = []
        while True:
            ret, frame = cap.read()
            if not ret:
                break
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            video_frames.append(frame)
        cap.release()

        height, width = video_frames[0].shape[:-1]
        if height != 1600:
            video_frames = [cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE) for frame in video_frames]
        return video_frames

    def _get_mac_id_from_path(self, path):
        sub_path = str(path).split(str(self.site_dir))[-1]
        return sub_path.split('/')[1]
    
    def _is_site_video(self, path):
        mac_id = self._get_mac_id_from_path(path)
        return mac_id in self.stores

    def get_video_paths(self, frame, video_dir=None):
        if video_dir is None:
            video_dir = self.site_dir
        logger.info(f"Getting video paths from: {video_dir}. This may take a while ...")
        avi_files = sorted(list(video_dir.rglob("*.avi")))
        avi_files = sorted([path for path in avi_files if self._is_site_video(path)], key=lambda p: p.stat().st_mtime, reverse=True)
        avi_files = [path for path in avi_files if str(path) not in frame.file.values]
        logger.info(f"Found {len(avi_files)} videos to analyze that are not in frame")
        return avi_files
    
    def get_recent_video_files(self, frame, video_dir=None):
        timer = Timer(f"get_recent_video_files[{video_dir.name}]")
        timer.start()
        avi_files = get_recent_files_fast(video_dir, ext=".avi")
        avi_files = sorted([path for path in avi_files if self._is_site_video(path)], key=lambda p: p.stat().st_mtime, reverse=True)
        avi_files = [path for path in avi_files if str(path) not in frame.file.values]
        logger.info(f"Found {len(avi_files)} videos to analyze that are not in frame")
        timer.stop()
        return avi_files

    def predict(self, video_path):
        images = self.get_video_frames(video_path)
        categories, confs, _ = self.frame_classifier.predict_batch(images)
        return categories, confs
    
    def predict_batch(self, video_paths):
        all_images = []
        start_indexes = []
        end_indexes = []
        index = 0
        for video_path in video_paths:
            try:
                video_images = self.get_video_frames(video_path)
                start_indexes.append(index)
                index += len(video_images)
                end_indexes.append(index)
                all_images.extend(video_images)
            except Exception as e:
                logger.error(f"Unable to open: {video_path}")
                # For error cases, we'll use strings rather than exception objects
                start_indexes.append(str(e))
                end_indexes.append(str(e))
        
        batch_categories = []
        batch_confs = []
        
        if len(all_images) > 0:
            categories, confs, _ = self.frame_classifier.predict_batch(all_images)
            
            for start, end in zip(start_indexes, end_indexes):
                if isinstance(start, str):  # Error case
                    batch_categories.append(start)
                    batch_confs.append(None)
                    continue
                batch_categories.append(categories[start:end])
                batch_confs.append(confs[start:end])
        else:
            # If no images were processed successfully
            for start, end in zip(start_indexes, end_indexes):
                batch_categories.append(start)  # Already an error string
                batch_confs.append(None)
        
        return batch_categories, batch_confs
    
    def get_video_dir_for_store(self, store):
        mac_id = STORE_MAPPER.get_mac_id_from_store_name(store)
        if mac_id is None:
            logger.error(f"Invalid store: {store}")
            raise ValueError(f"Unable to find store: {store}")
        video_dir = self.site_dir / mac_id
        assert video_dir.is_dir(), f"Unable to open: {video_dir}"
        return video_dir
    
    def _save_images(self, video_path, category, confidence, output_dir):
        shelf_settings  = STORE_MAPPER.get_shelf_settings(video_path)
        output_dir = output_dir / shelf_settings.mac_id
        output_dir.mkdir(exist_ok=True, parents=True)
        images = self.get_video_frames(video_path)
        # logger.info(f"num images: {len(images)} category: {category} confidence: {confidence}")
        for i, (image, frame_category, frame_confidence) in enumerate(zip(images, category, confidence)):
            # logger.info(f"Frame: {i}, Category: {frame_category}, Confidence: {frame_confidence}")
            if frame_category == 'totallyClear':
                image_path = output_dir / f"{video_path.stem}-frame{i:05d}.jpg"
                image = Image.fromarray(image)
                image.save(image_path)

    def analyze_dir(self, video_dir, output_dir=None, start_date=None, end_date=None):
        if output_dir is not None:
            output_dir = Path(output_dir)
            if not output_dir.is_dir():
                logger.info(f"Creating output directory: {output_dir}")
                output_dir.mkdir()

        logger.info(f"Analyzing videos in: {video_dir}")
        analysis_dir = video_dir / "analysis"
        analysis_dir.mkdir(exist_ok=True, parents=True)
        if start_date is None and end_date is None:
            frame_path = analysis_dir / f"videoAnalysis__{video_dir.name}.csv"
            stats_frame_path = analysis_dir / f"videoStats__{video_dir.name}.parquet"
        elif start_date is None and end_date is not None:
            raise ValueError("start_date is None and end_date is not None")
        elif start_date is not None and end_date is None:
            raise ValueError("start_date is not None and end_date is None")
        else:
            frame_path = analysis_dir / f"videoAnalysis__{video_dir.name}__{start_date}__{end_date}.csv"
            stats_frame_path = analysis_dir / f"videoStats__{video_dir.name}__{start_date}__{end_date}.parquet"
            start_date = datetime.strptime(start_date, "%Y-%m-%d")
            end_date = datetime.strptime(end_date, "%Y-%m-%d")

        if frame_path.is_file():
            frame = pd.read_csv(frame_path)
            initial_count = len(frame)
            frame = frame.drop_duplicates(subset=['file'])
            logger.info(f"Loading existing frame analysis: {frame_path} with {len(frame)} records. Initial count with duplicates: {initial_count}")
        else:
            frame = pd.DataFrame(columns=['file', 'category', 'confidence'])
        
        # video_paths = self.get_video_paths(frame, video_dir=video_dir)
        # video_paths = self.get_recent_video_files(frame, video_dir=video_dir)

        video_paths = get_filtered_files(video_dir, start_date, end_date)
        curr_frame = self.get_frame_for_videos(video_paths, frame, frame_path, output_dir=output_dir)

        logger.info(f"Adding {len(curr_frame)} records to frame with {len(frame)} records")
        frame = pd.concat([frame, curr_frame], ignore_index=True)

        frame.to_csv(frame_path, index=False)
        logger.info(f"Saved frame analysis to: {frame_path} number of records: {len(frame)}")

        self.save_video_stats(frame, stats_frame_path)

    def save_video_stats(self, frame, video_stats_path):
        frame = self.get_video_stats(frame)
        frame.to_parquet(video_stats_path)
        logger.info(f"Saved video stats to: {video_stats_path} with {len(frame)} records")

    def _get_error_status(self, category):
        category = str(category)
        if category.startswith("["):
            video_status = "ok"
        else:
            video_status = "error"
        return video_status
    
    def _fix_category_errors(self, category):
        category = str(category)
        if category.startswith("["):
            return category
        else:
            return None
    
    def _fix_conf_errors(self, category):
        conf = str(category)
        if conf.startswith("["):
            return conf
        else:
            return None

    def _get_timestamp_from_path(self, path):
        time_string = Path(path).stem.split('_')[1][:-3]
        if time_string is None:
            return None
        try:
            date_time = datetime.fromtimestamp(int(time_string))
        except ValueError as v:
            logger.warning(f"Invalid time_string: [{time_string}] path: {path}")
            return None
        return pd.to_datetime(date_time)

    def _get_video_status(self, category):
        if category is None:
            return 'error'
        num_frames = len(category)
        num_closed = len([c for c in category if c == CLOSED_DOOR])
        num_foggy = len([c for c in category if c == GLARE_OR_FOG])
        num_obstructed = len([c for c in category if c == TOO_OBSTRUCTED])
        num_not_open_enough = len([c for c in category if c == NOT_OPEN_ENOUGH])
        num_open_too_wide = len([c for c in category if c == OPEN_TOO_WIDE])
        num_all_skus_clear = len([c for c in category if c == ALL_SKUS_CLEAR])
        num_top3 = len([c for c in category if c == TOP_3_CLEAR])
        TOP3_THRESHOLD = 0.1
        ALL_SKUS_CLEAR_THRESHOLD = 0.1
        OTHER_THRESHOLD = 0.8
        CLOSED_THRESHOLD = 0.9
        # if num_top3 > TOP3_THRESHOLD * num_frames:
        #     # top 3 clear
        if num_all_skus_clear > 0:
            return ALL_SKUS_CLEAR
        if num_top3 > 0:
            return TOP_3_CLEAR
        if num_foggy > OTHER_THRESHOLD * num_frames:
            return GLARE_OR_FOG
        if num_obstructed > OTHER_THRESHOLD * num_frames:
            return TOO_OBSTRUCTED
        if num_not_open_enough > OTHER_THRESHOLD * num_frames:
            return NOT_OPEN_ENOUGH
        if num_open_too_wide > OTHER_THRESHOLD * num_frames:
            return OPEN_TOO_WIDE
        if num_closed > CLOSED_THRESHOLD * num_frames:
            return CLOSED_DOOR

        return OPEN_BUT_NOT_TOP3_CLEAR

    def _is_restocking(self, x):
        if x is None:
            return False
        if len(x) > 0:
            return x[0] in FIRST_FRAME_RESTOCKING_CATEGORIES
        return False
    
    def _get_shelf_settings(self, path, param_name):
        settings = STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_setting_from_store(path)
        if settings is None:
            return None
        return settings.model_dump()[param_name]
    
    def _is_file_path_valid(self, path):
        mac_id = STORE_MAPPER_WITHOUT_EXCEPTIONS.get_mac_id_from_path(path, site_dir=None)
        if mac_id is None:
            return False
        return mac_id in self.valid_macs

    def _get_conf_list_from_str(self, conf_list_str):
        if ',' in conf_list_str:
            return literal_eval(conf_list_str)
        else:
            splits = re.split('\s+', conf_list_str)
            conf_list_str = ",".join(splits)
            return literal_eval(conf_list_str)
    
    def get_video_stats(self, frame):
        logger.info(f"Generating video stats for frame with {len(frame)} records")
        frame = frame.copy()
        before = len(frame)
        frame = frame[frame.file.apply(self._is_file_path_valid)]
        after = len(frame)
        logger.info(f"Filtered out invalid files based on mac_id: {before} -> {after}")
        frame["video_status"] = frame.category.apply(self._get_error_status)
        frame["category"] = frame.category.apply(self._fix_category_errors)
        frame["confidence"] = frame.confidence.apply(self._fix_category_errors)
        frame["category"] = frame["category"].apply(lambda x: literal_eval(str(x)))
        frame["confidence"] = frame["confidence"].apply(lambda x: self._get_conf_list_from_str(str(x)))
        frame["video_status"] = frame.category.apply(self._get_video_status)
        frame['store_name'] = frame.file.apply(lambda x: self._get_shelf_settings(x, 'name'))
        frame['store_asset_name'] = frame.file.apply(lambda x: self._get_shelf_settings(x, 'asset_name'))
        frame['mac_id'] = frame.file.apply(lambda x: self._get_shelf_settings(x, 'mac_id'))
        frame['region'] = frame.file.apply(lambda x: self._get_shelf_settings(x, 'region'))
        frame = frame[frame.store_name.notnull()]
        frame = frame[frame.store_asset_name.notnull()]
        frame = frame[frame.mac_id.notnull()]
        frame = frame[frame.region.notnull()]
        frame['date'] = frame.file.apply(self._get_timestamp_from_path)
        frame = frame[frame.date.notnull()]
        frame['file'] = frame.file.apply(lambda x: str(x))
        frame['restocking'] = frame.category.apply(self._is_restocking)
        frame.set_index('date', inplace=True)
        sorted_df = frame.sort_index()
        return sorted_df

    def get_frame_for_videos(self, video_paths, master_frame, frame_path, output_dir=None):
        frame_path = frame_path.parent / f"partialVideoAnalysis__{frame_path.stem}.csv"
        if frame_path.is_file():
            frame = pd.read_csv(frame_path)
            frame = frame[~frame.file.isin(master_frame.file.values)]
            logger.info(f"Loading existing partial frame analysis: {frame_path} with {len(frame)} records that are not in the master frame")
        else:
            frame = pd.DataFrame(columns=['file', 'category', 'confidence'])
            logger.info(f"Creating new partial frame analysis: {frame_path} with {len(frame)} records")
        
        # Convert paths to strings for consistent comparison
        file_set = set(str(path) for path in master_frame.file.values)
        video_paths = [path for path in video_paths if str(path) not in file_set]
        
        filtered_video_paths = [path for path in video_paths if path not in frame.file.values]
        
        already_analyzed = len(video_paths) - len(filtered_video_paths)
        logger.info(f"Analyzing {len(filtered_video_paths)} videos that are not in the master frame, already analyzed records: {already_analyzed}")
        frame = self.add_rows_to_frame_batch(frame_path, frame, filtered_video_paths, output_dir=output_dir)
        return frame

    # def add_rows_to_frame(self, frame_path, frame, video_paths, output_dir=None):
    #     rows = []
    #     already_analyzed = 0
    #     for i, video_path in enumerate(tqdm(video_paths)):
    #         if video_path in frame.file.values:
    #             already_analyzed += 1
    #             continue
    #         try:
    #             category, confidence = self.predict(video_path)
    #             d = {}
    #             d['file'] = video_path
    #             d['category'] = category
    #             d['confidence'] = confidence
    #             if output_dir:
    #                 self._save_images(video_path, category, confidence, output_dir)
    #         except Exception as e:
    #             d = {}
    #             d['file'] = video_path
    #             d['category'] = f"{e}"
    #             d['confidence'] = None
    #         rows.append(d)
    #         if i % 1000:
    #             curr_frame = pd.DataFrame(rows)
    #             frame = pd.concat([frame, curr_frame], ignore_index=True)
    #             frame.to_csv(frame_path, index=False)
    #             rows = []
    #     logger.info(f"Analyzed {len(video_paths)} videos, already analyzed: {already_analyzed}")
    #     return frame
    
    def add_rows_to_frame_batch(self, frame_path, frame, video_paths, output_dir=None):
        batch_size = 32
        for i in tqdm(range(0, len(video_paths), batch_size)):
            rows = []
            batch_video_paths = video_paths[i:i+batch_size]
            categories, confs = self.predict_batch(batch_video_paths)
            for category, confidence, video_path in zip(categories, confs, batch_video_paths):
                d = {}
                d['file'] = str(video_path)  # Ensure path is a string
                d['category'] = category
                d['confidence'] = confidence
                rows.append(d)
            curr_frame = pd.DataFrame(rows)
            frame = pd.concat([frame, curr_frame], ignore_index=True)
            frame.to_csv(frame_path, index=False)
            gc.collect()
        return frame
    
    def analyze_dirs(self, video_dirs, output_dir=None, start_date=None, end_date=None):
        for video_dir in video_dirs:
            settings = STORE_MAPPER.get_shelf_settings(video_dir)
            if settings is None:
                logger.error(f"Unable to find settings for: {video_dir}")
                continue
            assert video_dir.is_dir(), f"Unable to open: {video_dir}"

        for video_dir in video_dirs:
            self.analyze_dir(video_dir, output_dir=output_dir, start_date=start_date, end_date=end_date)

    def analyze_site(self, start_date=None, end_date=None): 
        self.analyze_dir(self.site_dir, start_date=start_date, end_date=end_date)


def main():
    log_dir = Path(__file__).resolve().parent.parent / "logs"
    log_dir.mkdir(exist_ok=True, parents=True)
    logger.add(log_dir / "analyzeVideos_{time}.log", format="{time:MMMM D, YYYY HH:mm:ss} | {level} | {file}:{line} | {message}")
    if Path("/home/akshay/datasets").is_dir():
        site_dir = Path("/home/akshay/siteData")
    else:
        site_dir = Path("/media/akshay/datasets/coke/siteData")
    # base_dir = Path(__file__).resolve().parent.parent
    # model_path = base_dir / "models/frameSelection.pth"
    cfg = configparser.ConfigParser()
    cfg.read('inference.ini')
    frame_selection_model_path = Path(cfg['DEFAULT']['frameselection_model_path'])

    assert frame_selection_model_path.is_file(), f"Unable to open: {frame_selection_model_path}"

    v = VideoAnalyzer(model_path=frame_selection_model_path, site_dir=site_dir)

    args = parse_arguments()
    stores = args.stores
    if args.from_date is None:
        from_date = None
    else:
        from_date = datetime.strptime(args.from_date, "%Y-%m-%d")

    if args.to_date is None:
        to_date = None
    else:
        to_date = datetime.strptime(args.to_date, "%Y-%m-%d")

    if stores is None:
        v.analyze_site(start_date=from_date, end_date=to_date)
    else:
        video_dirs = [v.get_video_dir_for_store(store) for store in stores]
        v.analyze_dirs(video_dirs, output_dir=args.output_dir, start_date=from_date, end_date=to_date)
    

if __name__ == "__main__":
    main()
