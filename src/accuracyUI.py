from pathlib import Path
from loguru import logger
from types import GeneratorType
import cv2
import streamlit as st
from shelfSettings import StoreManager

from utils import ProdigyLoader, update_dict_paths
from commonUI import ItemNavigator
from accuracy import get_predictor, Prodigy2Chiller
from shelfPredictor import PredictedChiller, ChillerComparison


class JSONLNavigator(ItemNavigator):
    def __init__(self, item_dir):
        if "shelf_clip_index" not in st.session_state:
            st.session_state.shelf_clip_index = 3
        if "comparison_type" not in st.session_state:
            st.session_state.comparison_type = "coke_only"
        if "filter_asset_name" not in st.session_state:
            st.session_state.filter_asset_name = None

        super().__init__(item_dir=item_dir, display_image_callback=self.process_image_callback, uploadable=True, upload_types=["jsonl"])

        self.loader = ProdigyLoader()
        self.predictor = get_predictor(fixed_width_transform=True)
        self.processor = None
        self.STORE_MAPPER_WITHOUT_EXCEPTIONS = StoreManager(raise_exception=False, silent=True)
        # self.filter_asset_name = None
        self.filter_asset_name_choices = None
        # self.shelf_dicts = 
        self.shelf_jsonls = None
    
    def get_items(self):
        bottle_dicts = []
        with st.spinner("Loading items..."):
            bottle_files = sorted(list(self.item_dir.glob("*bottle*.jsonl")))
            shelf_files = sorted(list(self.item_dir.glob("*shelf*.jsonl")))
            self.shelf_jsonls = shelf_files

            shelf_dicts, _ = self.loader.load_files(shelf_files)
            bottle_dicts, _ = self.loader.load_files(bottle_files)
            if len(shelf_dicts) == 0:
                logger.warning("No shelf items found in the directory")
                self.processor = None
                return []
            if len(bottle_dicts) == 0:
                logger.warning("No bottle items found in the directory")
                self.processor = None
                return []
            self.processor = Prodigy2Chiller()
            self.shelf_dicts = shelf_dicts

        # IMP: Each bottle_dict is actually all bottles for a single chiller image
        asset_names = []
        for bottle_dict in bottle_dicts:
            path = bottle_dict["path"]
            settings = self.STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(path)
            if settings:
                asset_names.append(settings.asset_name)
        import re
        self.filter_asset_name_choices = sorted(list(set(asset_names)), key=lambda x: (re.match(r"([A-Za-z]+)(\d+)", x).groups()[0], int(re.match(r"([A-Za-z]+)(\d+)", x).groups()[1])))
        if st.session_state.filter_asset_name is not None:
            bottle_dicts = [bottle_dict for bottle_dict, region in zip(bottle_dicts, asset_names) if region == st.session_state.filter_asset_name]
        return bottle_dicts
    
    def get_filename_from_item(self, item):
        if item is None:
            return None
        return Path(item["path"]) 
    
    def _get_asset_name(self, item):
        path = Path(item["path"])
        settings = self.STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(path)
        if settings is None:
            logger.error(f"Shelf settings not found for: {path}")
            raise Exception(f"Shelf settings not found for: {path}")
        return settings.asset_name
    
    def get_item_names(self, items):
        # names = [Path(item['path']).name for item in items]
        asset_names = [self._get_asset_name(item) for item in items]
        paths = [Path(item['path']) for item in items]
        names = [f"{asset_name}: {path.name}" for asset_name, path in zip(asset_names, paths)]
        return names
    
    def delete_files(self):
        bottle_files = sorted(list(self.item_dir.glob("*bottle*.jsonl")))
        shelf_files = sorted(list(self.item_dir.glob("*shelf*.jsonl")))
        files = bottle_files + shelf_files
        for file in files:
            file.unlink()
        st.info(f"Deleted [{len(files)}] files")

    def get_image_from_item(self, item):
        image_path = item["path"]
        image_path = self.processor.get_path(image_path)
        if not Path(image_path).exists():
            logger.error(f"Image not found: {image_path}")
            return None
        input_image = cv2.imread(str(image_path))
        input_image = cv2.cvtColor(input_image, cv2.COLOR_BGR2RGB)
        # st.write(f"Image Path: {image_path} image shape: {input_image.shape}")

        return input_image
    
    def _on_filter_asset_change(self):
        st.file_slider = 0
        st.session_state.index = 0

    def _get_filtered_names(self, filter_asset_name):
        filtered_names = []
        i = 0
        for bottle_dict in st.session_state.item_list:
            path = Path(bottle_dict["path"])
            settings = self.STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(path)
            if settings.asset_name == filter_asset_name:
                filtered_names.append(f"{i}:{path.name}")
                i += 1
        return filtered_names
    
    def process_image_callback(self, input_image, bottle_d):
        image_path = bottle_d["path"]
        image_path = self.processor.get_path(image_path)
        st.write(f"Image Path: {image_path}")

        shelf_clip_index = st.slider("Shelf Clip Index", 3, 6, 3)
        st.session_state.shelf_clip_index = shelf_clip_index
        comparison_type = st.selectbox("Comparison Type:", ["coke_only", "catalog_only", "catalog_only_without_volume", "all"], index=0)
        st.session_state.comparison_type = comparison_type
        filter_asset_name = st.selectbox("Filter by Asset Name", ["None"] + self.filter_asset_name_choices, index=0, on_change=self._on_filter_asset_change)
        if filter_asset_name == "None":
            filter_asset_name = None
            st.session_state.filter_asset_name = None
        else:
            st.session_state.filter_asset_name = filter_asset_name

        progress_bar = st.progress(0, text="Processing...")
        accuracy_placeholder = st.empty()

        col1, col2, col3 = st.columns([1, 1, 1])
        col1.title("Ground Truth")
        col2.title("Predicted")        
        col3.title(f"Comparisons:")

        placeholder1 = col1.empty()
        placeholder2 = col2.empty()
        
        placeholder1.image(input_image, use_container_width=True)
        placeholder2.image(input_image, use_container_width=True)

        shelf_mapping = self.STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(image_path)
        if shelf_mapping is None:
            logger.error(f"Shelf mapping not found for: {image_path}")
            col2.write("Shelf mapping not found")
            st.stop()

        progress_bar.progress(10, text="Predicting ...")        
        chiller = self.predictor.predict(image_path, shelf_heights=shelf_mapping.shelf_heights, shelf_widths=shelf_mapping.shelf_widths)
        predicted_image = chiller.draw_bottles()
        placeholder2.image(predicted_image)

        progress_bar.progress(30, text="Generating Ground Truth ...")
        heights = [shelf.new_height for shelf in chiller.shelves]
        widths = [shelf.new_width for shelf in chiller.shelves]
        shelf_d = self.loader.find_matching_shelf_dict(self.shelf_jsonls, bottle_d['path'])
        if Path('/home/akshay/datasets').is_dir():
            path_prefix = None
            path_prefix_replacement = None
        else:
            path_prefix = '/home/akshay/datasets'
            path_prefix_replacement = '/home/akshay/workspace/labelling/coke/datasets'
        shelf_d = update_dict_paths(prodigy_dict=shelf_d, path_prefix=path_prefix, path_prefix_replacement=path_prefix_replacement)
        chiller_gt: PredictedChiller = self.processor.get_chiller(bottle_d, shelf_d, heights, widths)
        if chiller_gt is None:
            logger.error(f"Ground Truth not found for: {image_path}")
            col1.write("Ground Truth not found")
            st.stop()
        # for s in chiller_gt.shelves:
        #     logger.info(f"gt shelf[{s.label}]: {len(s.bottles)} bottles")
        placeholder1.image(chiller_gt.output_image)
        progress_bar.progress(60, text="Calculating Accuracy ...")

        comparison: ChillerComparison = chiller_gt.compare(chiller)
        accuracy, correct, total, mappings = comparison.get_accuracy(comparison_type, shelf_clip_index=shelf_clip_index)
        
        accuracy_placeholder.header(f"Accuracy: {accuracy:4.2f}% Correct: [{correct}/{total}]", divider=True)
        logger.info(f"Accuracy: {accuracy} comparison_type: {comparison_type}")
        for mapping in mappings:
            mapping_string = "[" + " ".join([f"{i}:{'NoMatch' if m is None else m}" for i, m in enumerate(mapping)]) + "]"
            col3.text(mapping_string)
            logger.info(mapping_string)
            col3.markdown("---")
        progress_bar.progress(100, text="Done!")


    

####################################################################################################################
# Initializations
####################################################################################################################
@st.cache_resource
def get_logger():
    # from loguru import logger

    log_file = LOG_DIR / "accuracyUI.log"
    logger.add(
        log_file,
        format="{time}|{file}|{line}|{thread}|{level}|{message}",
        colorize=True,
    )
    return logger

@st.cache_resource
def get_navigator():
    return JSONLNavigator(item_dir=DATA_DIR)

BASE_DIR = Path(__file__).parent.parent
LOG_DIR = BASE_DIR / "logs"
DATA_DIR = BASE_DIR / "data" / "accuracyUI"
if not DATA_DIR.is_dir():
    DATA_DIR.mkdir(parents=True)
# DATA_DIR = DATA_DIR / "partialSort"
# if not DATA_DIR.is_dir():
#     DATA_DIR.mkdir()
st.set_page_config(page_title="ShelfPredictor", layout="wide")
if "shelf_clip_index" not in st.session_state:
    st.session_state.shelf_clip_index = 3
if "comparison_type" not in st.session_state:
    st.session_state.comparison_type = "coke_only"
if "filter_asset_name" not in st.session_state:
    st.session_state.filter_asset_name = None

logger = get_logger()
navigator = get_navigator()
####################################################################################################################
# User Interface
####################################################################################################################
navigator.displayUI()
