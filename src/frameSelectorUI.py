import streamlit as st
import requests
import json
import base64
import io
from PIL import Image
import numpy as np
from pathlib import Path
import cv2
import pandas as pd
from pydantic import BaseModel
import re
from classificationInference import FrameSelector, VideoDecodeError, VideoClassification
import configparser
from commonVideoUI import ItemNavigator, UnableToOpenFile, VideoNavigator
from utils import get_video_frames, UnableToOpenFile
import traceback
# from loguru import logger

# st.set_page_config(page_title="FrameSelection", layout="wide")
st.set_page_config(page_title="FrameSelection", layout="centered")


@st.cache_resource
def get_model() -> FrameSelector:
    model = FrameSelector()
    return model


@st.cache_resource
def get_logger(name):
    from loguru import logger
    LOG_DIR = Path(__file__).parent.parent.parent
    log_file = LOG_DIR / f"{name}.log"
    logger.add(
        log_file,
        format="{time}|{file}|{line}|{thread}|{level}|{message}",
        colorize=True,
    )
    return logger


class FrameSelectorUI(VideoNavigator):
    def __init__(self, name="frameSelectorUI"):
        super().__init__(name=name)
        self.model = get_model()
        if "videoClassification" not in st.session_state:
            st.session_state.videoClassification = None

    def reset_frame_state(self):
        super().reset_frame_state()
        st.session_state.videoClassification = None

    def get_frame_classification_string(self, frame_classifications):
        if frame_classifications is None:
            logger.info(f"frame_classification_string: ")
            return f""
        frame_classification_string = "<br/>".join([f"{i:02d}: {f.category.strip():15s}[*{f.score:4.2f}*]" for i, f in enumerate(frame_classifications)])
        frame_classification_string.replace(" ", "&nbsp;")
        return frame_classification_string
    
    def get_image_from_item(self, item):
        try:
            if st.session_state.frame_index is None:
                videoClassification: VideoClassification
                videoClassification, frames = self.model.select_from_video(item, return_frames=True)
                logger.info(f"Num frames in video: {len(frames)}")
                if frames is None or len(frames) == 0:
                    st.warning(f"No frames found in: {item}")

                    st.session_state.num_frames = 0
                    st.session_state.frame_index = 0
                    st.session_state.frames = []
                    return None
                if videoClassification.frame_classifications.result_index is None:
                    st.session_state.num_frames = len(frames)
                    st.session_state.frame_index = 0
                    st.session_state.frames = frames
                    st.session_state.frame_slider = 0
                    st.session_state.videoClassification = videoClassification

                    st.warning(f"Unable to find suitable frame in: {item.name}")
                    if st.session_state.frame_index is None:
                        st.session_state.frame_index = 0
                    output_frame = st.session_state.frames[st.session_state.frame_index]
                    return output_frame
                
                st.session_state.num_frames = len(videoClassification.frame_classifications.frame_classifications)

                # result_frame = videoClassification.frame_classifications.result_frame
                result_index = videoClassification.frame_classifications.result_index

                # save in session state
                st.session_state.frame_index = result_index
                st.session_state.frames = frames
                st.session_state.frame_slider = result_index
                st.session_state.videoClassification = videoClassification

            else:
                st.session_state.frame_slider = st.session_state.frame_index
            
            if st.session_state.frames and len(st.session_state.frames) > 0:
                output_frame = st.session_state.frames[st.session_state.frame_index]
            else:
                output_frame = None

            return output_frame
        
        except UnableToOpenFile as e:
            exception_message = str(e)
            exception_type = type(e).__name__
            traceback_info = traceback.format_exc()
            logger.error(f"Exception Type: {exception_type}")
            logger.error(f"Exception Message: {exception_message}")
            logger.error(f"Traceback Info:\n{traceback_info}")

    def display_image(self, input_image, item):
            col1, col2 = st.columns([1, 1])
            videoClassification = st.session_state.videoClassification
            if st.session_state.frames is None or len(st.session_state.frames) == 0:
                return

            if videoClassification is None:
                result_index = None
                status_string = ""
            else:
                result_index = videoClassification.frame_classifications.result_index
                status_string = self.get_frame_classification_string(videoClassification.frame_classifications.frame_classifications)

            if input_image is not None:
                col1.image(input_image, use_container_width=True)

            col2.write(f"Result Index: {result_index} frame_index: {st.session_state.frame_index}")
            if videoClassification.frame_classifications.result_index is None:
                col2.write(f"Result Category: No acceptable frames found")
            else:
                col2.write(f"Result Category: {videoClassification.frame_classifications.frame_classifications[result_index].category}")
            col2.markdown(f"Frame Status: <br/>{status_string}", unsafe_allow_html=True)
        


def main():
    name="frameSelectorUI"
    global logger
    logger = get_logger(name)
    logger.info("Starting FrameSelectorUI")    
    st.title(f"{name}")
    frame_selector = FrameSelectorUI(name=name)
    frame_selector.displayUI()


if __name__ == "__main__":
    main()