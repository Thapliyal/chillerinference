#!/usr/bin/env python3

from pathlib import Path
from fabric import Connection
from tqdm.auto import tqdm
import re
import pandas as pd
import traceback
from loguru import logger
from argparse import ArgumentParser
from multiprocessing import Pool
from functools import partial
from shelfSettings import StoreManager
import time

STORE_MAPPER = StoreManager()

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('-r', '--remote-dir', help='Remote directory', required=False, default=None)
    parser.add_argument('-l', '--local-dir', help='Local directory', required=False, default=None)
    parser.add_argument('-t', '--threads', help='Number of threads', required=False, default=8, type=int)
    return parser.parse_args()

class DirDownloader:
    def __init__(self, remote_dir, local_dir):
        self.remote_dir = remote_dir
        self.local_dir = local_dir
        self.connection = self._get_connection()
    
    def download(self):
        files = self._get_files_for_dir(self.remote_dir)
        total = len(files)
        local_paths = [self.get_local_path(f) for f in files]
        new_files = [Path(f) for f in local_paths if not Path(f).is_file()]
        logger.info(f"{self.remote_dir} --> {self.local_dir}: [{len(new_files)}/{len(files)}] to download")
        downloaded = 0
        for remote_file, local_file in zip(files, local_paths):
            if local_file.is_file():
                continue
            status = self.download_file(remote_file, local_file)
            if status:
                downloaded += 1
        return downloaded, len(new_files), total

    def _get_connection(self):
        connection = Connection(
                    host='yotta',
                    user='ubuntu',
                    connect_kwargs={
                        "key_filename": "/home/akshay/.ssh/iot.pem",
                    }
                )        
        return connection
    def _get_files_for_dir(self, remote_dir):
        get_files_command = f"find {remote_dir} -name '*.avi'"
        # logger.info(f"executing: {get_files_command}")
        files_result = self.connection.run(get_files_command, hide="stdout")
        # files_result = self.connection.run(get_files_command)
        if files_result.ok:
            files = files_result.stdout.split("\n")
            files = [f for f in files if len(f) > 0]
            return files
        return []

    def get_local_path(self, remote_path):
        local_path = re.sub(self.remote_dir, str(self.local_dir), remote_path)
        return Path(local_path)

    def download_file(self, remote_path, local_path):
        # local_path = self.get_local_path(remote_path)
        local_path = Path(local_path)
        if local_path.is_file():
            # logger.debug(f"file already exists: {local_path}")
            return True
        if not local_path.parent.is_dir():
            local_path.parent.mkdir(parents=True)

        try:
            # logger.debug(f"downloading: {remote_path}")
            res = self.connection.get(remote_path, str(local_path))
            if not res:
                logger.error(f"unable to download: {remote_path}")                
                return False            
            return True
        except Exception as e:
            logger.error(e)
            trace_string = traceback.format_exc()
            logger.error(trace_string)
            return False


def download(remote_dir, base_local_dir):
    # local_dir = Path("/tmp/testDownload/downloadTesting") / Path(remote_dir).name
    try:
        d = DirDownloader(remote_dir, base_local_dir / Path(remote_dir).name)
        downloaded, new, total = d.download()
        return downloaded, new, total
    except Exception as e:
        logger.error(e)
        trace_string = traceback.format_exc()
        logger.error(trace_string)
        return 0, 0, 0


class SFTPDownloader:
    def __init__(self, server_dir=None, local_dir=None, num_threads=4):
        self.connection = self.connect()
        self.num_threads = num_threads
        
        if server_dir is None:
            self.SERVER_DIR = "/datadisk/sdc/rdp/cocacola"
        else:
            self.SERVER_DIR = server_dir

        if local_dir is None: 
            if Path("/home/akshay/datasets").is_dir():       
                self.LOCAL_DIR = Path('/home/akshay/siteData')
            else:
                self.LOCAL_DIR = Path('/media/akshay/datasets/coke/siteData')
        else:
            self.LOCAL_DIR = Path(local_dir)
        if not self.LOCAL_DIR.is_dir():
            self.LOCAL_DIR.mkdir()    

        self.asset_name_mapping = {s.mac_id.lower(): s.asset_name for s in STORE_MAPPER.store_mappings}
        self.name_mapping = {s.mac_id.lower(): s.name for s in STORE_MAPPER.store_mappings}
        self.MACS = [s.mac_id.lower() for s in STORE_MAPPER.store_mappings if STORE_MAPPER.is_asset_of_interest(s.asset_name)]

    # def _is_allowed_asset(self, store):
    #     if store.asset_name[0] in ['V', 'P']:
    #         return True
    #     if store.asset_name in ["Demo Unit Pune", "9 Caser - Frigo", "Rajal Dryfruits", "Rajal Dryfruits 2", "9 Caser-Western", "9 Caser-Western2"]:
    #         return True
    #     return False
    
    def connect(self):
        return Connection(
                    host='yotta',
                    user='ubuntu',
                    connect_kwargs={
                        "key_filename": "/home/akshay/.ssh/iot.pem",
                    }
                )        

    def get_site_dirs(self):
        # command = f"find {SERVER_DIR}/00B0D063C226 -name '*.avi'"
        command = f"find {self.SERVER_DIR} -maxdepth 1 -type d"
        result = self.connection.run(command, hide='stdout')
        if result.ok:
            remote_dirs = result.stdout.split("\n")[1:]
            remote_dirs = [r for r in remote_dirs if len(r) > 0 and self.is_valid(r)]
            logger.info(f"Found [{len(remote_dirs)}] remote dirs")
        return remote_dirs

    def get_mac_id(self, path):
        p = str(path)
        if self.SERVER_DIR[-1] != "/":
            server_dir = self.SERVER_DIR + "/"
        else:
            server_dir = self.SERVER_DIR 
        sub_path = p.split(server_dir)[-1]
        mac = sub_path.split("/")[0].lower()
        return mac

    def is_valid(self, path):
        mac = self.get_mac_id(path)
        # print(f"{mac}, result: {mac in MACS}")
        if mac in self.MACS:
            return True
        return False

    def get_local_path(self, remote_path):
        local_path = re.sub(self.SERVER_DIR, str(self.LOCAL_DIR), remote_path)
        return local_path
    
    def display_local_summary(self):
        sub_dirs = [d for d in self.LOCAL_DIR.iterdir() if d.is_dir()]
        for sub_dir in sorted(sub_dirs, key=lambda x: self.asset_name_mapping.get(x.name.lower(), x.name)):
            files = list(sub_dir.glob('**/*.avi'))
            if sub_dir.name.lower() in self.asset_name_mapping:
                asset_name = self.asset_name_mapping[sub_dir.name.lower()]
            else:
                asset_name = sub_dir.name
            retailer_name = self.name_mapping.get(sub_dir.name.lower(), "")
            logger.info(f"{asset_name:12s}: {retailer_name:40s}: {len(files)}")

    def sync(self):
        remote_dirs = self.get_site_dirs()
        download_fn = partial(download, base_local_dir=self.LOCAL_DIR)
        with Pool(self.num_threads) as pool:
            r = pool.imap(download_fn, remote_dirs)
            results = [result for result in r] 
        for result, remote_dir in zip(results, remote_dirs):
            downloaded, new, total = result
            asset_name = self.asset_name_mapping.get(Path(remote_dir).name.lower(), Path(remote_dir).name)
            logger.info(f"Downloaded [{downloaded:4d}/{new:4d}] new files from [{asset_name:4s}: {remote_dir:30s}] with total [{total:5d}] files")
        # self.display_local_summary()

    def _get_files_for_dir(self, remote_dir):
        get_files_command = f"find {remote_dir} -name '*.avi'"
        files_result = self.connection.run(get_files_command, hide="stdout")
        # files_result = self.connection.run(get_files_command)
        if files_result.ok:
            files = files_result.stdout.split("\n")
            files = [f for f in files if len(f) > 0 and self.is_valid(f)]
            return files
        return []
    

def main():
    args = parse_arguments()
    downloader = SFTPDownloader(args.remote_dir, args.local_dir, args.threads)
    downloader.sync()

if __name__ == "__main__":
    main()
