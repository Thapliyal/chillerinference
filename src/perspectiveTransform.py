from pathlib import Path
import json
import cv2
import numpy as np
from matplotlib import pyplot as plt
from pydantic import BaseModel
from typing import List, Union, Optional
from loguru import logger
from lensCorrector import LensCorrector, Undistorter


class PerspectiveTransformer:
    def __init__(self, debug=True):
        self.debug = debug
        
    def _get_polygon(self, largest_contour):
        # segment = np.array(segment).astype(np.uint8) 
        # contours, _ = cv2.findContours(segment, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_SIMPLE)
        # largest_contour = max(contours, key=cv2.contourArea)
        simplified_contour = None
        for min, max, segments in zip([0.1, 0.01, 0.001, 0.0001], [1, 10, 100, 1000], [10, 100, 10000, 1000000]):
            found = False
            for eps in np.linspace(min, max, segments):
                peri = cv2.arcLength(largest_contour, True)
                simplified_contour = cv2.approxPolyDP(largest_contour, eps * peri, True)
                if len(simplified_contour) == 4:
                    found = True
                    break
            if found:
                break
            else:
                if self.debug:
                    logger.info(f"min: {min} max: {max} num_segments: {segments} simplified_contour: {simplified_contour}")
            assert len(simplified_contour) == 4, f"Could not simplify contour: {simplified_contour}"
        
        if self.debug:
            logger.info(f"min: {min} max: {max} num_segments: {segments} simplified_contour: {simplified_contour}")
        largest_contour = np.array(simplified_contour).squeeze(axis=1)
        return largest_contour
    
    
    # def _get_simplified_contour(self, contour):
    #     epsilon = 0.1*cv2.arcLength(contour,True)
    #     approx = cv2.approxPolyDP(contour,epsilon,True)
    #     return approx
    
    def _get_dist(self, p1, p2):
        import math
        return math.sqrt(pow((p2[1] - p1[1]), 2) + pow((p2[0] - p1[0]), 2))
    
    def _get_angle(self, p1, p2):
        dx = abs(p2[0] - p1[0])
        dy = abs(p2[1] - p1[1])
        radians = np.arctan(dy/dx)
        degrees = radians * 180 / np.pi
        return degrees, radians
    
    def _get_mid_point(self, p1, p2):
        x = abs(p2[0] - p1[0]) // 2
        y = abs(p2[1] - p1[1]) // 2
        return [x, y]
    
    def _get_intersection(self, x1,y1,x2,y2,x3,y3,x4,y4):
            px= ( (x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4) ) / ( (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) ) 
            py= ( (x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4) ) / ( (x1-x2)*(y3-y4)-(y1-y2)*(x3-x4) )
            return [px, py]
    
    def _get_4point_contour(self, contour, start, end, intervals):
        if len(contour) == 4:
            return contour
        for eps in np.linspace(start, end, intervals):
            peri = cv2.arcLength(contour, True)
            simplified_contour = cv2.approxPolyDP(contour, eps * peri, True)
            if len(simplified_contour) == 4:
                return simplified_contour
        return None
    
    def get_polygon(self, largest_contour):
        if self.debug:
            logger.debug(f"get_polygon: {largest_contour.tolist()}")
        if len(largest_contour) == 4:
            return self._reorder(largest_contour)

        simplified_contour = None
        # Method for simplifying contour
        starts = [0.0001, 0.001, 0.01, 0.1, 1]
        ends = [0.1, 1, 10, 100, 100]
        intervals = [1000000, 100000, 10000, 1000, 100]
        for start, end, interval in zip(starts, ends, intervals):
            simplified_contour = self._get_4point_contour(largest_contour, start, end, interval)
            if simplified_contour is not None:
                break

        if simplified_contour is None:
            logger.error(f"Could not simplify contour to 4 points: {simplified_contour} ... getting enclosing rectangle")
            r1 = cv2.minAreaRect(largest_contour)
            box = cv2.boxPoints(r1)
            box = [[b] for b in box]
            simplified_contour = np.int8(box)

        if len(simplified_contour) != 4:
            raise ValueError(f"Could not simplify contour to 4 points: {simplified_contour}")

        if self.debug:
            logger.debug(f"simplified contour: {simplified_contour.tolist()}")

        largest_contour = np.array(simplified_contour).squeeze(axis=1)
        return self._reorder(largest_contour)
        
    def _reorder(self, contour):
        points = contour.tolist()
        # sort based on x-axis
        points = sorted(points, key=lambda p: p[0])
        if self.debug:
            logger.debug(f"sorted points by x: {points}")
        left1, left2 = points[:2]
        right1, right2 = points[2:]

        if left1[1] > left2[1]:
            left1, left2 = left2, left1
        if right1[1] > right2[1]:
            right1, right2 = right2, right1
        if self.debug:
            logger.debug(f"reordered points: {left1} {right1} {right2} {left2}")
        return left1, right1, right2, left2

    # def _reorder(self, contour):
    #     if isinstance(contour, list):
    #         contour = np.array(contour)
    #     c = contour.tolist()
    #     indexes = list(range(len(contour.tolist())))
    #     x_indexes = sorted(indexes, key=lambda i: c[i][0])
    #     y_indexes = sorted(indexes, key=lambda i: c[i][1])
    #     left_two = x_indexes[0: 2]
    #     right_two = x_indexes[2:]

    #     if self.debug:
    #         logger.debug(f"before sorting: left two: {left_two} right two: {right_two}")
        
    #     first = y_indexes[0]
    #     second = y_indexes[1]

    #     if self.debug:
    #         logger.debug(f"y first: {first} y second: {second}")
        
    #     if first in left_two and first != left_two[0]:
    #         left_two = left_two[::-1]
    #     elif first in right_two and first != right_two[0]:
    #         right_two = right_two[::-1]
    
    #     if second in left_two and second != left_two[0]:
    #         left_two = left_two[::-1]
    #     elif second in right_two and second != right_two[0]:
    #         right_two = right_two[::-1]

    #     if self.debug:
    #         logger.debug(f"after sorting: left two: {left_two} right two: {right_two}")
        
    #     p1, p4 = left_two
    #     p2, p3 = right_two
        
    #     return contour[p1].tolist(), contour[p2].tolist(), contour[p3].tolist(), contour[p4].tolist()
    
    def _get_transform_params(self, image, contour, width, height):
        # contour = sorted(contour.tolist(), key=lambda x: w * x[1] + x[0])
        # ordered = self._reorder(contour)
        p1, p2, p3, p4 = contour
        # if self.debug:
        #     logger.debug(f'p1, p2, p3, p4: {ordered}')
        if width is None:
            width = int(max(self._get_dist(p1, p2), self._get_dist(p3, p4)))
        if height is None:
            height = int(max(self._get_dist(p2, p3), self._get_dist(p4, p1)))
        pts_src = np.array([p1, p2, p3, p4], dtype=np.float32)
        pts_dst = np.array([[0, 0], [width - 1, 0], [width - 1, height - 1], [0, height - 1]], dtype=np.float32)
        M = cv2.getPerspectiveTransform(pts_src, pts_dst)
        warped = cv2.warpPerspective(image, M, (width, height))
        # logger.debug(f"after warpPerspective: {warped.shape}")
        return M, warped
    
    def fix_lighting(self, image):
        #HSV+ INPAINT + CLAHE
        lab1 = cv2.cvtColor(image, cv2.COLOR_RGB2LAB)
        lab_planes0, lab_planes1, lab_planes2 = cv2.split(lab1)
        clahe1 = cv2.createCLAHE(clipLimit=2.0,tileGridSize=(8,8))
        lab_planes0 = clahe1.apply(lab_planes0)
        lab1 = cv2.merge([lab_planes0, lab_planes1, lab_planes2])
        clahe_rgb = cv2.cvtColor(lab1, cv2.COLOR_LAB2RGB)
        return clahe_rgb

    def get_transform_params(self, image_or_path, points, fix_lighting=False, height=None, width=None):
        is_path = False
        if isinstance(image_or_path, str):
            image_or_path = Path(image_or_path)
            is_path = True
        elif isinstance(image_or_path, Path):
            is_path = True
        elif isinstance(image_or_path, np.ndarray):
            is_path = False
        else:
            raise ValueError(f"image_or_path should be a path or a numpy array, found: {type(image_or_path)}")
        
        if self.debug and is_path:
            logger.debug(f'get_transform_params for: {image_or_path}')
        if is_path:
            image = cv2.imread(str(image_or_path))
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        else:
            image = image_or_path
        
        contour = np.array(points).astype(np.float32)
        contour = self.get_polygon(contour)
        M, sub = self._get_transform_params(image, contour, width, height)
        if fix_lighting:
            sub = self.fix_lighting(sub)
        if self.debug:
            logger.debug('-'*120)
        return M, sub
    
