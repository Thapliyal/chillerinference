#!/usr/bin/env python3
from pathlib import Path
import cv2
from segmentationInference import SegmentationPredictor, PredictedBottle, Detection
from perspectiveTransform import PerspectiveTransformer, Undistorter
from typing import Optional, ClassVar, List, Union
from pydantic import BaseModel, model_validator
from matplotlib import pyplot as plt
import numpy as np
from PIL import Image
import PIL
from loguru import logger
from utils import load_image, get_blur_value, get_sub_image_from_points
from argparse import ArgumentParser
from shelfSettings import StoreManager
from tqdm.auto import tqdm
import configparser
import json
import random
from segmentationInference import BBox, code2brandVolumeType
from collections import Counter

STORE_MAPPER = StoreManager()
cfg = configparser.ConfigParser()
cfg.read('inference.ini')
SHELF_MODEL_PATH = Path(cfg['DEFAULT']['shelf_model_path'])


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('-i', '--input-dir', help='input-dir with files to predict on', required=True)
    parser.add_argument('-o', '--output-jsonl', help='Output jsonl to store predictions', required=True)
    parser.add_argument('-t', '--trunc-index', help='index to truncate dataset', type=int, default=None)
    parser.add_argument('-b', '--batch-size', help='Batch size for prediction', type=int, default=8)
    parser.add_argument('-u', '--undistorted-image-dir', help='Dir to place undistorted images', default=None)
    return parser.parse_args()


class BottleComparison(BaseModel):
    NOT_IN_CATALOG: ClassVar[list] = ['pet', 'can', 'tetra', 'rgb', 'object', 'grpobj']
    NON_COKE_BRANDS: ClassVar[list] = ['Pepsi', 'Frooti', 'Slice', 'Sting', 'Rio', 'Mirinda', 'SunrichWater', 'Amul', 'Duke']

    gt: Optional[PredictedBottle] = None
    gt_index: Optional[int] = None
    pred: Optional[PredictedBottle] = None
    pred_index: Optional[int] = None

    def is_gt_non_coke(self):
        brand = self.gt.brand.lower()
        if brand in self.NOT_IN_CATALOG:
            return True
        for m in self.NON_COKE_BRANDS:
            if m.lower() in brand or brand in m.lower():
                return True
        return False

    def is_correct(self, comparison_type):
        if self.gt is None:
            logger.warning(f"GT is None")
            return None
        if comparison_type == 'all':
            if self.pred is None:
                return False
            if self.pred.code is None:
                return False
            # special handling for the case where the code is bottle/can/tetra/gbottle but code shows volume also
            if self.gt.code in self.NOT_IN_CATALOG:
                # logger.error(f"{self.gt.code} is not in catalog. gt brand: {self.gt.brand.lower()}: gt code: {self.gt.code} pred brand: {self.pred.brand.lower()} pred code: {self.pred.code}")
                return self.gt.brand.lower() == self.pred.brand.lower()
            return self.gt.code == self.pred.code
        elif comparison_type in ['catalog_only', 'catalog_only_without_volume']:
            if self.gt.brand.lower() in self.NOT_IN_CATALOG:
                return True
            if self.pred is None:
                return False
            if self.pred.code is None:
                return False
            if comparison_type == 'catalog_only':
                return self.gt.code == self.pred.code
            else:
                return self.gt.brand.lower() == self.pred.brand.lower()
        elif comparison_type == 'coke_only':
            if self.is_gt_non_coke():
                return True
            if self.pred is None:
                return False
            if self.pred.code is None:
                return False
            if self.gt.brand.lower() == self.pred.brand.lower():
                return True
            return False
        else:
            raise ValueError(f"Invalid comparison type: {comparison_type}")


class ShelfComparison(BaseModel):
    overlaps: Optional[List[BottleComparison]] = None
    others: Optional[List[BottleComparison]] = None
    all: Optional[List[BottleComparison]] = None # automatically calculated

    @model_validator(mode="before")
    @classmethod
    def calculate_c(cls, values):
        all = []
        all.extend(values['overlaps'])
        all.extend(values['others'])
        values['all'] = sorted(all, key=lambda x: x.gt.bbox.x1 if x.gt is not None else 0)
        return values

class ChillerComparison(BaseModel):
    shelves: List[ShelfComparison]

    def get_accuracy(self, comparison_type, shelf_clip_index):
        correct = 0
        total = 0
        mappings = []
        for shelf_i, shelf_comparison in enumerate(self.shelves):
            if shelf_i >= shelf_clip_index:
                break

            shelf_comparison_output = []
            for bottle_comparison in shelf_comparison.all:
                if comparison_type == 'coke_only' and bottle_comparison.is_gt_non_coke():
                    # shelf_comparison_output.append(None)
                    continue
                comparison_result = bottle_comparison.is_correct(comparison_type)
                shelf_comparison_output.append(comparison_result)
                if comparison_result:
                    correct += 1
                total += 1
            mappings.append(shelf_comparison_output)
            
        if total > 0:
            pct = 100 * correct / total
        else:
            pct = 0
        return pct, correct, total, mappings
    
    def get_counts_per_brand(self, comparison_type, shelf_clip_index, correct_counts: Counter, total_counts: Counter):
        pct, correct, total, mappings = self.get_accuracy(comparison_type, shelf_clip_index)
        if correct_counts is None:
            correct_counts = Counter()
        if total_counts is None:
            total_counts = Counter()
        for shelf_mappings, shelf in zip(mappings, self.shelves):
            bottle: BottleComparison
            for bottle_mapping, bottle in zip(shelf_mappings, shelf.all):
                brand, volume, sku_type = code2brandVolumeType(bottle.gt.code)
                brand_n_type = f"{bottle.gt.sku_type}-{brand}"
                # logger.error(f"{brand_n_type}<{bottle_mapping}>: {bottle.gt.code} --> {brand}: {volume}: {sku_type}")
                if bottle_mapping:
                    correct_counts.update([brand_n_type])
                total_counts.update([brand_n_type])
        return correct_counts, total_counts


class PredictedShelf:
    pastel_colors_rgb = [
        (193, 182, 255),  # Light Pink
        (225, 228, 255),  # Misty Rose
        (215, 235, 250),  # Antique White
        (140, 230, 240),  # Khaki
        (250, 230, 230),  # Lavender
        (245, 240, 255),  # Lavender Blush
        (221, 160, 221),  # Plum
        (216, 191, 216),  # Thistle
        (173, 222, 255),  # Navajo White
        (230, 224, 176),  # Powder Blue
        (152, 251, 152),  # Pale Green
        (213, 239, 255),  # Papaya Whip
        (220, 248, 255),  # Cornsilk
        (255, 255, 240),  # Azure
        (240, 255, 240),  # Honeydew
        (205, 250, 255),  # Lemon Chiffon
        (230, 240, 250),  # Linen
        (220, 245, 245),  # Beige
        (179, 222, 245),  # Wheat
        (181, 228, 255)   # Moccasin
    ]

    def __init__(self, bgr_image, orig_points, label, height, width, shelf_height=None, shelf_width=None):
        self.APPLY_CLAHE_THRESHOLD = 40
        self.image = bgr_image
        self.orig_points = orig_points
        self.label = label
        self.shelf_height = shelf_height
        self.shelf_width = shelf_width
        self.new_height = height
        self.new_width = width
        self.transformer = PerspectiveTransformer(debug=False)
        # self.corners = self._get_corners(self.orig_points)
        self.corners = self.transformer.get_polygon(np.array(self.orig_points))

        self.min_x = min([point[0] for point in self.orig_points])
        self.max_x = max([point[0] for point in self.orig_points])
        self.min_y = min([point[1] for point in self.orig_points])
        self.max_y = max([point[1] for point in self.orig_points])

        fix_lighting = False

        self.M, self.transformed_image = self.transformer.get_transform_params(self.image, self.corners, height=height, width=width, fix_lighting=fix_lighting)
        self.output_image = cv2.cvtColor(self.transformed_image, cv2.COLOR_RGB2BGR)
        self.transformed_height, self.transformed_width, _ = self.output_image.shape
        self.bottles = None
        self.bottle_image = None
        self.facings = None
        self.depth_image = None
        # self.depth = 0
        self.occupancy = None

    def transform_points(self, points):
        if isinstance(points, list):
            points = np.array(points).reshape(-1, 1, 2).astype(np.float32)
        else:
            points = points.reshape(-1, 1, 2).astype(np.float32)
        transformed_points = cv2.perspectiveTransform(points, self.M)
        # Clip the x coordinates to [0, new_width]
        transformed_points[:, 0, 0] = np.clip(transformed_points[:, 0, 0], 0, self.new_width)
        # Clip the y coordinates to [0, new_height]
        transformed_points[:, 0, 1] = np.clip(transformed_points[:, 0, 1], 0, self.new_height)
        transformed_points = transformed_points.astype(np.uint32)
        transformed_points = transformed_points.squeeze(axis=1).tolist()
        return transformed_points

    def contains_point(self, point):
        x, y = point
        return self.min_x <= x <= self.max_x and self.min_y <= y <= self.max_y
    
    def crop_bbox(self, bbox: Union[BBox, list]):
        if isinstance(bbox, list):
            [[x1, y1], [x2, y2]] = bbox
        else:    
            x1, y1, x2, y2 = bbox.x1, bbox.y1, bbox.x2, bbox.y2
        x1 = max(0, x1)
        x2 = min(self.new_width, x2)
        y1 = max(0, y1)
        y2 = min(self.new_height, y2)
        if isinstance(bbox, list):
            new_bbox = [[x1, y1], [x2, y2]]
        else:
            new_bbox = BBox(x1=x1, y1=y1, x2=x2, y2=y2)
        return new_bbox
    
    def find_closest_sku(self, tl, br):
        ## calculate distance from center of each bottle to the center of the rectangle
        x1, y1 = tl
        x2, y2 = br
        center_x = (x1 + x2) / 2
        center_y = (y1 + y2) / 2
        distances = []
        indexes = []
        if self.bottles is None or len(self.bottles) == 0:
            return None
        for i, bottle in enumerate(self.bottles):
            x, y = bottle.bbox.center
            distance = np.sqrt(np.square(center_x - x) + np.square(center_y - y))
            distances.append(distance)
            indexes.append(i)
        indexes = [i for _, i in sorted(zip(distances, indexes))]
        index = indexes[0]
        bottle = self.bottles[index]
        return bottle, index
    
    def _find_matching_bottle(self, other_bottle: PredictedBottle, mapped_indexes: List[int]):
        b: PredictedBottle
        for i, b in enumerate(self.bottles):
            if i in mapped_indexes:
                continue
            if other_bottle.has_overlap(b, margin=0.5):
                return b, i
        return None, None

    def compare(self, other):
        bottle: PredictedBottle
        overlaps, others = [], []
        mapped_indexes = []
        for i, bottle in enumerate(self.bottles):
            other_bottle, index = other._find_matching_bottle(bottle, mapped_indexes)
            if other_bottle is None:
                c = BottleComparison(gt=bottle, gt_index=i, pred=None)
                others.append(c)
            else:
                c = BottleComparison(gt=bottle, gt_index=i, pred=other_bottle, pred_index=index)
                overlaps.append(c)
                mapped_indexes.append(index)
        return ShelfComparison(overlaps=overlaps, others=others)

            
    def draw_bottles(self):
        if self.transformed_image is None:
            return None
        if self.bottles is None or len(self.bottles) == 0:
            return cv2.cvtColor(self.transformed_image, cv2.COLOR_RGB2BGR)
        # image = self.transformed_image.copy()
        image = cv2.cvtColor(self.transformed_image, cv2.COLOR_RGB2BGR)
        mask = np.zeros_like(image, dtype=np.uint8)
        for bottle in self.bottles:
            pastel_color = random.choice(self.pastel_colors_rgb)
            contour = np.array(bottle.polygon, dtype=np.int32)
            cv2.fillPoly(mask, [contour], pastel_color)
            cv2.drawContours(image, [contour], 0, color=pastel_color, thickness=2)
            x, y, w, h = cv2.boundingRect(contour)
            cv2.rectangle(image, (x, y), (x+w, y+h), pastel_color, 2)

            font = cv2.FONT_HERSHEY_SIMPLEX
            font_scale = 0.3
            font_thickness = 1
            text_color = (0, 0, 0)  # Black text
            text = bottle.code
            (text_width, text_height), baseline = cv2.getTextSize(text, font, font_scale, font_thickness)
            background_color = pastel_color
            x1 = int(x + w/6)
            y1 = int(y + h - text_height)
            cv2.rectangle(image, (x1, y1 - text_height - baseline), (x1 + text_width, y1 + baseline), background_color, -1)
            cv2.putText(image, text, (x1, y1), font, font_scale, text_color, font_thickness, cv2.LINE_AA)
            
        alpha = 0.15  # Transparency factor (0 is fully transparent, 1 is fully opaque)
        translucent_image = cv2.addWeighted(image, 1 - alpha, mask, alpha, 0)
        translucent_image = cv2.drawContours(translucent_image, [contour], 0, color=pastel_color, thickness=2)
        
        return translucent_image
    
    def _get_corners(self, points):
        r1 = cv2.minAreaRect(np.array(points))
        box = cv2.boxPoints(r1)
        box = [[b] for b in box]
        corners = np.int0(box).squeeze(axis=1).tolist()
        return corners
    
    def _get_angle(self, p1, p2):
        dx = abs(p2[0] - p1[0])
        if dx == 0:
            return 0
        dy = abs(p2[1] - p1[1])
        radians = np.arctan(dy/dx)
        degrees = np.degrees(radians)
        return degrees
    
    def get_shelf_angles(self, x_axis=True):
        if x_axis:
            a1 = self._get_angle(self.corners[0], self.corners[1])
            a2 = self._get_angle(self.corners[3], self.corners[2])
        else:
            a1 = self._get_angle(self.corners[0], self.corners[3])
            a2 = self._get_angle(self.corners[1], self.corners[2])
        axis_string = "x_axis" if x_axis else "y_axis"
        logger.debug(f"corners: {self.corners} angles: {a1}, {a2} axis: {axis_string}")
        return a1, a2
    
    def x_angle_in_range(self, min_angle, max_angle):
        a1, a2 = self.get_shelf_angles(x_axis=True)
        return min_angle <= a1 <= max_angle and min_angle <= a2 <= max_angle
    
    def y_angle_in_range(self, min_angle, max_angle):
        a1, a2 = self.get_shelf_angles(x_axis=False)
        return min_angle <= a1 <= max_angle and min_angle <= a2 <= max_angle
    
    def _get_distance(self, p1, p2):
        return np.sqrt(np.square(p2[0] - p1[0]) + np.square(p2[1] - p1[1]))
    
    def get_shelf_widths(self):
        w1 = self._get_distance(self.corners[0], self.corners[1])
        w2 = self._get_distance(self.corners[2], self.corners[3])
        return w1, w2
    
    def to_dict(self):
        # Do not touch this, it is used by inference api
        shelf_dict = {}
        shelf_dict['label'] = self.label
        bottles = []
        bottle: PredictedBottle
        for bottle in self.bottles:
            bottles.append(bottle.to_dict())
        if len(bottles) > 0:
            shelf_dict['bottles'] = bottles
        shelf_dict['occupancy'] = self.occupancy
        return shelf_dict
    
    def to_prodigy_spans(self):
        spans = []
        bottle: PredictedBottle
        for bottle in self.bottles:
            span = bottle.to_prodigy_dict()
            spans.append(span)
        return spans
    

class PredictedChiller:
    def __init__(self, shelves: List[PredictedShelf], shelf_output_image, image_path=None):
        self.shelves = shelves
        if len(shelves) == 0:
            self.output_image = None
        else:
            self.output_image = np.concatenate([shelf.bottle_image for shelf in shelves], axis=0)
        if len(shelves) == 0:
            self.grid_image = None
        else:
            self.grid_image = np.concatenate([shelf.output_image for shelf in shelves], axis=0)
        self.shelf_output_image = shelf_output_image
        self.image_path = image_path
        self.depth_image = None

    def get_per_shelf_string(self):
        per_shelf = []
        for shelf in self.shelves:
            per_shelf.append(f"{shelf.label}: {shelf.facings}")
        return per_shelf
    
    def to_list(self):
        # logger.info(f"-"*120)
        # logger.info(f"num shelves: {len(self.shelves)}")
        shelves = []
        for shelf in self.shelves:
            shelves.append(shelf.to_dict())
        return shelves
    
    def to_prodigy(self, shelf_only=True):
        prodigy_label = {}
        prodigy_label["image"] = str(self.image_path)
        prodigy_label["path"] = str(self.image_path)

        spans = []
        if shelf_only:
            for shelf in self.shelves:
                span = {}
                span["label"] = shelf.label
                span["points"] = shelf.orig_points
                span["type"] = "polygon"
                spans.append(span)
        else:
            for shelf in self.shelves:
                shelf_spans = shelf.to_prodigy_spans()
                spans.extend(shelf_spans)

        prodigy_label["spans"] = spans
        return prodigy_label
    
    def save_bottle_sub_images(self, output_dir, prefix="", threshold=None, gt=False):
        num_images = 0
        for shelf_i, shelf in enumerate(self.shelves):
            if shelf_i == 0:
                # chiller_image = cv2.cvtColor(shelf.image, cv2.COLOR_RGB2BGR)
                chiller_dir = output_dir / "chiller_images"
                if not chiller_dir.is_dir():
                    chiller_dir.mkdir()
                output_path = chiller_dir / f"{prefix}_chiller.jpg"
                cv2.imwrite(str(output_path), shelf.image)
            bottle: PredictedBottle
            shelf: PredictedShelf
            for bottle_i, bottle in enumerate(shelf.bottles):
                # bottle_image = cv2.cvtColor(np.array(shelf.output_image.copy()), cv2.COLOR_RGB2BGR)
                bottle_image = shelf.transformed_image.copy()
                bottle_image = get_sub_image_from_points(bottle_image, bottle.polygon)
                if bottle_image is None:
                    logger.warning(f"{output_path.name}: s[{shelf_i}][b{bottle_i}]: bottle_image is None: prefix: [{prefix}] bottle: {bottle.code} shelf: {shelf.label}")
                    continue
                if gt == True:
                    cat_dir = output_dir / f"{bottle.sku_type}" / f"{bottle.brand}"
                else:
                    if threshold is None:
                        cat_dir = output_dir / f"{bottle.sku_type}"
                    else:
                        bottle: PredictedBottle
                        if bottle.brand_confidence > threshold:
                            cat_dir = output_dir.parent / f"{bottle.sku_type}" / f"{bottle.brand}"
                        else:
                            cat_dir = output_dir / f"{bottle.sku_type}" 
                if not cat_dir.is_dir():
                    cat_dir.mkdir(parents=True)
                output_path = cat_dir / f"{prefix}_s{shelf_i}_b{bottle_i:02d}.jpg"
                if output_path.exists():
                    logger.error(f"{output_path.name} already exists ... overwriting")
                logger.debug(f"{output_path.name}: bottle_image shape: {bottle_image.shape} height: {bottle.height} width: {bottle.width}")
                cv2.imwrite(str(output_path), bottle_image)
                logger.debug(f"saved image: {output_path} score: {bottle.brand_confidence} threshold: {threshold}")
                num_images += 1
        return num_images

    def compare(self, other) -> ChillerComparison:
        shelves = []
        for gt_shelf, pred_shelf in zip(self.shelves, other.shelves):
            shelf_comparison: ShelfComparison = gt_shelf.compare(pred_shelf)
            shelves.append(shelf_comparison)
        chiller_comparison = ChillerComparison(shelves=shelves)
        return chiller_comparison

    def get_accuracy(self, other, shelf_clip_index=3, comparison_type='coke_only'):
        accuracy, _, _, _ = self.get_accuracy_params(other, shelf_clip_index=shelf_clip_index, comparison_type=comparison_type)
        return accuracy
    
    def draw_bottles(self):
        if self.output_image is None:
            return None
        if len(self.shelves) == 0:
            return self.output_image
        images = [shelf.draw_bottles() for shelf in self.shelves]
        image = np.concatenate(images)
        return image
        

class ShelfPredictor:
    def __init__(self, model_path=None, clip_index=3, threshold=0.8, fixed_width_transform=False, sort_fn=None, debug=False):
        if model_path is None:
            self.model_path = Path('/media/akshay/datasets/coke/models/shelf2024-March24-01/shelf2024-output/model_final.pth')
        else:
            self.model_path = Path(model_path)
        if not self.model_path.is_file():
            raise Exception(f"Unable to open: {self.model_path}")
        
        self.SHELF_WIDTH_PIXELS_POST_TRANSFORM = 800
        self.fixed_width_transform = fixed_width_transform

        # self.labels=["shelf1", "shelf2", "shelf3", "shelf4", "shelf5", "shelf6"]
        self.shelf_predictor = SegmentationPredictor(model_path=self.model_path, threshold=threshold, sort_fn=sort_fn, debug=debug)
        # logger.info(f"loaded shelfModel: {self.model_path}")
        self.clip_index = clip_index
        self.undistorter = Undistorter()
        self.perspective_transformer = PerspectiveTransformer()
        self.debug = debug

    def _remove_outlier_size_detections(self, detections: List[Detection]):
        if len(detections) < 4:
            logger.debug(f"Skipping small detection removal: {len(detections)} < 4")
            return detections
        heights = [d.height for d in detections]
        median_height = np.median(heights)
        filtered = []
        THRESHOLD_MIN = 0.5 * median_height
        THRESHOLD_MAX = 3 * median_height
        for detection in detections:
            if THRESHOLD_MIN <= detection.height <= THRESHOLD_MAX:
                filtered.append(detection)
            else:
                if detection.height < THRESHOLD_MIN:
                    logger.warning(f"removing small detection: {detection.label} height: {detection.height} < {THRESHOLD_MIN}")
                elif detection.height > THRESHOLD_MAX:
                    logger.warning(f"removing small detection: {detection.label} height: {detection.height} > {THRESHOLD_MAX}")
                else:
                    raise ValueError(f"Invalid height: {detection.height}")
        return filtered

    def _remove_duplicate_detections(self, detections):
        if len(detections) < 4:
            logger.debug(f"Skipping duplicate detection removal: {len(detections)} < 4")
            return detections
        filtered = []
        for detection in detections:
            if detection.label not in [f.label for f in filtered]:
                filtered.append(detection)
            else:
                logger.warning(f"removing duplicate detection: {detection.label}")
        return filtered

    def predict(self, image_or_path, shelf_heights, shelf_widths, overlap_margin=0.50, undistort=True):
        output_dir = Path(__file__).parent.parent / "data" / "trace"
        if not output_dir.is_dir():
            output_dir.mkdir(parents=True)

        image = load_image(image_or_path)
        if undistort:
            image = self.undistorter.undistort_image(image)
        detections, output_image = self.shelf_predictor.predict(image, output_image=True, overlap_margin=overlap_margin, sort_fn=lambda shelf: shelf.min_y)
        shelves, output_image = self._get_shelves(detections, image, output_image, shelf_heights, shelf_widths)
        return shelves, output_image

    def _get_shelves(self, detections, image, output_image, shelf_heights, shelf_widths):
        # fix labels:
        logger.info(f"shelf heights: {shelf_heights} shelf widths: {shelf_widths} output_image shape: {output_image.size}")
        detections = self._remove_outlier_size_detections(detections)
        for i, detection in enumerate(detections):
            if detection.label != f"shelf{i+1}":
                logger.warning(f"changing label from: {detection.label} to: shelf{i+1}")
                detection.label = f"shelf{i+1}"
        detections = self._remove_duplicate_detections(detections)
        string = ", ".join([f"[{d.label}:{d.pred_score:0.2f}]" for d in detections])
        logger.info(f"After removing duplicates: {string}")

        shelves = []
        # heights = [33.5, 29, 40, 27, 29]
        # widths = [50, 50, 50, 50, 50]
        if shelf_heights is None:
            heights = self.shelf_setting.heights
            logger.warning(f"shelf heights not found. Using default shelf heights: {heights}")
        else:
            heights = shelf_heights
        if shelf_widths is None:
            widths = self.shelf_setting.widths
            logger.warning(f"shelf widths not found. Using default shelf widths: {widths}")
        else:
            widths = shelf_widths
        detections = sorted(detections, key=lambda detection: detection.min_y)
        if len(detections) == 0:
            logger.error(f"No detections found")
            return [], output_image
        min_detection_width = min([d.width for d in detections])
        for i, (detection, shelf_height, shelf_width) in enumerate(zip(detections, shelf_heights, shelf_widths)) :
            # logger.debug(f"detection[{i}]: {detection.label} detection.min_y: {detection.min_y}")
            # TODO: remove this hardcoding. It should be based on detections
            if self.fixed_width_transform:
                width = self.SHELF_WIDTH_PIXELS_POST_TRANSFORM
            else:
                width = min_detection_width
            if i > len(heights) - 1:
                logger.warning(f"detection[{i}] is greater than shelves: {len(heights)}")
                i = len(heights) - 1
            height = int(heights[i] / widths[i] * width)
            
            p = PredictedShelf(image, detection.polygon, detection.label, height=height, width=width, shelf_height=shelf_height, shelf_width=shelf_width)
            logger.debug(f"shelf[{i}]: heightxwidth: [{height}x{width}] detection heightxwidth: [{detection.height}x{detection.width}] corners: {p.corners}")
            shelves.append(p)

        if self.debug:
            for shelf in shelves:
                logger.debug(f"{shelf.label}.min_y: {shelf.min_y}")
        shelves = sorted(shelves, key=lambda shelf: shelf.min_y)

        for shelf in shelves:
            output_dir = Path(__file__).parent.parent / "data" / "trace"
            cv2.imwrite(str(output_dir / f"{shelf.label}.jpg"), shelf.transformed_image)
        return shelves[:self.clip_index], output_image
    
    def predict_batch(self, images, batch_shelf_heights, batch_shelf_widths, overlap_margin=0.50):
        batch_shelves = []
        batch_output_images = []
        if self.undistort:
            images = [self.undistorter.undistort_image(image) for image in images]
        batch_detections, batch_detection_output_images = self.shelf_predictor.predict_batch(images, overlap_margin)
        for image, detections, detection_output_image, shelf_heights, shelf_widths in zip(images, batch_detections, batch_detection_output_images, batch_shelf_heights, batch_shelf_widths):
            shelves, output_image = self._get_shelves(detections, image, detection_output_image, shelf_heights, shelf_widths)
            batch_shelves.append(shelves)
            batch_output_images.append(output_image)
        return batch_shelves, batch_output_images
    
def save_image(image, path):
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    cv2.imwrite(str(path), image)

def save(batch_shelves, batch_output_images, file_handle):
    for shelves in batch_shelves:
        for shelf in shelves:
            shelf_dict = shelf.to_dict()
            file_handle.write(f"{shelf_dict}\n")


def main():
    args = parse_arguments()
    input_path = Path(args.input_dir)
    output_path = Path(args.output_jsonl)
    if args.undistorted_image_dir is not None:
        undistort_dir = Path(args.undistorted_image_dir)
        if not undistort_dir.is_dir():
            undistort_dir.mkdir(parents=True)
            logger.info(f"Created undistort_dir: {undistort_dir}")
    else:
        undistort_dir = None
    trunc_index = args.trunc_index
    batch_size = args.batch_size

    files = sorted(list(input_path.rglob("*.jpg")))
    logger.info(f"Found [{len(files)}] images to predict on")

    if trunc_index is not None:
        files = files[:trunc_index]        
        logger.info(f"Truncating to: {trunc_index} files")

    if len(files) == 0:
        logger.info(f"No files found in: {input_path} ... quitting")
        return
    predictor = ShelfPredictor(model_path=SHELF_MODEL_PATH, clip_index=5)
    
    num_batches = len(files) // batch_size
    if len(files) % batch_size != 0:
        num_batches += 1
    
    with open(output_path, 'w') as file:
        for i in tqdm(range(num_batches)):
            start = i * batch_size
            end = min(start + batch_size, len(files))
            batch_files = files[start:end]
            batch_shelf_settings = [STORE_MAPPER.get_shelf_settings(f) for f in batch_files]
            batch_shelf_heights = [shelf_setting.shelf_heights for shelf_setting in batch_shelf_settings]
            batch_shelf_widths = [shelf_setting.shelf_widths for shelf_setting in batch_shelf_settings]
            images = [load_image(f, bgr=False) for f in batch_files]
            if undistort_dir is not None:
                undistorted_image_paths = [undistort_dir / f.name for f in batch_files]
                undistorted_images = [predictor.undistorter.undistort_image(image) for image in images]
                for undistorted_image, undistorted_image_path in zip(undistorted_images, undistorted_image_paths):
                    save_image(undistorted_image, undistorted_image_path)
                images = undistorted_images
                batch_files = undistorted_image_paths

            batch_shelves, batch_output_images = predictor.predict_batch(images, batch_shelf_heights=batch_shelf_heights, 
                                                                         batch_shelf_widths=batch_shelf_widths, undistort=False)

            for image_path, shelves, output_image in zip(batch_files, batch_shelves, batch_output_images):
                for shelf in shelves:
                    o_height, o_width = shelf.output_image.shape[:2]
                    output_image = cv2.rectangle(np.array(output_image), (0, 0), (o_width, o_height), (0, 0, 0), thickness=6)
                    shelf.bottle_image = Image.fromarray(output_image)

                predictedChiller = PredictedChiller(shelves, output_image, image_path=str(image_path))
                prodigy_dict = predictedChiller.to_prodigy(shelf_only=True)
                line = json.dumps(prodigy_dict)
                file.write(f"{line}\n")

if __name__ == "__main__":
    main()