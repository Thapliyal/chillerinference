import streamlit as st
from pathlib import Path
import cv2
import pandas as pd
from loguru import logger


class ItemNavigator:
    def __init__(self, item_dir, display_image_callback, uploadable=False, upload_types=None):
        self.item_dir = Path(item_dir)
        self.display_image_callback = display_image_callback
        self.uploadable = uploadable
        if upload_types is None:
            self.upload_types = ["jpg"]
        else:
            self.upload_types = upload_types
        if "item_list" not in st.session_state:
            st.session_state.item_list = []

    def set_item_dir(self, item_dir):
        if self.uploadable:
            raise Exception("Cannot set item_dir when uploadable is enabled")
        self.item_dir = item_dir
        logger.info(f"item_dir set to: {self.item_dir}")

    def upload(self, bytes_data, file_name): 
        if not self.uploadable:
            return
        file_path = self.item_dir / file_name
        with open(file_path, "wb") as file:
            file.write(bytes_data)
            # st.info("")
            logger.debug(f"Uploaded file: {file_path}")

    def get_items(self):
        items = [f for f in sorted(self.item_dir.glob("*")) if f.is_file() and f.name[0] != "."]
        st.session_state.item_list = items
        logger.debug(f"Found [{len(items)}] items")
        return items

    def delete_files(self):
        if not self.uploadable:
            return
        items = self.get_items()
        deleted = len([f.unlink() for f in items])
        st.info(f"deleted [{deleted}] files")

    def on_click_prev(self):
        if st.session_state.index == 0:
            return
        st.session_state.index -= 1
        logger.debug(f"index decremented to: {st.session_state.index}")

    def on_click_next(self):
        if st.session_state.index == st.session_state.num_items - 1:
            return
        st.session_state.index += 1
        logger.debug(f"index incremented to: {st.session_state.index}")

    def on_slider_change(self):
        st.session_state.index = st.session_state.file_slider
        logger.debug(f"index changed to: {st.session_state.index}")

    def get_curr_item(self, items):
        if st.session_state.num_items == 0:
            return None
        curr_item = items[st.session_state.index]
        return curr_item
    
    def get_filename_from_item(self, item):
        return item.name
    
    def get_image_from_item(self, item):
        if item is None:
            return None
        input_image = cv2.imread(str(item))
        if input_image is None:
            return None
        input_image = cv2.cvtColor(input_image, cv2.COLOR_BGR2RGB)
        return input_image
    
    def get_item_names(self, items):
        names = [f.name for f in items]
        return names
    
    def display_dropdown(self):
        if self.uploadable:
            with st.expander("Upload Files"):
                uploaded_files = st.file_uploader(
                    "Choose a jpg file",
                    accept_multiple_files=True,
                    type=self.upload_types,
                    key="file_uploader",
                )

                for uploaded_file in uploaded_files:
                    bytes_data = uploaded_file.read()
                    self.upload(bytes_data, uploaded_file.name)

                items = self.get_items()
                logger.debug(f"Found [{len(items)}] items")
                names = self.get_item_names(items)
                frame = pd.DataFrame.from_dict({"File Name": names})
                st.table(frame)
                reset_button = st.button("DeleteFiles")
                if reset_button:
                    self.delete_files()

    def displayUI(self):    
        INDEX = "index"
        if INDEX not in st.session_state:
            st.session_state.index = 0

        NUM_FILES = "num_files"
        if NUM_FILES not in st.session_state:
            st.session_state.num_items = 0

        self.display_dropdown()
        self.display_item_navigation()

    def display_item_navigation(self):
        items = self.get_items()
        st.session_state.item_list = items
        st.session_state.num_items = len(items)
        if st.session_state.num_items == 0:
            st.warning("No files found in the directory")
            st.stop()

        file_name_holder = st.empty()
        col_prev, col_slider, col_next = st.columns([2, 10, 2])
        col_prev.button("Prev", on_click=self.on_click_prev)
        if st.session_state.num_items > 1:
            col_slider.slider(
                "Slider",
                min_value=0,
                max_value=st.session_state.num_items - 1,
                value=st.session_state.index,
                on_change=self.on_slider_change,
                key="file_slider",
            )
        col_next.button("Next", on_click=self.on_click_next)

        if len(items) > 0:
            curr_item = self.get_curr_item(items)
            file_name = self.get_filename_from_item(curr_item)
            file_name_holder.write(f"File: {file_name}")
            input_image = self.get_image_from_item(curr_item)
            if input_image is not None:
                if self.display_image_callback:
                    self.display_image_callback(input_image, curr_item)

