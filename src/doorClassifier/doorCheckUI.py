import streamlit as st
import requests
import json
import base64
import io
from PIL import Image
import numpy as np
from pathlib import Path
import cv2
import pandas as pd
from pydantic import BaseModel
import re
from classificationInference import ClassificationPredictor

# st.set_page_config(page_title="Chiller Inference", layout="wide")
st.set_page_config(page_title="Chiller Inference", layout="centered")


@st.cache_resource
def get_model():
    curr_dir = Path(__file__).parent.parent
    model_path = curr_dir.parent / "models" / "doorClassification.pth"
    categories = ["closed", "open"]
    model = ClassificationPredictor(model_path, categories)
    return model


@st.cache_resource
def get_logger():
    from loguru import logger

    log_file = LOG_DIR / "inferenceUI.log"
    logger.add(
        log_file,
        format="{time}|{file}|{line}|{thread}|{level}|{message}",
        colorize=True,
    )
    return logger

def upload(bytes_data, file_name): 
    global UPLOAD_DIR
    file_path = UPLOAD_DIR / file_name
    with open(file_path, "wb") as file:
        file.write(bytes_data)
        # st.info("")


def get_files():
    global UPLOAD_DIR
    return [f for f in sorted(UPLOAD_DIR.glob("*")) if f.is_file() and f.name[0] != "."]


def delete_files():
    files = get_files()
    deleted = len([f.unlink() for f in files])
    st.info(f"deleted [{deleted}] files")


def on_click_prev():
    if st.session_state.index == 0:
        return
    st.session_state.index -= 1
    logger.debug(f"index decremented to: {st.session_state.index}")


def on_click_next():
    if st.session_state.index == st.session_state.num_files - 1:
        return
    st.session_state.index += 1
    logger.debug(f"index incremented to: {st.session_state.index}")


def on_slider_change():
    st.session_state.index = st.session_state.file_slider
    logger.debug(f"index changed to: {st.session_state.index}")


def on_radio_change():
    st.session_state.cache_enabled = st.session_state.radio_cache_enabled


def change_language():
    pass
    st.session_state.language = st.session_state.lang_radio


# @st.cache_resource
# def load_image(file_path):
#     image = cv2.imread(file_path)
#     image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
#     return image


def get_curr_path(files):
    if st.session_state.num_files == 0:
        return None
    curr_path = files[st.session_state.index]
    return curr_path

####################################################################################################################
# Initializations
####################################################################################################################
# initialize()
# args = get_args()
# g_debug = args.debug
g_debug = True

BASE_DIR = Path(__file__).parent.parent.parent
LOG_DIR = BASE_DIR / "logs"
DATA_DIR = BASE_DIR / "data"
UPLOAD_DIR = DATA_DIR / "doorCheckData"
for dir in [DATA_DIR, LOG_DIR, UPLOAD_DIR]:
    if not dir.exists():
        dir.mkdir()

logger = get_logger()
predictor = get_model()
####################################################################################################################
# User Interface
####################################################################################################################
INDEX = "index"
if INDEX not in st.session_state:
    st.session_state.index = 0

NUM_FILES = "num_files"
if NUM_FILES not in st.session_state:
    st.session_state.num_files = 0

files = get_files()
st.session_state.num_files = len(files)


with st.expander("Upload Images"):
    uploaded_files = st.file_uploader(
        "Choose a jpg file",
        accept_multiple_files=True,
        type=["jpg"],
    )

    for uploaded_file in uploaded_files:
        bytes_data = uploaded_file.read()
        upload(bytes_data, uploaded_file.name)

    names = [f.name for f in files]
    frame = pd.DataFrame.from_dict({"File Name": names})
    st.table(frame)
    reset_button = st.button("DeleteFiles")
    if reset_button:
        delete_files()

col_prev, col_slider, col_next = st.columns([2, 10, 2])
col_prev.button("Prev", on_click=on_click_prev)
if st.session_state.num_files > 1:
    col_slider.slider(
        "Slider",
        min_value=0,
        max_value=st.session_state.num_files - 1,
        value=st.session_state.index,
        on_change=on_slider_change,
        key="file_slider",
    )
col_next.button("Next", on_click=on_click_next)

if len(files) > 0:
    curr_path = get_curr_path(files)
    if Path(curr_path).is_file():
        input_image = cv2.imread(str(curr_path))
        input_image = cv2.cvtColor(input_image, cv2.COLOR_BGR2RGB)
        logger.debug(f"curr_path: {curr_path}")
        placeholder = st.empty()
        st.image(input_image, use_column_width=True)
        output_class, confidence, _ = predictor.predict(input_image)
        placeholder.write(f"output: {output_class}, confidence: {confidence}")
        st.markdown("---")

