#!/usr/bin/env python3
import cv2
from pathlib import Path
# from argparse import ArgumentParser
from loguru import logger
import sys
from classificationInference import ClassificationPredictor
from fastapi import FastAPI, HTTPException, Body
from fastapi.responses import JSONResponse
import traceback

class StatusErrors(Exception):
    pass

class VideoDecodeError(StatusErrors):
    pass

class UnableToOpenFile(StatusErrors):
    pass


class DoorStatusPredictor:
    def __init__(self):
        curr_dir = Path(__file__).parent.parent
        model_path = curr_dir.parent / "models" / "doorClassification.pth"
        self.categories = ["closed", "open"]
        self.model = ClassificationPredictor(model_path, self.categories)

    def _get_frames(self, input_video):
        if not Path(input_video).is_file():
            logger.error(f"Unable to open: {input_video}")
            raise UnableToOpenFile(f"Unable to open: {input_video}")
        
        cap = cv2.VideoCapture(input_video)
        if not cap.isOpened():
            logger.error(f"Unable to open: {input_video}")
            sys.exit(1)

        frames = []
        while True:
            ret, frame = cap.read()
            if not ret:
                break
            frames.append(frame)
        cap.release()

        height, width = frames[0].shape[:-1]
        if height != 1600:
            frames = [cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE) for frame in frames]
        return frames
    
    def is_closed(self, input_video):
        frames = self._get_frames(input_video)
        if len(frames) == 0:
            logger.error(f"No frames found in: {input_video}")
            raise VideoDecodeError(f"No frames found in: {input_video}")
        
        results = []
        for frame in frames:
            status, score, _ = self.model.predict(frame)
            results.append(status)

        closed_ratio = len([result for result in results if result == "closed"]) / len(results)
        return closed_ratio > 0.75
    
    def are_closed(self, input_videos):
        results = []
        for input_video in input_videos:
            result = {}
            result["input_video"] = input_video
            try:
                output = self.is_closed(input_video)
                if output:
                    output = "closed"
                else:
                    output = "open"
                result["status"] = output
                result["error"] = ""
            except Exception as e:
                exception_message = str(e)
                exception_type = type(e).__name__
                traceback_info = traceback.format_exc()
                logger.error(f"Exception Type: {exception_type}")
                logger.error(f"Exception Message: {exception_message}")
                logger.error(f"Traceback Info:\n{traceback_info}")
                result["status"] = "error"
                if isinstance(e, VideoDecodeError):
                    result["error"] = exception_message
                else:
                    raise HTTPException(status_code=500, detail=exception_message)
            results.append(result)
                
        return results

predictor = DoorStatusPredictor()
app = FastAPI()


@app.post("/door_status")
async def door_status(input_videos: list[str] = Body(..., embed=False)):
    results = predictor.are_closed(input_videos)
    return JSONResponse(content=results)
