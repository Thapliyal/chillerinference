from pathlib import Path
import torch
import cv2


class ClassificationPredictor:
    def __init__(self, model_path, categories):
        self.model_path = Path(model_path)
        self.categories = categories
        self.id2category = {id: category for id, category in enumerate(self.categories)}
        self.category2id = {category: id for id, category in enumerate(self.categories)}
        if not self.model_path.is_file():
            raise Exception(f"Unable to open: {self.model_path}")
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else 'cpu')
        self.model = torch.load(self.model_path, map_location=self.device)

    def predict(self, image, debug=False):
        image = cv2.resize(image, (224, 224))
        t = torch.tensor(image).float()
        t = t.permute(2, 0, 1)
        t = t.to(self.device)
        t = t.unsqueeze(0)
        output = self.model(t)
        index = output[0].argmax().item()
        output_class = self.categories[index]
        confidences = torch.softmax(output[0], dim=0)
        if debug:
            print(f"output: {output[0].tolist()}")
            print(f"conf: {confidences.tolist()}")
        return output_class, confidences[index], confidences.tolist()