from typing import Annotated, List, Union, Optional
from fastapi import FastAPI, HTTPException, Body, UploadFile, File, Form
from fastapi.responses import JSONResponse
from fastapi import FastAPI, Request, Response, HTTPException
from pathlib import Path
import os
import json
from loguru import logger
from PIL import Image
import base64
import io
import numpy as np
import configparser
import traceback
from PIL import Image
from pathlib import Path
from shelfSettings import StoreManager, ShelfSettingException
from datetime import datetime
from classificationInference import VideoClassification, FrameSelector
from analyzeVideos import VideoManager, TOP_3_CLEAR, OPEN_BUT_NOT_TOP3_CLEAR
import configparser
from segmentationInference import SegmentationPredictor, code2brandVolumeType
from shelfPredictor import ShelfPredictor
from lensCorrector import LensCorrector
from perShelfPredictor import PerShelfChillerPredictor, PredictedChiller
from pydantic import BaseModel
from classificationInference import FrameClassifications
from typing import Optional, Union, List
from matplotlib import pyplot as plt
import numpy as np
from loguru import logger
import cv2
from utils import encode_image, Timer
from urllib import parse


app = FastAPI()
cfg = configparser.ConfigParser()
cfg.read('inference.ini')

BASE_DIR = Path(__file__).parent.parent
LOG_DIR = BASE_DIR / "logs"
if not LOG_DIR.exists():
    LOG_DIR.mkdir()
log_file = LOG_DIR / "salesMethod1Inference.log"

logger.add(
    log_file,
    format="{time}|{file}|{line}|{thread}|{level}|{message}",
    colorize=True,
)


class SalesMethod1Result(BaseModel):
    video_path: Path
    sale_status: str
    frame_classification: Optional[FrameClassifications] = None
    sales_frame_classification: Optional[FrameClassifications] = None
    transformed_detection_points: Optional[List[List[int]]] = None
    sales_shelf_label: Optional[str] = None
    sku_indexes: List[int] = []
    sku_codes: List[str] = []

    def to_dict(self, sales_frame, bottle_sold_image, sku_analysis_image):
        result = {}
        # result['sale_status'] = self.sale_status
        # if self.sale_status == "No sales frame found":
        #     result['status'] = 'sale'
        # if self.sale_status == "No shelves found in sales frame":
        #     result['status'] = 'noSale'
        result['status'] = self.sale_status

        if len(self.sku_indexes) > 0:
            result['sku_indexes'] = self.sku_indexes
            result['sku_codes'] = self.sku_codes
            sku_brands, sku_volumes, sku_types = [], [], []
            for sku_code in self.sku_codes:
                sku_brand, sku_volume, sku_type = code2brandVolumeType(sku_code)
                sku_brands.append(sku_brand)
                sku_volumes.append(sku_volume)
                sku_types.append(sku_type)
            result['sku_brands'] = sku_brands
            result['sku_volumes'] = sku_volumes
            result['sku_types'] = sku_types
        if self.sales_shelf_label is not None:
            result['sales_shelf_label'] = self.sales_shelf_label
        if sales_frame is not None:
            result['sales_image'] = encode_image(sales_frame)
        if bottle_sold_image is not None:
            result['bottle_sold_image'] = encode_image(bottle_sold_image)
        if sku_analysis_image is not None:
            result['sku_analysis_image'] = encode_image(sku_analysis_image)
        return result


class SalesMethod1:
    def __init__(self):
        cfg = configparser.ConfigParser()
        cfg.read('inference.ini')
        sales_frame_selector_path = Path(cfg['DEFAULT']['sales_frameselection_model_path'])
        self.sales_frame_selector = FrameSelector(sales_frame_selector_path, acceptable_categories=['certain'])
        segmentation_model_path = Path(cfg['DEFAULT']['sales_segmentation_model_path'])
        self.segmentation_model = SegmentationPredictor(model_path=segmentation_model_path, threshold=0.5)
    
        shelf_model_path = Path(cfg['DEFAULT']['shelf_model_path'])
        self.shelf_model = ShelfPredictor(shelf_model_path, clip_index=3, fixed_width_transform=True)
        self.lens_corrector = LensCorrector()
        self.store_manager = StoreManager(raise_exception=False)    

        frame_selector_path = Path(cfg['DEFAULT']['frameselection_model_path'])
        self.frame_selector = FrameSelector(frame_selector_path)
        shelf_model_path = Path(cfg['DEFAULT']['shelf_model_path'])
        bottle_model_path = Path(cfg['DEFAULT']['bottle_model_path'])
        brand_models_dir = Path(cfg['DEFAULT']['brand_models_dir'])
        volume_model_path = Path(cfg['DEFAULT']['volume_model_path'])
        bottle_model_threshold = float(cfg['DEFAULT']['bottle_model_threshold'])
        
        self.predictor = PerShelfChillerPredictor(shelf_model_path=shelf_model_path, bottle_model_path=bottle_model_path, 
                                             brand_models_dir=brand_models_dir, volume_model_path=volume_model_path, 
                                             shelf_clip_index=5, bottle_model_threshold=bottle_model_threshold, 
                                             debug=False, save_images_flag=False, fixed_width_transform=True)

    def get_video_classification(self, video_path, trunc_index):
        video_classification = self.frame_selector.select_from_video(video_path, trunc_index=trunc_index)
        # if video_classification is None:
            # logger.info("Unable to find a previous good frame, attempting with sales frame included")
            # video_classification = self.frame_selector.select_from_video(video_path, trunc_index=trunc_index, include_sales_frame=True)
        return video_classification

    def predict_chiller(self, video_path, sales_frame, sales_frame_index):
        frame_classification = self.get_video_classification(video_path, sales_frame_index)
        if frame_classification is None:
            # Unable to find a previous good frame, attempting with sales frame
            image = sales_frame.copy()
        else:
            image = frame_classification.frame_classifications.result_frame
        file_path = '/tmp/selected_frame.jpg'
        bgr_image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        cv2.imwrite(file_path, bgr_image)
        shelf_settings = self.store_manager.get_shelf_settings(video_path)
        if shelf_settings is None:
            return None
        chiller: PredictedChiller = self.predictor.predict(file_path, shelf_heights=shelf_settings.shelf_heights, shelf_widths=shelf_settings.shelf_widths)
        return chiller

    def get_sale(self, chiller, result):
        shelf_label = result.sales_shelf_label
        shelf_sku_analysis_image = chiller.output_image
        target_shelf = None
        for shelf in chiller.shelves:
            if shelf.label == shelf_label:
                target_shelf = shelf
                break
        if target_shelf is None:
            result.status = 'Unable to find target shelf'
            return result, None
            
        # shelf_sku_analysis_image = target_shelf.output_image.copy()
        # shelf_sku_analysis_image = np.array(target_shelf.bottle_image.copy())
        shelf_sku_analysis_image = target_shelf.draw_bottles()
        for points in result.transformed_detection_points:
            cv2.rectangle(shelf_sku_analysis_image, points[0], points[1], color=(255, 255, 255), thickness=4)

        for bbox in result.transformed_detection_points:
            bottle, index = target_shelf.find_closest_sku(bbox[0], bbox[1])
            if bottle is None:
                result.status = "No bottle detected"
                return result, shelf_sku_analysis_image

            result.sku_indexes.append(index)
            result.sku_codes.append(bottle.code)
        return result, shelf_sku_analysis_image
        
    def find_shelf(self, shelves, detection):
        p1 = [(detection.bbox.x1 + detection.bbox.x1)/2, (detection.bbox.y1 + detection.bbox.y2)/2]
        for shelf in shelves:
            if shelf.contains_point(p1):
                return shelf
        return None

    def transform_points(self, detections, shelves):
        orig_points = []
        transformed_points = []
        shelf = None
        for detection in detections:
            search = self.find_shelf(shelves, detection)
            if search is None:
                logger.warning(f"no shelf found for detection: {detection} .. skipping")
                continue
            if shelf is None:
                shelf = search
            if search != shelf:
                logger.warning(f"existing sale shelf: {shelf.label} is not found for detection: {detection} ... skipping")
                continue
            b = detection.bbox
            p = [[b.x1, b.y1], [b.x2, b.y2]]
            orig_points.append(p)
            tp = shelf.transform_points(p)
            cropped_tp = shelf.crop_bbox(tp)
            logger.opt(colors=True).info(f"<yellow> transforming detection bbox {shelf.label} {shelf.corners}: [{b}] -- transformed --> {tp} -- cropped --> {cropped_tp} </yellow>")
            transformed_points.append(cropped_tp)
            # logger.debug(f"drawing rectangle in shelf: {shelf.label} from {tp[0]} to {tp[1]}, transformed points: {tp} transformed_image shape: {shelf.transformed_image.shape}")
            # cv2.rectangle(shelf.transformed_image, tp[0], tp[1], color=(255, 0, 0), thickness=2)
            return transformed_points, search.label
        return None, None
        
        
    def process_sale(self, video_path):
        status = f"Shelf settings not found"
        video_path = Path(video_path)
        result = SalesMethod1Result(video_path=video_path, sale_status=status)

        shelf_settings = self.store_manager.get_shelf_settings(video_path)
        if shelf_settings is None:
            status = f"Shelf settings not found"
            result.sale_status = status
            return result, None, None, None
        
        ## Determine sales frame
        result.sale_status = f"Sales Detected"
        sales_video_classification = self.sales_frame_selector.select_from_video(video_path)
        result.sales_frame_classification = sales_video_classification
        if sales_video_classification is None:
            result.sale_status = f"No sales frame found"
            return result, None, None, None

        # Locate SKU sold in sales frame
        sales_frame_index = sales_video_classification.frame_classifications.result_index
        sales_frame = sales_video_classification.frame_classifications.result_frame.copy()
        sales_frame, _, _ = self.lens_corrector.correct(sales_frame)
        detections, segmentation_image = self.segmentation_model.predict(sales_frame, output_image=True, overlap_margin=0.2)
        if detections is None or len(detections) == 0:
            result.sale_status = f"Unable to detect SKU sold"
            return result, sales_frame, None, None
        logger.info(f"number of SKUs sold: {len(detections)}")
        
        for i, d in enumerate(detections):
            logger.info(f"Detection[{i}]: {d.bbox}: {d.pred_score}")
        bottle_sold_image = np.array(segmentation_image)

        # Locate shelves in sales frame
        shelves, sku_analysis_image = self.shelf_model.predict(sales_frame, shelf_heights=shelf_settings.shelf_heights, 
                                                   shelf_widths=shelf_settings.shelf_widths, undistort=False, overlap_margin=0.8)
        if shelves is None or len(shelves) == 0:
            result.sale_status = f"No shelves found in sales frame"
            return result, sales_frame, bottle_sold_image, None
        
        for shelf in shelves:
            if len(shelf.corners) != 4:
                logger.warning(f"skipping {shelf.label} which has {len(shelf.corners)} corners")
                continue
            polygon = shelf.corners
            for point in polygon:
                cv2.circle(bottle_sold_image, tuple(point), 10, (0, 255, 0), -1)
            cv2.polylines(bottle_sold_image, [np.array(polygon, np.int32)], isClosed=True, color=(0, 255, 0), thickness=2)

        result.transformed_detection_points, result.sales_shelf_label = self.transform_points(detections, shelves)
        if result.transformed_detection_points is None or len(result.transformed_detection_points) == 0:
            result.sale_status = f"Unable to transform detections"
            return result, sales_frame, bottle_sold_image, None
        
        # predict chiller
        chiller = self.predict_chiller(video_path, sales_frame, sales_frame_index)
        if chiller is None:
            result.sale_status = f"Unable to analyze chiller"
            return result, bottle_sold_image, None
        result, sku_analysis_image = self.get_sale(chiller, result)
        return result, sales_frame, bottle_sold_image, sku_analysis_image        

sales_endpoint = SalesMethod1()

@app.post("/salesMethodOneInference")
async def select_frame(video_path: str = Body(..., embed=True)):
    timer = Timer("salesMethodOneInference")
    timer.start()
    result = None
    video_path = parse.unquote(video_path)    
    logger.info(f"Got a request: {video_path}")
    try:
        sale_result, sales_frame, bottle_sold_image, sale_shelf_image = sales_endpoint.process_sale(video_path=video_path)
        result = sale_result.to_dict(sales_frame, bottle_sold_image, sale_shelf_image)
        timer.stop()
    except Exception as e:
        if result is None:
            result = {}
        exception_message = str(e)
        exception_type = type(e).__name__
        traceback_info = traceback.format_exc()
        logger.error(f"Exception Type: {exception_type}")
        logger.error(f"Exception Message: {exception_message}")
        logger.error(f"Traceback Info:\n{traceback_info}")
        result["status"] = "error"
        raise HTTPException(status_code=500, detail=exception_message)

    return JSONResponse(content=result)
