from pathlib import Path
import torch
import cv2
from loguru import logger
from pydantic import BaseModel
import numpy as np
from typing import Optional, List
from torchvision.transforms import v2
from PIL import Image
from transforms import VALIDATION_TRANSFORMS
from utils import encode_image
import shutil
import configparser
from utils import UnableToOpenFile


class FrameClassification(BaseModel):
    category: str
    score: float

class FrameClassifications(BaseModel):
    frame_classifications: List[FrameClassification]
    result_index: Optional[int]
    result_frame: Optional[np.ndarray] = None
    class Config:
        arbitrary_types_allowed = True
        json_encoders = {
                    # np.ndarray: lambda x: encode_image(x),
                    np.ndarray: lambda x: None,
                }


class VideoClassification(BaseModel):
    input_video: str
    frame_classifications: FrameClassifications


class ClassificationPredictor:
    def __init__(self, model_path, categories=None):
        self.model_path = Path(model_path)
        if not self.model_path.is_file():
            raise Exception(f"Unable to open: {self.model_path}")
        if categories is None:
            categories_path = self.model_path.parent / f"{model_path.stem}_categories.json"
            if not categories_path.is_file():
                raise Exception(f"Unable to open: {categories_path}")
            import json
            with open(categories_path) as f:
                self.categories = json.load(f)
                # logger.info(f"Loaded categories from: {categories_path} are: {self.categories}")
        else:
            self.categories = categories

        self.id2category = {id: category for id, category in enumerate(self.categories)}
        self.category2id = {category: id for id, category in enumerate(self.categories)}
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else 'cpu')
        self.model = torch.load(self.model_path, map_location=self.device)
        logger.info(f"Loaded model: {self.model_path} with categories: {self.categories} to: {self.device}")
        self.transform = VALIDATION_TRANSFORMS

    def predict(self, image, debug=False):
        if isinstance(image, np.ndarray):
            image = Image.fromarray(image)
        t = self.transform(image).to(self.device)
        t = t.unsqueeze(0)
        output = self.model(t)
        index = output[0].argmax().item()
        # logger.info(f"index: {index} categories: {self.categories}")
        output_class = self.categories[index]
        confidences = torch.softmax(output[0], dim=0)
        if debug:
            logger.debug(f"index: {index} class: {output_class} confidence: {confidences[index]} confidences: {confidences.tolist()}")
        return output_class, confidences[index].data.item(), confidences.tolist()    

    def predict_batch(self, images, debug=False):
        if isinstance(images[0], np.ndarray):
            images = [Image.fromarray(image) for image in images]

        t = torch.stack([self.transform(image) for image in images]).to(self.device)
        output = self.model(t)
        o_max = output.argmax(dim=1).detach().tolist()
        output_classes = [self.categories[index] for index in o_max]
        
        confidences = torch.softmax(output, dim=1)
        confidences = confidences.detach().tolist()
        max_confs = [confidences[i][index] for i, index in enumerate(o_max)]
        return output_classes, max_confs, confidences
    
class StatusErrors(Exception):
    pass

class VideoDecodeError(StatusErrors):
    pass


class ImageClassifier:
    def __init__(self, model_path):
        self.model_path = model_path
        # self.categories = categories
        assert self.model_path.is_file(), f"Unable to open: {self.model_path}"
        self.model = ClassificationPredictor(self.model_path)
        self.categories = self.model.categories
        assert self.categories and len(self.categories) > 0, "No categories provided"

    def get_frames(self, input_video):
        input_video_path = Path(input_video)
        if not input_video_path.is_file():
            logger.error(f"Unable to open: {input_video}")
            raise UnableToOpenFile(f"Unable to open: {input_video_path.name}")
        
        cap = cv2.VideoCapture(str(input_video))
        if not cap.isOpened():
            logger.error(f"Unable to open: {input_video}")
            raise UnableToOpenFile(f"Unable to open: {input_video_path.name}")

        frames = []
        while True:
            ret, frame = cap.read()
            if not ret:
                break
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frames.append(frame)
        cap.release()

        if len(frames) == 0:
            logger.error(f"No frames found in: {input_video}")
            raise VideoDecodeError(f"No frames found in: {input_video}")
        
        height, width = frames[0].shape[:-1]
        if height != 1600:
            frames = [cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE) for frame in frames]
        return frames
    
    def predict_video(self, input_video, batch=True, trunc_index=None):
        frames = self.get_frames(input_video)
        if trunc_index:
            logger.opt(colors=True).info(f"<yellow>Truncating frames to: {trunc_index} total frames: {len(frames)}</yellow>")
            frames = frames[:trunc_index]
        if len(frames) == 0:
            logger.error(f"No frames found in: {input_video}")
            raise VideoDecodeError(f"No frames found in: {input_video}")
        
        frame_results = self.predict_images(frames, batch=batch)
        return frame_results, frames
    
    def predict_image_paths(self, image_paths, batch_size=32):
        num_images = len(image_paths)
        frame_results = []
        for i in range(0, num_images, batch_size):
            batch_paths = image_paths[i:i+batch_size]
            frames = [cv2.imread(str(p)) for p in batch_paths]
            frame_results.extend(self.predict_images(frames, batch=True))
        return frame_results

    def predict_images(self, frames, batch=True) -> Optional[List[FrameClassification]]:
        if batch:
            b_category, b_confs, _ = self.model.predict_batch(frames)
        else:
            b_category, b_confs = self._predict_sequential(frames)
        frame_results = []
        for frame, category, conf in zip(frames, b_category, b_confs):
            result = {}
            # category, score, _ = self.model.predict(frame)
            result["frame"] = frame
            result["category"] = category
            result["score"] = conf
            classification = FrameClassification(**result)
            frame_results.append(classification)
        return frame_results
    
    def _predict_sequential(self, frames):
        frame_results = []
        for frame in frames:
            result = {}
            category, score, _ = self.model.predict(frame, debug=False)
            result["frame"] = frame
            result["category"] = category
            result["score"] = score
            classification = FrameClassification(**result)
            frame_results.append(classification)
        return frame_results


class   FrameSelector(ImageClassifier):
    def __init__(self, model_path=None, acceptable_categories=None):
        if model_path is None:
            cfg = configparser.ConfigParser()
            cfg.read('inference.ini')
            model_path = Path(cfg['DEFAULT']['frameselection_model_path'])
        else:
            model_path = Path(model_path)
        # self.categories = ['closedDoor', 'glareOrFog', 'notOpenEnough', 'tooObstructed', 'top3Clear']
        if acceptable_categories is None:
            self.acceptable_categories = ['allSKUsClear', 'top3Clear']
        else:
            self.acceptable_categories = acceptable_categories
        super().__init__(model_path)

    def _get_variance_of_laplacian(self, image):
        gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
        return cv2.Laplacian(gray, cv2.CV_64F).var()        
    
    def select_from_images(self, frames, batch=True):
        frame_results = super().predict_images(frames, batch=batch)
        index = self.select_frame(frame_results)
        if index is None:
            logger.warning(f"No good frames found")
            return None
        result = FrameClassifications(frame_classifications=frame_results, result_index=index, result_frame=frames[index])
        return result

    def select_from_video(self, input_video, batch=True, trunc_index=None, return_frames=False):
         frame_results, frames = super().predict_video(input_video, batch=batch, trunc_index=trunc_index)
         if frames is None or len(frames) == 0:
             if return_frames:
                 return None, None
             else:
                 return None
             
         index = self.select_frame(frame_results)
         
         if index is None:
            frame_classifications = FrameClassifications(frame_classifications=frame_results, result_index=None, result_frame=None) 
         else:
            frame_classifications = FrameClassifications(frame_classifications=frame_results, result_index=index, result_frame=frames[index])
         result = VideoClassification(input_video=str(input_video), frame_classifications=frame_classifications)
         if return_frames:
            return result, frames
         return result

    def select_frame(self, frame_results: List[FrameClassification]):
        for i, r in enumerate(frame_results):
            logger.info(f"[{i:02d}]: category: {r.category:16s} score: {r.score:.2f}")
        
        good_frames_indexes = []
        for preferred_category in self.acceptable_categories:
            good_frames_indexes = [i for i, frame_result in enumerate(frame_results) if frame_result.category == preferred_category]
            if len(good_frames_indexes) == 0:
                logger.opt(colors=True).info(f"<yellow>No frames found for category: {preferred_category}</yellow>")
                continue
            else:
                break

        if len(good_frames_indexes) == 0:
            logger.warning(f"No acceptable frames found")
            return None

        good_frame_scores = [frame_results[i].score for i in good_frames_indexes]
        index = good_frame_scores.index(max(good_frame_scores))
        actual_index = good_frames_indexes[index] 
        # index = len(good_frames_indexes) // 3
        # middle_frame_index = good_frames_indexes[index]
        logger.info(f"Selected frame: {actual_index}")
        return actual_index


class CategoryBasedSorter:
    def __init__(self, model_path: Path, progress_callback=None, threshold: float = 0.85):
        self.image_classifier = ImageClassifier(model_path)
        self.categories = self.image_classifier.categories
        self.threshold = threshold
        self.progress_callback = progress_callback

    def predict_image_paths(self, image_paths, batch_size=128, progress_callback=None, max_pct=90):
        num_images = len(image_paths)
        frame_results = []
        for i in range(0, num_images, batch_size):
            # logger.info(f"Processing images: {i} to {i+batch_size}")
            batch_paths = image_paths[i:i+batch_size]
            curr_results = self.image_classifier.predict_image_paths(batch_paths, batch_size=batch_size)
            frame_results.extend(curr_results)
            progress = int((i+1) * max_pct // num_images)
            if i % 100 == 0:
                logger.debug(f"Progress: {progress} %")
            if progress_callback:
                progress_callback(progress)
        if progress_callback:
            progress_callback(max_pct)
        return frame_results

    def sort(self, input_dir: Path, output_dir: Path, threshold=None, progress_callback=None):
        if not input_dir.is_dir():
            raise ValueError(f"Input directory does not exist: {input_dir}")
        if not output_dir.is_dir():
            output_dir.mkdir()
            logger.info(f"Created output directory: {output_dir}")

        if threshold is None:
            threshold = self.threshold
        if progress_callback is None:
            progress_callback = self.progress_callback
        
        file_paths = [f for f in sorted(input_dir.glob("*.jpg")) if f.is_file() and f.name[0] != "."]
        logger.info(f"Found [{len(file_paths)}] images in input_dir: {input_dir}")

        prediction_pct = 90
        classification_results = self.predict_image_paths(file_paths, batch_size=32, progress_callback=progress_callback, max_pct=prediction_pct)
        classification_result: FrameClassification
        logger.info(f"processing classification results: {len(classification_results)}")
        for i, (file_path, classification_result) in enumerate(zip(file_paths, classification_results)):
            logger.debug(f"Processing: {file_path.name} category: {classification_result.category} score: {classification_result.score:.2f}")
            if classification_result.score < threshold:
                category_dir = output_dir / "unsorted"
                if not category_dir.is_dir():
                    category_dir.mkdir()
                    logger.info(f"Created category directory: {category_dir}")
                output_file = category_dir / file_path.name
                # copy file to output dir using shtuil
                # logger.info(f"Copying: {file_path} to: {output_file}")
                shutil.copy(file_path, output_file)
                continue
            category_dir = output_dir / classification_result.category
            if not category_dir.is_dir():
                category_dir.mkdir()
                logger.info(f"Created category directory: {category_dir}")
            output_file = category_dir / file_path.name
            # copy file to output dir using shtuil
            # logger.info(f"Copying: {file_path} to: {output_file}")
            shutil.copy(file_path, output_file)
            if progress_callback:
                progress = prediction_pct + int(i * (100-prediction_pct) // len(file_paths))
                progress_callback(progress)
        
        subs = [f for f in output_dir.iterdir() if f.is_dir()]
        sort_result = {}
        for sub in subs:
            num_images = len(list(sub.glob('*')))
            logger.info(f"Category: {sub.name} has [{num_images}] images")
            sort_result[sub.name] = num_images

        progress_callback(100)
        return sort_result
