from typing import Optional
from fastapi import FastAPI, File, UploadFile, Form
from fastapi import FastAPI, Request, Response, HTTPException
from pathlib import Path
import os
import json
from loguru import logger
from PIL import Image
import base64
import io
import numpy as np
import configparser
import traceback
from PIL import Image

from perShelfPredictor import PerShelfChillerPredictor
from shelfPredictor import PredictedChiller
from utils import encode_image
from chillerDepth import DepthPredictor
from shelfSettings import StoreManager

STORE_MAPPER_WITHOUT_EXCEPTIONS = StoreManager(raise_exception=False)

app = FastAPI()
cfg = configparser.ConfigParser()
cfg.read('inference.ini')

BASE_DIR = Path(__file__).parent.parent
LOG_DIR = BASE_DIR / "logs"
if not LOG_DIR.exists():
    LOG_DIR.mkdir()
log_file = LOG_DIR / "inference.log"

logger.add(
    log_file,
    format="{time}|{file}|{line}|{thread}|{level}|{message}",
    colorize=True,
)

def get_chiller_predictor():
    shelf_model_path = Path(cfg['DEFAULT']['shelf_model_path'])
    bottle_model_path = Path(cfg['DEFAULT']['bottle_model_path'])
    brand_models_dir = Path(cfg['DEFAULT']['brand_models_dir'])
    volume_model_path = Path(cfg['DEFAULT']['volume_model_path'])
    bottle_model_threshold = float(cfg['DEFAULT']['bottle_model_threshold'])
    
    predictor = PerShelfChillerPredictor(shelf_model_path=shelf_model_path, bottle_model_path=bottle_model_path, 
                                         brand_models_dir=brand_models_dir, volume_model_path=volume_model_path, 
                                         shelf_clip_index=5, bottle_model_threshold=bottle_model_threshold, 
                                         debug=False, fixed_width_transform=False, save_images_flag=False)
    return predictor

predictor = get_chiller_predictor()
depth_predictor = DepthPredictor(predictor=predictor)
UPLOAD_DIR = Path(__file__).parent.parent/ "data" / "uploads"
if not UPLOAD_DIR.exists():
    UPLOAD_DIR.mkdir()

def parse_list_string(list_string: str):
    # TODO: add regex based checking here?
    l =  json.loads(list_string)
    assert isinstance(l, list)
    for i in l:
        if not isinstance(i, int) and not isinstance(i, float):
            raise ValueError("Invalid list")
    return l

@app.post("/analyzeChiller")
async def analyze_chiller(httprequest: Request, httpresponse: Response, file: UploadFile = File(...), 
                          shelf_heights: str = Form(...), shelf_widths: str = Form(...), 
                          video_dir: Optional[str] = Form(None), frame_name: Optional[str] = Form(None),
                          evaluate_depth: Optional[bool] = Form(True)):
# async def analyze_chiller(httprequest: Request, httpresponse: Response, shelf_heights: str, shelf_widths: str, file: UploadFile = File(...)):
# async def analyze_chiller(httprequest: Request, httpresponse: Response, file: Annotated[bytes, File()]):    
    ALLOWED_NUM_SHELVES = [4, 5, 6, 7]
    try:
        logger.info(f"="*160)
        logger.info(f"Got a request: shelf_heights: {shelf_heights}, shelf_widths: {shelf_widths}, file: {file.filename}")
        logger.info(f"video_dir: {video_dir}, frame_name: {frame_name}")

        shelf_settings = STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(file.filename)
        if shelf_settings is not None:
            shelf_heights = shelf_settings.shelf_heights
            shelf_widths = shelf_settings.shelf_widths
            logger.debug(f"From DB for {file.filename}: shelf_heights: {shelf_heights}, shelf_widths: {shelf_widths}")
        else:
            shelf_heights = parse_list_string(shelf_heights)
            shelf_widths = parse_list_string(shelf_widths)
            logger.debug(f"After parsing: shelf_heights: {shelf_heights}, shelf_widths: {shelf_widths}")
    
        if len(shelf_heights) not in ALLOWED_NUM_SHELVES or len(shelf_widths) not in ALLOWED_NUM_SHELVES:
            raise ValueError("Invalid shelf_heights or shelf_widths")

        file_path = Path(UPLOAD_DIR / f"{file.filename}")
        with open(file_path, "wb+") as file_object:
            file_object.write(file.file.read())
        chiller: PredictedChiller = predictor.predict(file_path, shelf_heights=shelf_heights, shelf_widths=shelf_widths, video_dir=video_dir, frame_name=frame_name)
        if evaluate_depth:
            chiller = depth_predictor.update_chiller(chiller)
        shelf_listing = chiller.to_list()
        image = chiller.draw_bottles()
        image = encode_image(image)
        shelf_image = encode_image(chiller.shelf_output_image)
        depth_image = encode_image(chiller.depth_image)
        grid_image = encode_image(chiller.grid_image)
        response = {"chiller": shelf_listing, "image": image, "shelf_image": shelf_image, "depth_image": depth_image, "grid_image": grid_image}
        return response
    except Exception as e:
        exception_message = str(e)
        exception_type = type(e).__name__
        traceback_info = traceback.format_exc()
        logger.error(f"Exception Type: {exception_type}")
        logger.error(f"Exception Message: {exception_message}")
        logger.error(f"Traceback Info:\n{traceback_info}")        
        raise HTTPException(status_code=500, detail=exception_message)
