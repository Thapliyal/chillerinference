import streamlit as st
from pathlib import Path
import cv2
from loguru import logger
import pandas as pd
from commonUI import ItemNavigator
from utils import get_video_frames, UnableToOpenFile
import traceback

@st.cache_resource
def get_logger(name):
    # from loguru import logger
    LOG_DIR = Path(__file__).parent.parent.parent
    log_file = LOG_DIR / f"{name}.log"
    logger.add(
        log_file,
        format="{time}|{file}|{line}|{thread}|{level}|{message}",
        colorize=True,
    )
    return logger


class VideoNavigator(ItemNavigator):
    def __init__(self, name="videoNavigator", display_image_callback=None):
        logger.info(f"Initializing VideoNavigator with name: {name}")
        self.name = name
        item_dir = Path(__file__).parent.parent / "data" / f"{self.name}"
        if not item_dir.is_dir():
            item_dir.mkdir()
        upload_types = ["mp4", "avi"]

        if display_image_callback is None:
            display_image_callback = self.display_buttons_and_image
        
        super().__init__(item_dir=item_dir, display_image_callback=display_image_callback, uploadable=True, upload_types=upload_types)

        if "num_frames" not in st.session_state:
            st.session_state.num_frames = 0
        if "frame_index" not in st.session_state:
            st.session_state.frame_index = None
        if "frames" not in st.session_state:
             st.session_state.frames = None

    def get_image_from_item(self, item):
        try:
            frames = get_video_frames(item)
            st.session_state.frames = frames
            st.session_state.num_frames = len(frames)
            return frames[st.session_state.frame_index]
        except UnableToOpenFile as e:
            exception_message = str(e)
            exception_type = type(e).__name__
            traceback_info = traceback.format_exc()
            logger.error(f"Exception Type: {exception_type}")
            logger.error(f"Exception Message: {exception_message}")
            logger.error(f"Traceback Info:\n{traceback_info}")

    def on_click_next_image(self):
        if st.session_state.frame_index == st.session_state.num_frames - 1:
            return
        st.session_state.frame_index += 1
        logger.debug(f"frame index incremented to: {st.session_state.frame_index}")

    def on_click_prev_image(self):
        if st.session_state.frame_index == 0:
            return
        st.session_state.frame_index -= 1
        logger.debug(f"frame index decremented to: {st.session_state.frame_index}")

    def reset_frame_state(self):
        st.session_state.frame_index = None
        st.session_state.num_frames = 0
        st.session_state.frames = None

    def on_frame_slider_change(self):
        st.session_state.frame_index = st.session_state.frame_slider
        logger.debug(f"frame index changed to: {st.session_state.frame_index}")

    def on_click_next(self):
        super().on_click_next()
        self.reset_frame_state()
    
    def on_click_prev(self):
        super().on_click_prev()
        self.reset_frame_state()

    def display_buttons_and_image(self, input_image, item):
        self.display_buttons()
        self.display_image(input_image, item)
    
    def display_buttons(self):
        col_prev, col_slider, col_next = st.columns([3, 8, 3])
        col_prev.button("PrevImage", on_click=self.on_click_prev_image, key='prev_image')
        if st.session_state.num_frames > 1:
            col_slider.slider(
                "FrameSlider",
                min_value=0,
                max_value=st.session_state.num_frames - 1,
                value=st.session_state.frame_index,
                on_change=self.on_frame_slider_change,
                key="frame_slider",
            )
        col_next.button("NextImage", on_click=self.on_click_next_image, key='next_image')
    
    def display_image(self, input_image, item):
        st.image(input_image, caption=item.name, use_container_width=True)


def main():
    name="videoNavigator"
    logger = get_logger(name)
    st.title("Video Navigator")
    video_navigator = VideoNavigator(name=name)
    video_navigator.displayUI()

if __name__ == "__main__":
    main()
