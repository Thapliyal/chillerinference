import cv2
import numpy as np
from pathlib import Path


INTRINSIC_MATRIX_160_FOV = np.array([[567.85175132,   0.        , 599.82685264], 
                             [  0.        , 568.51529038, 781.80197694], 
                             [  0.        ,   0.        ,   1.        ]])
DISTORTION_PARAMS_160_FOV = np.array([ 0.21041354, -0.22728782, -0.00024983,  0.00126894,  0.0552651 ])

INTRINSIC_MATRIX_200_FOV = np.array([[548.32691677,   0.        , 643.06277938],
                                     [  0.        , 555.64075818, 837.14705894],
                                     [  0.        ,   0.        ,   1.        ]])
DISTORTION_PARAMS_200_FOV = np.array([[-0.24504365,  0.05897783, -0.00157071,  0.00240543, -0.00608496]])


class CAMERA_TYPE:
    def __init__(self, intrinsic_matrix, distortion_params):
        self.intrinsic_matrix = intrinsic_matrix
        self.distortion_params = distortion_params
    

CAMERA_TYPE_160_FOV = CAMERA_TYPE(intrinsic_matrix=INTRINSIC_MATRIX_160_FOV, distortion_params=DISTORTION_PARAMS_160_FOV)
CAMERA_TYPE_200_FOV = CAMERA_TYPE(intrinsic_matrix=INTRINSIC_MATRIX_200_FOV, distortion_params=DISTORTION_PARAMS_200_FOV)


class LensCorrector:
    def __init__(self, camera_type=CAMERA_TYPE_200_FOV, alpha=0):
        self.camera_type = camera_type
        self.mtx = camera_type.intrinsic_matrix
        self.dist = camera_type.distortion_params
        self.w = 1200
        self.h = 1600
        self.newcameramtx, self.roi = cv2.getOptimalNewCameraMatrix(self.mtx, self.dist, (self.w,self.h), alpha=alpha, newImgSize=(int(1*self.w),int(1*self.h)))
        # print(self.roi)

    def _load(self, rgb_image_or_path):
        if isinstance(rgb_image_or_path, np.ndarray):
            return rgb_image_or_path
        elif isinstance(rgb_image_or_path, str) or isinstance(rgb_image_or_path, Path):
            image = cv2.imread(str(rgb_image_or_path))
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            return image
        raise Exception(f"Invalid rgb_image_or_path type: {type(rgb_image_or_path)}")

    def correct(self, rgb_image_or_path):
        image = self._load(rgb_image_or_path)
        mapped_image = cv2.undistort(image, self.mtx, self.dist, None, self.newcameramtx)
        x1, y1, w1, h1 = self.roi
        mapped_image = mapped_image[y1: y1+h1, x1: x1+w1].copy()
        return mapped_image, image, self.roi

    def correct_points(self, points):
        x1, y1, w1, h1 = self.roi
        points = np.array(points).astype(np.float32)
        # print(points.shape)
        new_points = cv2.undistortPoints(points, self.mtx, self.dist, None, self.newcameramtx)
        new_points = [[int(x) - int(x1), int(y) - int(y1)] for x, y in new_points.squeeze().tolist()]
        return new_points
    
    def draw_shelf(self, image, new_points):
        new_points = [[int(x), int(y)] for x, y in new_points]
        new_points.append(new_points[0])
        for p1, p2 in zip(new_points[0: -1], new_points[1:]):
            # print(f"drawing line from {p1} --> {p2}")
            cv2.circle(image, p1, radius=12, color=(255, 0, 0), thickness=2)
            cv2.line(image, p1, p2, color=(0, 0, 255), thickness=2)
        return image

    def get_prodigy_shelf_images(self, d):
        image_path = d['path']
        spans = d['spans']
        new_image, image, roi = self.correct(image_path)
        for span in spans:
            points = span['points']
            new_points = self.correct_points(points)
            print(points)
            print(new_points)
            image = self.draw_shelf(image, points)
            new_image = self.draw_shelf(new_image, new_points)
        return image, new_image


class Undistorter:
    def __init__(self, camera_type=None):
        if camera_type is None:
            camera_type = CAMERA_TYPE_200_FOV
        # if intrinsic_matrix is None:
        #     intrinsic_matrix = np.array([[567.85175132,   0.        , 599.82685264], 
        #                                  [  0.        , 568.51529038, 781.80197694], 
        #                                  [  0.        ,   0.        ,   1.        ]])
        # if distortion_params is None:
        #     distortion_params = np.array([ 0.21041354, -0.22728782, -0.00024983,  0.00126894,  0.0552651 ])
        self.M  = camera_type.intrinsic_matrix
        self.D = camera_type.distortion_params

    def undistort_image(self, image):
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        return cv2.undistort(image, self.M, self.D, None, self.M)

    def undistort_points(self, points):
        if not isinstance(points, np.ndarray):
            points = np.array(points).astype(np.float32)
        undistorted_points_normalized = cv2.undistortPoints(points, self.M, self.D)
        undistorted_points = cv2.convertPointsToHomogeneous(undistorted_points_normalized)
        undistorted_points = np.dot(self.M, undistorted_points.squeeze().T).T
        undistorted_points = undistorted_points[:, :2] / undistorted_points[:, 2, np.newaxis]
        # print(f"undistorted points: {undistorted_points}")
        return undistorted_points
