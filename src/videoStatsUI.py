import streamlit as st
import requests
import json
import base64
import io
from PIL import Image, ImageDraw
import numpy as np
from pathlib import Path
import cv2
import pandas as pd
from pydantic import BaseModel
import re
from shelfPredictor import ShelfPredictor, PredictedShelf
from commonUI import ItemNavigator
from loguru import logger
from analyzeVideos import VideoManager, OPEN_DOOR_CATEGORIES, GLARE_OR_FOG, TOO_OBSTRUCTED, OPEN_BUT_NOT_TOP3_CLEAR, TOP_3_CLEAR, NOT_OPEN_ENOUGH
import matplotlib.pyplot as plt
from shelfSettings import StoreManager
from datetime import datetime, timedelta

DEPLOYMENT_DATE = datetime.strptime("16-07-2024", "%d-%m-%Y")
date_today = datetime.today()
STORE_MAPPER = StoreManager()

# st.set_page_config(page_title="FrameSelection", layout="wide")
st.set_page_config(page_title="VideoStatistics", layout="centered")

# def is_asset_of_interest(x):
#     if x[0] in ["V", "P"]:
#         return True
#     return False


def get_store_history(frame, store_asset_name, num_days=10):
    # Assuming 'frame' is already defined and processed earlier
    f = frame[frame.video_status != 'closedDoor']
    f = f[f.video_status != 'error']
    store_frame = f[f['store_asset_name'] == store_asset_name]
    daily_counts = store_frame.groupby(['video_status', pd.Grouper(freq='D')]).size()
    daily_counts = daily_counts.reset_index(name='daily_stats')
    daily_counts.set_index('date', inplace=True)
    daily_counts.index = daily_counts.index.date

    # Pivot the table to have dates as index and video statuses as columns
    pivot_table = daily_counts.pivot(columns='video_status', values='daily_stats')
    pivot_table = pivot_table.fillna(0).astype(int)
    # Check the pivot table
    # print("Pivot Table Data:", pivot_table)

    # Filter to get only the last 10 entries for plotting
    last_10_entries = pivot_table.tail(num_days)
    # Dynamically determine which columns to include based on data availability
    # all_possible_statuses = [TOO_OBSTRUCTED, GLARE_OR_FOG, OPEN_BUT_NOT_TOP3_CLEAR, TOP_3_CLEAR]
    column_order = [status for status in OPEN_DOOR_CATEGORIES if status in pivot_table.columns]
    last_10_entries = last_10_entries[column_order]
    return last_10_entries


def get_todays_video_counts(frame, category=None, today=None, region=None):
    f = frame.copy()
    f = f.reset_index()
    f['date'] = f['date'].apply(lambda x: x.date())  # Strip time part from datetime
    f.set_index('date', inplace=True)

    if today is None:
        # Filter for today's data with status TOP_3_CLEAR
        today = pd.Timestamp('today').date()  # Get today's date as a date object

    
    todays_data = f[f.index == today]
    todays_total_data = f[f.index == today]
    if category:
        todays_data = todays_data[todays_data['video_status'] == category]
    else:
        todays_data = todays_data[todays_data['video_status'] != 'closedDoor']
    todays_total_data = todays_total_data[todays_total_data['video_status'] != 'closedDoor']

    if region:
        todays_data = todays_data[todays_data.region == region]
        todays_total_data = todays_total_data[todays_total_data.region == region]

    # Count videos per store
    todays_videos_count = todays_data.groupby('store_asset_name').size()
    todays_total_videos_count = todays_total_data.groupby('store_asset_name').size()

    # Ensure all stores are represented
    all_stores = pd.Series(index=pd.Index(frame['store_asset_name'].unique(), name='store_asset_name'), dtype=int)
    if region:
        all_stores = all_stores[all_stores.index.isin(frame[frame.region == region].store_asset_name.unique())]
    todays_videos_count = all_stores.combine_first(todays_videos_count).fillna(0).astype(int)
    todays_total_videos_count = all_stores.combine_first(todays_total_videos_count).fillna(0).astype(int)

    # Plotting
    if category:
        todays_videos_count_df = todays_videos_count.reset_index(name=f'{category}_count')
        todays_total_videos_count_df = todays_total_videos_count.reset_index(name='total_count')
        field = f'{category}_count'
    else:
        todays_videos_count_df = todays_videos_count.reset_index(name='count')
        todays_total_videos_count_df = todays_total_videos_count.reset_index(name='total_count')
        field = 'count'
    
    todays_total_videos_count_df[field] = todays_videos_count_df[field].fillna(0)
    todays_total_videos_count_df['fraction'] = todays_total_videos_count_df[field] / todays_total_videos_count_df['total_count']
    return todays_videos_count_df, todays_total_videos_count_df


# @st.cache_resource
# def get_frame(mod_time):
#     video_manager = VideoManager()
#     mod_time = video_manager.mod_time
#     frame = video_manager.frame[video_manager.frame.store_asset_name.apply(STORE_MAPPER.is_asset_of_interest)]
#     return frame, mod_time


def get_uncached_frame(remove_restocking=True):
    video_manager = VideoManager()
    mod_time = video_manager.mod_time
    frame = video_manager.frame[video_manager.frame.store_asset_name.apply(STORE_MAPPER.is_asset_of_interest)]
    before = len(frame)
    if remove_restocking:
        frame = frame[frame.restocking == False]
        after = len(frame)
        logger.info(f"Restocking videos removed: {before - after}")
    return frame, mod_time


@st.cache_resource
def get_logger():
    log_file = LOG_DIR / "videoStatsUI.log"
    logger.add(
        log_file,
        format="{time}|{file}|{line}|{thread}|{level}|{message}",
        colorize=True,
    )
    return logger


####################################################################################################################
# Initializations
####################################################################################################################
FIGSIZE = (25, 6)
BASE_DIR = Path(__file__).parent.parent
LOG_DIR = BASE_DIR / "logs"
DATA_DIR = BASE_DIR / "data"
for dir in [DATA_DIR, LOG_DIR]:
    if not dir.exists():
        dir.mkdir()

logger = get_logger()
if 'mod_time' not in st.session_state:
    st.session_state.mod_time = None

# frame, mod_time = get_frame(st.session_state.mod_time)
remove_restocking = st.radio("Remove Restocking", [True, False], index=0)
frame, mod_time = get_uncached_frame()
# if mod_time != st.session_state.mod_time:
#     frame, mod_time = get_uncached_frame()
#     st.session_state.mod_time = mod_time

update_time = max(frame.index)
st.caption(f"Last updated: {update_time}")

COLOR_MAP = {
    GLARE_OR_FOG: 'lightCoral', 
    TOO_OBSTRUCTED: 'tomato', 
    OPEN_BUT_NOT_TOP3_CLEAR: 'paleTurquoise', 
    NOT_OPEN_ENOUGH: 'lightSkyBlue',
    TOP_3_CLEAR: 'lightSeaGreen'
}

# Start Date Selection
st.markdown("---")
today = st.date_input("Today", value=date_today, min_value=DEPLOYMENT_DATE, max_value=date_today)
start_date = st.date_input("Start Date", value=DEPLOYMENT_DATE, min_value=DEPLOYMENT_DATE, max_value=today)
frame = frame[frame.index.date >= pd.to_datetime(start_date).date()]
frame = frame[frame.index.date <= pd.to_datetime(today).date()]
region_list = frame.region.unique().tolist()
region_list.insert(0, None)
region = st.selectbox("Region", region_list)
if region is not None:
    frame = frame[frame.region == region]


####################################################################################################################
# top3Clear within doorOpens
####################################################################################################################
todays_counts, todays_totals = get_todays_video_counts(frame, category='top3Clear', today=today, region=region)
if not todays_totals.empty:
    # Set up the figure
    plt.figure(figsize=(10, 6), dpi=1200)
    # plt.figure(figsize=FIGSIZE, dpi=1200)

    # Number of items
    N = len(todays_totals)
    indices = np.arange(N)

    # Plotting the stacked bars
    plt.bar(indices, todays_totals['top3Clear_count'], width=0.5, label=TOP_3_CLEAR, color=COLOR_MAP[TOP_3_CLEAR])
    plt.bar(indices, todays_totals['total_count'] - todays_totals['top3Clear_count'], width=0.5, bottom=todays_totals['top3Clear_count'], label='openButNotTop3Clear', color='gray')

    # Setting labels, ticks, and legend
    plt.xlabel('Store Asset Name')
    plt.ylabel('Count')
    plt.xticks(indices, todays_totals['store_asset_name'], rotation=90, ha='center')
    plt.legend()
    plt.title('top3Clear within doorOpens')
    plt.tight_layout()
    plt.xlim(-0.5, N - 0.5)
    st.pyplot(plt.gcf())

####################################################################################################################
# Summary
####################################################################################################################
st.title("Today's data based on video status")
category = st.selectbox("Video Status", OPEN_DOOR_CATEGORIES)
# color_dict = {GLARE_OR_FOG: 'lightCoral', TOO_OBSTRUCTED: 'tomato', OPEN_BUT_NOT_TOP3_CLEAR: 'paleTurquoise', TOP_3_CLEAR: 'lightSeaGreen'}
todays_counts, _ = get_todays_video_counts(frame, category=category, today=today, region=region)
if not todays_counts.empty:
    plt.figure(figsize=FIGSIZE)
    ax = todays_counts.plot(kind='bar', x='store_asset_name', y=f'{category}_count', legend=False, color=COLOR_MAP[category], width=0.5)
    ax.set_ylabel(f'Count of Videos with Status: {category}')
    plt.xticks(rotation=90, ha='center')
    plt.tight_layout()
    st.pyplot(plt.gcf())

####################################################################################################################
# Summary Open Door
####################################################################################################################
st.title("Today's door opened data")
# color_dict = {GLARE_OR_FOG: 'lightCoral', TOO_OBSTRUCTED: 'tomato', OPEN_BUT_NOT_TOP3_CLEAR: 'paleTurquoise', TOP_3_CLEAR: 'lightSeaGreen'}
todays_counts, _ = get_todays_video_counts(frame, category=None, today=today, region=region)
if not todays_counts.empty:
    plt.figure(figsize=FIGSIZE)
    ax = todays_counts.plot(kind='bar', x='store_asset_name', y=f'count', legend=False, color=COLOR_MAP[category])
    ax.set_ylabel(f'Count of Videos Where door is Opened')
    plt.xticks(rotation=90, ha='center')
    plt.tight_layout()
    st.pyplot(plt.gcf())

st.markdown("---")
####################################################################################################################
# Per Store Video Status History
####################################################################################################################
st.title("Per Store Status for last 10 entries")
stores = sorted(frame.store_asset_name.unique().tolist())
store_choice = st.selectbox("Select store", stores)
h_frame = get_store_history(frame, store_choice)
if not h_frame.empty:
    plt.figure(figsize=FIGSIZE)
    # color_dict = {GLARE_OR_FOG: 'lightCoral', TOO_OBSTRUCTED: 'tomato', OPEN_BUT_NOT_TOP_3_CLEAR: 'paleTurquoise', TOP_3_CLEAR: 'lightSeaGreen'}
    h_frame.plot(kind='bar', color=COLOR_MAP)  # Assuming two colors for two statuses
    plt.xlabel('Date (MM-DD)')
    plt.ylabel('Counts')
    plt.title('Video Status - Last 10 Entries')
    plt.xticks(rotation=90)  # Rotate x-axis labels for better readability
    plt.tight_layout()  # Adjust layout to make room for label rotation
    plt.legend(title='Video Status')
    st.pyplot(plt.gcf())
else:
    st.write("No history found for the selected store")

st.markdown("---")

####################################################################################################################
# Average Clear Videos Per Day
####################################################################################################################
st.title("Average Clear Videos Per Day")
daily_counts = frame[frame.video_status == TOP_3_CLEAR].groupby('store_asset_name').resample('D').size()
average_videos_per_day = daily_counts.groupby('store_asset_name').mean()
average_videos_per_day_df = average_videos_per_day.reset_index(name='average_videos_per_day').sort_index()
plt.figure(figsize=FIGSIZE)
ax = average_videos_per_day.plot(kind='bar', color='lightSeaGreen')
plt.xticks(rotation=90, ha='center')
st.pyplot(plt.gcf())

####################################################################################################################
# Average Videos Per Day
####################################################################################################################
st.title("Average Videos Per Day")
daily_counts = frame[frame.video_status != 'closedDoor'].groupby('store_asset_name').resample('D').size()
average_videos_per_day = daily_counts.groupby('store_asset_name').mean()
average_videos_per_day_df = average_videos_per_day.reset_index(name='average_videos_per_day').sort_index()
plt.figure(figsize=FIGSIZE)
ax = average_videos_per_day.plot(kind='bar', color='lightSeaGreen')
plt.xticks(rotation=90, ha='center')
st.pyplot(plt.gcf())

####################################################################################################################
# Total Video Volume
####################################################################################################################

st.title("Total Video Volume")
filtered_frame = frame[frame.video_status != 'closedDoor']
if filtered_frame.empty:
    st.write("No videos found")
else:
    # Get the value counts and sort by asset name
    value_counts_sorted = filtered_frame.store_asset_name.value_counts().sort_index()
    # Create a larger figure to accommodate all data
    plt.figure(figsize=FIGSIZE)
    # Plot the bar chart
    ax = value_counts_sorted.plot(kind='bar', color='lightSeaGreen')
    # Rotate x-axis labels for better readability
    plt.xticks(rotation=90, ha='center')
    # Display the plot in Streamlit
    st.pyplot(plt.gcf())

####################################################################################################################
# Video Status (Volume)
####################################################################################################################

st.title("Video Status (Volume)")
video_status = st.selectbox("Select video status", OPEN_DOOR_CATEGORIES)
filtered_frame = frame[frame.video_status == video_status]
if len(filtered_frame) == 0:
    st.write("No videos found")
else:
    ax = filtered_frame.store_asset_name.value_counts().sort_index().plot(kind='bar', color=COLOR_MAP[video_status])
    st.pyplot(plt.gcf())



####################################################################################################################
# Query File Name
####################################################################################################################
st.title("Query File Name")
video_manager = VideoManager()
frame = video_manager.frame
query_file_name = st.text_input("Enter the file name to query")
if query_file_name:
    query_frame = frame[frame.file.str.contains(query_file_name, flags=re.IGNORECASE)]
    if query_frame.empty:
        st.write("No data found for the query")
    else:
        st.write(query_frame)
