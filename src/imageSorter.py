from pathlib import Path
from classificationInference import ClassificationPredictor
from PIL import Image
from tqdm.auto import tqdm
from loguru import logger


class ImageDirSorter:
    def __init__(self, object_type="PET", threshold=0.95):
        self.object_type = object_type
        self.threshold = threshold
        self.object_threshold = 0.5

        model_path = Path("/media/akshay/datasets/coke/models/brand/July-23-01/CategoryClassification.pth")
        category_dir = Path("/media/akshay/datasets/coke/data/bottleClassification/brand-July-10/Category/")
        # categories = sorted([d.name for d in category_dir.iterdir()])
        categories = ['Cans', 'PET', 'RGB', 'Tetra']
        self.category_predictor = ClassificationPredictor(model_path, categories)

        if self.object_type in ['Can', 'PET', 'RGB', 'Tetra']:
            self.brand_predictor = self._get_predictor(self.object_type)
        else:
            raise Exception(f"Invalid object type: {self.object_type}")

    def _get_predictor(self, object_type):
        if object_type  == 'Cans':
            object_type = 'Can'
        model_path = Path(f"/home/akshay/workspace/computervision/cokeInference/models/{object_type}Classification.pth")
        category_dir = Path(f"/media/akshay/datasets/coke/data/bottleClassification/brand-July-10/{object_type}")
        brand_categories = sorted([d.name for d in category_dir.iterdir()])
        brand_predictor = ClassificationPredictor(model_path, brand_categories)
        return brand_predictor

    def _predict(self, model, image_paths):
        batch_size = 64
        num_images = len(image_paths)
        num_batches = num_images // batch_size
        if num_images % batch_size > 0:
            num_batches += 1
        categories = []
        confs = []
        for b in tqdm(range(num_batches)):
            start = b * batch_size
            end = (b + 1) * batch_size
            curr_paths = image_paths[start: end]
            batch_images = [Image.open(p) for p in curr_paths]
            curr_categories, curr_confs, _ = model.predict_batch(batch_images)
            categories.extend(curr_categories)
            confs.extend(curr_confs)
        return categories, confs

    def sort(self, files, category_of_interest=None, threshold=None, filter_catgory=True):
        if not threshold:
            threshold = self.threshold

        if filter_catgory:
            object_categories, object_confs = self._predict(self.category_predictor, files)
            files = [file for file, object_category, object_conf in zip(files, object_categories, object_confs) if object_category == self.object_type and object_conf > self.object_threshold]
            logger.info(f"Found [{len(files)}] files of [{self.object_type}]")

        brand_categories, brand_confs = self._predict(self.brand_predictor, files)
        filtered_files = [file for file, brand_category, brand_conf in zip(files, brand_categories, brand_confs) if brand_category == category_of_interest and brand_conf > threshold]
        remainder = [f for f in files if f not in filtered_files]
        for file in filtered_files:
            self.move(file, category_of_interest)
        logger.info(f"Moved [{len(filtered_files)}] files")
        return remainder

    def move(self, file, dest_dir):
        dest_dir = file.parent / "categorized" / dest_dir
        if not dest_dir.is_dir():
            dest_dir.mkdir(parents=True)
        new_path = dest_dir / file.name
        file.rename(new_path)


