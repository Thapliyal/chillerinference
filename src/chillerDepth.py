from pathlib import Path
import configparser
from utils import ProdigyLoader
from perShelfPredictor import PerShelfChillerPredictor, PredictedShelf
from shelfSettings import StoreManager
from lensCorrector import LensCorrector
from transformers import AutoImageProcessor, AutoModelForDepthEstimation
import torch
import numpy as np
from PIL import Image
import requests
import cv2
from matplotlib import pyplot as plt
from loguru import logger
import sys
from torchvision import transforms
from utils import load_image


to_pil = transforms.ToPILImage()


class DepthAnything:
    def __init__(self):
        self.image_processor = AutoImageProcessor.from_pretrained("depth-anything/Depth-Anything-V2-Large-hf")
        self.model = AutoModelForDepthEstimation.from_pretrained("depth-anything/Depth-Anything-V2-Large-hf")        

    def _get_depth_image(self, image):
        if isinstance(image, np.ndarray):
            image = Image.fromarray(image)
        inputs = self.image_processor(images=image, return_tensors="pt")
        
        with torch.no_grad():
            outputs = self.model(**inputs)
            predicted_depth = outputs.predicted_depth
        
        # interpolate to original size
        prediction = torch.nn.functional.interpolate(
            predicted_depth.unsqueeze(1),
            size=image.size[::-1],
            mode="bicubic",
            align_corners=False,
        )
        
        # visualize the prediction
        output = prediction.squeeze().cpu().numpy()
        formatted = (output * 255 / np.max(output)).astype("uint8")
        # depth = Image.fromarray(formatted)
        return formatted

    def get_image_and_depth(self, image_or_path):
        if isinstance(image_or_path, np.ndarray):
            image = image_or_path
        elif isinstance(image_or_path, Image.Image):
            image = np.array(image_or_path)
        elif isinstance(image_or_path, str) or isinstance(image_or_path, Path):
            image = load_image(str(image_or_path), bgr=False)
        else:
            raise Exception(f"incorrect image_or_path type: {type(image_or_path)}")
        return image, self._get_depth_image(image)
    

class DepthPredictor:
    def __init__(self, predictor=None):
        self.depth_model = DepthAnything()       
        self.STORE_MAPPER_WITHOUT_EXCEPTIONS = StoreManager(raise_exception=False)
    
        if predictor is None:
            cfg = configparser.ConfigParser()
            cfg.read('inference.ini')

            shelf_model_path = Path(cfg['DEFAULT']['shelf_model_path'])
            bottle_model_path = Path(cfg['DEFAULT']['bottle_model_path'])
            brand_models_dir = Path(cfg['DEFAULT']['brand_models_dir'])
            volume_model_path = Path(cfg['DEFAULT']['volume_model_path'])
            bottle_model_threshold = float(cfg['DEFAULT']['bottle_model_threshold'])
            
            self.predictor = PerShelfChillerPredictor(shelf_model_path=shelf_model_path, bottle_model_path=bottle_model_path, 
                                                brand_models_dir=brand_models_dir, volume_model_path=volume_model_path, 
                                                shelf_clip_index=5, bottle_model_threshold=bottle_model_threshold, 
                                                fixed_width_transform=True, debug=False)
        else:
            self.predictor = predictor

    def _get_depth_image(self, image):
        _, depth_image = self.depth_model.get_image_and_depth(image)
        return depth_image
        
    def _draw_depth(self, shelf: PredictedShelf):
        if shelf.depth_image is None:
            return

        image = shelf.depth_image
        pastel_color = (215, 235, 250)  # Antique White
        font = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 0.3
        font_thickness = 1
        text_color = (0, 0, 0)  # Black text
        background_color = pastel_color

        for b in shelf.bottles:
            x1, y1, x2, y2 = b.bbox.x1, b.bbox.y1, b.bbox.x2, b.bbox.y2
            cv2.rectangle(image, (x1, y1), (x2, y2), pastel_color, 2)

            text = f"{b.depth:2.2f}"
            (text_width, text_height), baseline = cv2.getTextSize(text, font, font_scale, font_thickness)
            w = x2 - x1
            x1 = int(x1 + w/6)
            # y1 = int(y + h - text_height)
            y2 = y2 - text_height
            cv2.rectangle(image, (x1, y2 - text_height), (x1 + text_width, y2 + baseline), background_color, -1)
            cv2.putText(image, text, (x1, y2), font, font_scale, text_color, font_thickness, cv2.LINE_AA)

        # x1, y1 = shelf.corners[0]
        # x2, y2 = shelf.corners[2]
        x1, y1 = 0, 0
        x2, y2 = image.shape[1], image.shape[0]//5
        font_scale = 0.5
        # text = f"{shelf.depth:2.2f}"
        text = f"{shelf.occupancy:2.2f}"
        (text_width, text_height), baseline = cv2.getTextSize(text, font, font_scale, font_thickness)
        x = x1 + x2 // 2 - text_width // 2
        y = y2 - text_height
        cv2.rectangle(image, (x, y - text_height), (x + text_width, y + baseline), background_color, -1)
        cv2.putText(image, text, (x, y), font, font_scale, text_color, font_thickness, cv2.LINE_AA)
        # logger.error(f"{shelf.label} depth: {shelf.depth:2.2f}")
        
    
    def _get_chiller_from_dict(self, d):
        image_path = d["path"]
        image_path = image_path.replace('/home/akshay/datasets/accuracy', '/home/akshay/workspace/labelling/coke/datasets/200DLens/distorted')

    def get_chiller_from_path(self, image_path):
        logger.remove()
        logger.add(sys.stdout, level="ERROR")
        shelf_mapping = self.STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(image_path)
        chiller = self.predictor.predict(image_path, shelf_heights=shelf_mapping.shelf_heights, shelf_widths=shelf_mapping.shelf_widths)
        chiller = self.update_chiller(chiller)
        logger.remove()
        logger.add(sys.stdout, level="DEBUG")
        return chiller
        
    def update_chiller(self, chiller):
        depth_images = []
        for shelf in chiller.shelves:
            o_image = Image.fromarray(shelf.output_image)
            shelf.depth_image = self._get_depth_image(o_image)

            if shelf.label == 'shelf1':
                max_depth = 30
                min_depth = 23
            elif shelf.label == 'shelf2':
                max_depth = 32
                min_depth = 17
            elif shelf.label == 'shelf3':
                max_depth = 28
                min_depth = 16
            else:
                max_depth = 120
                min_depth = 80
            
            bottles_per_col = 8
            total_facings = 8
            per_bottle_depth = (max_depth - min_depth) / bottles_per_col
            logger.debug(f"{shelf.label}: per_bottle_depth: {per_bottle_depth}")
            
            total_bottles_removed = 0
            num_empty_cols = total_facings - len(shelf.bottles)
            if num_empty_cols < 0:
                num_empty_cols = 0

            total_bottles_removed = num_empty_cols * bottles_per_col
            max_possible_bottles = total_facings * bottles_per_col

            for i, b in enumerate(shelf.bottles):
                mask = np.array(b.mask).astype(np.uint8) * 255
                b_depth_mask = np.bitwise_and(shelf.depth_image, mask)
                # b.depth = 15 - b_depth_mask.mean()
                
                b.depth = (max_depth - np.mean(b_depth_mask)) 
                if b.depth < min_depth:
                    num_depthwise_bottles_removed = 0
                else:
                    num_depthwise_bottles_removed = int((b.depth - min_depth) // per_bottle_depth)
                total_bottles_removed += num_depthwise_bottles_removed
                logger.info(f"{shelf.label}[{i}] depth: {b.depth:2.2f} num_depthwise_bottles_removed: {num_depthwise_bottles_removed}")
                # shelf.depth += num_depthwise_bottles_removed
            
            shelf.occupancy = (max_possible_bottles - total_bottles_removed) / max_possible_bottles
            self._draw_depth(shelf)
            depth_images.append(shelf.depth_image)
        chiller.depth_image = np.vstack(depth_images)
        return chiller