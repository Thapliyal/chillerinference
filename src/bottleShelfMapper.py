#!/usr/bin/env python3

from pathlib import Path
import json
import cv2
import numpy as np
from matplotlib import pyplot as plt
from pydantic import BaseModel
from typing import List, Union, Optional
from argparse import ArgumentParser
from loguru import logger
from perspectiveTransform import PerspectiveTransformer, Undistorter
from tqdm import tqdm
import re
import traceback
from shelfSettings import StoreManager, ShelfSettingException

STORE_MAPPER = StoreManager()

DEFAULT_DATASET_DIR = "/home/akshay/workspace/labelling/coke/datasets"

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('-b', '--bottle-jsonl', help='Bottle jsonl(s)', required=True, nargs='+')
    parser.add_argument('-s', '--shelf-jsonl', help='Shelf jsonl(s)', required=True, nargs='+')
    parser.add_argument('-o', '--output-jsonl', help='Output jsonl file to store the per shelf labels', required=True)
    parser.add_argument('-O', '--output-dir', help='Output Directory to store Shelf sub images', required=True)
    parser.add_argument('-d', '--dataset-dir', help='Local Dataset Directory (everything in the path upto "dataset" will be replaced by this)', required=True)
    return parser.parse_args()


def read_jsonl(json_path, dataset_dir=DEFAULT_DATASET_DIR):
    with open(json_path, 'r') as file:
        lines = file.readlines()
        dicts = [json.loads(line) for line in lines]
        new_dicts = []
        for dict in tqdm(dicts):
            if 'answer' in dict and dict['answer'] != 'accept':
                continue
            del dict['image']
            p = dict['path']
            p = re.sub('^.*?datasets', dataset_dir, p)
            assert Path(p).is_file(), f"File not found: {p}"
            dict['path'] = p
            new_dicts.append(dict)
            # if dict['path'].startswith('http://'):
            #     # dict['path'] = 'http://localhost:8000/' + dict['path']
            #     dict['path'] = dict['path'].replace('http://localhost:8000', '/home/akshay/workspace/labelling/coke')
            # if dict['path'][0] != '/':
            #     dict['path'] = dict['path'].replace('datasets', '/home/akshay/workspace/labelling/coke/datasets')
        logger.info(f"Read [{len(new_dicts)}/{len(dicts)}] annotations from: {json_path}")
        return new_dicts
        

class Span(BaseModel):
    # id: Optional[str]
    label: str
    # color: str
    type: str
    points: List[List[int]]

    def _get_min_max(self, span):
        x = [p[0] for p in span.points]
        y = [p[1] for p in span.points]
        return min(x), min(y), max(x), max(y)

    def get_miny(self):
        return min([p[1] for p in self.points])

    def is_completely_inside(self, outer):
        min_x, min_y, max_x, max_y = self._get_min_max(outer)
        for point in self.points:
            if not (min_x < point[0] < max_x and min_y < point[1] < max_y):
                return False
        return True
    
    def to_json(self):
        return json.dumps(self.dict())
    
    def to_dict(self):
        r = {}
        r['label'] = self.label
        r['type'] = self.type
        r['points'] = self.points
        return r


class ProdigyLabel(BaseModel):
    path: str
    height: int
    width: int
    spans: List[Span]

    def to_jsonl(self):
        d = {}
        d['image'] = self.path
        d['path'] = self.path
        d['height'] = self.height
        d['width'] = self.width
        d['spans'] = [span.to_dict() for span in self.spans]
        return json.dumps(d)
    

class BottleShelfMapping(BaseModel):
    bottle_label: ProdigyLabel
    shelf_label: ProdigyLabel
    path: str

    def get_shelves_for_mapping(self, image_count=0, output_dir=None, clip_index=4, undistort=True, remove_empty_shelves=True):
        shelves = []
        l_shelves = self.shelf_label
        l_bottles = self.bottle_label
        base_dataset_path = '/home/akshay/workspace/labelling/coke'
        curr_path = self.path.replace('http://localhost:8000', base_dataset_path)

        shelf_setting = STORE_MAPPER.get_shelf_settings(curr_path)
        if shelf_setting is None:
            raise Exception(f"Shelf setting not found for: {curr_path}")
        
        height = 600
        width = 800

        # heights = [33.5, 29, 40, 27, 29]
        # widths = [50, 50, 50, 50, 50]
        heights = shelf_setting.shelf_heights
        widths = shelf_setting.shelf_widths

        for i, span in enumerate(l_shelves.spans[:clip_index]):
            height = int(heights[i] / widths[i] * width)
            bottles = get_bottles_for_shelflabel(span, l_bottles)
            if remove_empty_shelves and len(bottles) == 0:
                continue
            # print(f"creating shelf[{i}]")
            shelf = Shelf(shelf_name=f'{i:02d}_{image_count:05d}_', image_path=curr_path, orig_points=span.points, bottles=bottles, shelf_height=height, shelf_width=width, output_dir=output_dir, undistort_points=undistort, save_output_image=output_dir is not None)
            # shelf = Shelf(shelf_name=f'{i:02d}_{image_count:05d}_', image_path=curr_path, orig_points=span.points, bottles=bottles, shelf_height=height, shelf_width=width, output_dir=output_dir)
            image_count += 1
            if height is None and width is None:
                height, width, _ = shelf.transformed_image.shape
            # print(f"Shelf[{i}]: [{len(bottles)} bottles]: height: {height} width: {width}")
            shelves.append(shelf)
        # shelves = sorted(shelves, key=lambda s: s.min_y)[:3]
        return shelves, image_count


class Bottle:
    def __init__(self, orig_points, label):
        self.orig_points = orig_points
        self.label = label
        self._transformed_points = None
        self._transformed_contours = None
        self._transformed_r_minAreaRect = None
        self.transformed_rotation = None
    
    @property
    def transformed_points(self):
        return self._transformed_points

    @transformed_points.setter
    def transformed_points(self, value):
        self._transformed_points = value
        self._transformed_contours = np.array([self._transformed_points], dtype=int)
        self._transformed_r_minAreaRect = cv2.minAreaRect(self._transformed_contours)
        self.transformed_rotation = self._transformed_r_minAreaRect[2]

    def draw_t_rotated(self, image, color=(255, 255, 255), thickness=1):
        box = cv2.boxPoints(self._transformed_r_minAreaRect)
        box = np.intp(box)
        image = cv2.drawContours(image, [box], 0, color=color, thickness=thickness)
        return image

    def to_prodigy(self):
        r = {}
        r['label'] = self.label
        r['type'] = 'polygon'
        r['points'] = self._transformed_points.tolist()
        return r

class Shelf:
    def __init__(self, shelf_name, image_path, orig_points, bottles: List[Bottle], shelf_height, shelf_width, output_dir, undistort_points=False, save_output_image=True):
        self.image_path = image_path
        self.shelf_name = shelf_name
        path = Path(self.image_path)
        dataset = self._get_dataset_name(path)
        self.sub_image_path = str(output_dir) + f"/{dataset}-{path.stem}_shelf{self.shelf_name}{path.suffix}"
        self.transformer = PerspectiveTransformer(debug=False)
        self.undistorter = Undistorter()
        self.undistort = undistort_points

        if self.undistort:
            image = cv2.imread(image_path)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            self.undistorted_image = self.undistorter.undistort_image(image)
            orig_points = self.undistorter.undistort_points(orig_points)
            image_or_path = self.undistorted_image
        else:
            image_or_path = image_path

        self.orig_points = orig_points
        self.min_y = min([point[1] for point in self.orig_points])
        self.M, self.transformed_image = self.transformer.get_transform_params(image_or_path, self.orig_points, height=shelf_height, width=shelf_width)
        self.output_image = cv2.cvtColor(self.transformed_image, cv2.COLOR_RGB2BGR)
        if save_output_image:
            cv2.imwrite(self.sub_image_path, self.output_image)
        # else:
        #     assert Path(self.sub_image_path).is_file(), f"Output image not found at: {self.sub_image_path}"
        self.transformed_height, self.transformed_width, _ = self.output_image.shape
        
        self.bottles = self._get_bottles(bottles)

    def _get_dataset_name(self, path):
        path = Path(path)
        path = path.parent
        if 'batch' in path.stem.lower():
            path = path.parent
        return path.name

    def _get_bottles(self, bottles):
        for bottle in bottles:
            orig_points = np.array([bottle.orig_points]).astype(np.float32)
            # print(orig_points)
            if self.undistort:
                orig_points = self.undistorter.undistort_points(orig_points)
                orig_points = np.array([orig_points])
                # print(orig_points)
            bottle.transformed_points = cv2.perspectiveTransform(orig_points, self.M)[0]
        return bottles
    
    def _get_min_max(self, points):
        x = [int(p[0]) for p in points]
        y = [int(p[1]) for p in points]
        return min(x), min(y), max(x), max(y)

    def _get_sub_image(self, points):
        min_x, min_y, max_x, max_y = self._get_min_max(points)
        return self.transformed_image[min_y: max_y+1, min_x: max_x+1].copy()
    
    def is_distorted(self, margin=5, threshold=0.4):
        THRESHOLD = int(threshold * len(self.bottles))
        count = 0
        for bottle in self.bottles:
            if bottle.transformed_rotation > 45 and abs(90 - bottle.transformed_rotation) > margin:
                count += 1
            if bottle.transformed_rotation < 45 and bottle.transformed_rotation > margin:
                count += 1
        return count > THRESHOLD

    def get_bottle_subs(self):
        contours = []
        labels = []
        sub_images = []
        bottle: Bottle
        for bottle in self.bottles:
            if bottle.transformed_points is None:
                raise ValueError(f"transformed points in Shelf for: {self.image_path} is None")
            contours.append(bottle.transformed_points.astype(int))
            sub_images.append(self._get_sub_image(bottle.transformed_points))
            labels.append(bottle.label)
            
        im = self.transformed_image
        mask = np.zeros((im.shape[0], im.shape[1]), np.uint8)
        cv2.drawContours(mask, contours, -1, (255), thickness=-1)
        im = cv2.bitwise_and(im, im, mask=mask)
        return sub_images, labels, im
    
    def draw_t_rotated(self, image=None, color=(255, 255, 255), thickness=1):
        if image is None:
            t_rotated = self.transformed_image.copy()
        else:
            t_rotated = image.copy()
        for bottle in self.bottles:
            t_rotated = bottle.draw_t_rotated(t_rotated, color, thickness)
        return t_rotated
    
    def to_prodigy(self):
        r = {}
        r['image'] = self.sub_image_path
        r['path'] = self.sub_image_path
        r['orig_path'] = self.image_path
        r['height'] = self.transformed_height
        r['width'] = self.transformed_width
        r['spans'] = [bottle.to_prodigy() for bottle in self.bottles]
        return r
        
class Chiller:
    def __init__(self, shelves: List[Shelf]):
        self.shelves = shelves
        self.image_path = shelves[0].image_path

    def draw_orig_shelves(self, color=(255, 255, 255), thickness=2):
        image = cv2.imread(self.image_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        for shelf in self.shelves:
            output_image = cv2.drawContours(image, [shelf.orig_points.astype(int)], 0, color=color, thickness=thickness)
        return output_image

def get_spans(span_dicts):
    spans = []
    for span_dict in span_dicts:
        points = span_dict['points']
        if not isinstance(points[0], list):
            continue
        points = [[int(x), int(y)] for x, y in points]
        s = Span(label=span_dict['label'], type=span_dict['type'], points=points)
        spans.append(s)
    return spans

def get_prodigy_label(label_dict):
    if 'spans' not in label_dict:
        path = label_dict['path'] or "unknown"
        raise ValueError(f'spans not found in dict. path: {path}')
    spans = get_spans(label_dict['spans'])
    spans = sorted(spans, key=lambda x: x.get_miny())
    return ProdigyLabel(path=label_dict['path'], height=int(label_dict['height']), width=int(label_dict['width']), spans=spans)

def find_matching_anno(annos, search):
    for anno in annos:
        if anno['path'] == search:
            return anno
    return None

def get_jsonl_files(paths):
    if not isinstance(paths, list) and (isinstance(paths, str) or isinstance(paths, Path)):
        paths = [paths]
    paths = [Path(p) for p in paths]
    files = []
    for path in paths:
        if not path.exists():
            raise FileNotFoundError(f'Unable to open: {path}')
        if path.is_dir():
            curr_files = list(path.rglob("*.jsonl"))
            if len(curr_files) == 0:
                raise FileNotFoundError(f'No jsonl files found in: {path}')
            files.extend(curr_files)
        else:
            files.append(path)
    return files

def get_mappings_for_dicts(bottle_dicts, shelf_dicts):
    mappings = []
    size = len(shelf_dicts)
    unmapped = []
    for shelf_d in tqdm(shelf_dicts, total=size, desc='Searching for Mappings'):
        bottle_d = find_matching_anno(bottle_dicts, shelf_d['path'])
        if bottle_d:
            path = shelf_d['path']
            try:
                shelf = get_prodigy_label(shelf_d)
                bottle = get_prodigy_label(bottle_d)
                mapping = BottleShelfMapping(path=path, bottle_label=bottle, shelf_label=shelf)
                mappings.append(mapping)
            except ValueError as v:
                logger.error(v)
                unmapped.append(shelf_d)
                continue  
        else:
            unmapped.append(shelf_d)
    logger.info(f'{len(mappings)} mappings found')
    logger.info(f'{len(unmapped)} bottle annotations not found')
    logger.info("-"*80)
    return mappings, unmapped

def has_bad_labels(shelf):
    BAD_LABELS = ["bottle"]
    for bottle in shelf.bottles:
        if bottle.label.lower() in BAD_LABELS:
            return True
    return False

def get_bottles_for_shelflabel(shelf_label, l_bottles):
    # shelf_label = matched_span.shelf.spans[1]
    shelf_bottles = []
    l_bottle: Span
    for l_bottle in l_bottles.spans:
        if l_bottle.is_completely_inside(shelf_label):
            bottle = Bottle(l_bottle.points, l_bottle.label)
            shelf_bottles.append(bottle)    
    return shelf_bottles


def generate_output(mappings: List[BottleShelfMapping], output_dir, output_json_path, bad_labels_file):
    # output_dir = Path('/media/akshay/datasets/coke/subShelf/test-March09')
    if not output_dir.is_dir():
        output_dir.mkdir()

    existing_files = list(output_dir.iterdir())
    if len(existing_files) > 0:
        logger.warning(f'Output dir: {output_dir} is not empty. Deleting all files')

    for file in existing_files:
        file.unlink()

    count = 0
    # output_file = Path('/home/akshay/workspace/labelling/coke/datasets/exported/perShelf-March09.jsonl')
    # bad_labels_file = Path('/home/akshay/workspace/labelling/coke/datasets/exported/perShelf-March09-badLabels.jsonl')
    with open(output_json_path, 'w') as file:
        bad_labels = []
        for m in tqdm(mappings, desc='Generating Shelf Images'):
            try:
                # if m.path != '/home/akshay/workspace/labelling/coke/datasets/shelfSegmentation/Set09-February-03/batch00/IMAGE_00010.jpg':
                #     continue
                curr_shelves, count = m.get_shelves_for_mapping(image_count=count, output_dir=output_dir)
                shelf: Shelf
                for shelf in curr_shelves:
                    if len(shelf.bottles) == 0:
                        continue
                    if has_bad_labels(shelf):
                        bad_labels.append(shelf)
                        continue
                    
                    d = shelf.to_prodigy()
                    line = json.dumps(d)

                    file.write(f"{line}\n")
            except ShelfSettingException as e:
                logger.error(f'Error handling {m.path}')
                logger.error(e)
                raise e                
            except Exception as e:
                logger.error(f'Error handling {m.path}')
                traceback.print_exc()
                logger.error(e)
                continue
    logger.info(f'{count} lines written to {output_json_path}')

    with open(bad_labels_file, 'w') as file:
        for shelf in bad_labels:
            d = shelf.to_prodigy()
            line = json.dumps(d)
            file.write(f"{line}\n")
    logger.info(f'{len(bad_labels)} lines written to {bad_labels_file}')

def get_mappings(bottle_jsonl, shelf_jsonl, dataset_dir=DEFAULT_DATASET_DIR):
    shelf_files = get_jsonl_files(shelf_jsonl)
    shelf_dicts = []
    for shelf_file in shelf_files:
        shelf_dicts.extend(read_jsonl(shelf_file, dataset_dir))

    bottle_files = get_jsonl_files(bottle_jsonl)
    bottle_dicts = []
    for bottle_file in bottle_files:
        bottle_dicts.extend(read_jsonl(bottle_file, dataset_dir))

    b_initial = len(bottle_dicts)
    s_initial = len(shelf_dicts)

    bottle_dicts = [b for b in bottle_dicts if ("answer" in b and b["answer"] == "accept") or "answer" not in b]
    shelf_dicts = [s for s in shelf_dicts if ("answer" in s and s["answer"] == "accept") or "answer" not in s]
    
    logger.info(f"Shelf Annotations: {len(shelf_dicts)} Bottle Annotations: {len(bottle_dicts)}")
    mappings, unmapped = get_mappings_for_dicts(bottle_dicts, shelf_dicts)
    return mappings, unmapped

def main():
    args = parse_arguments()
    output_json_path = Path(args.output_jsonl)
    assert output_json_path.suffix == '.jsonl', 'Output file should be a jsonl file'
    assert Path(args.dataset_dir).is_dir(), f"Dataset directory not found: {args.dataset_dir}"
    
    output_dir = Path(args.output_dir)
    if not output_dir.is_dir():
        output_dir.mkdir()

    mappings, unmapped = get_mappings(args.bottle_jsonl, args.shelf_jsonl, args.dataset_dir)
    
    bad_labels_path = output_json_path.parent / f"{output_json_path.stem}-badLabels.jsonl"
    unmapped_labels_path = output_json_path.parent / f"{output_json_path.stem}-unmapped.jsonl"

    with open(unmapped_labels_path, 'w') as file:
        for shelf in unmapped:
            line = json.dumps(shelf)
            file.write(f"{line}\n")
    logger.info(f'{len(unmapped)} unmapped shelf annotations written to: {unmapped_labels_path}')

    generate_output(mappings, output_dir, output_json_path, bad_labels_path)


if __name__ == "__main__":
    main()
