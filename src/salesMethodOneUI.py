import streamlit as st
import requests
import json
import base64
import io
from PIL import Image
import numpy as np
from pathlib import Path
import cv2
import pandas as pd
from utils import decode_image, get_blur_value, get_frames
from pydantic import BaseModel
import re
import os
from shelfSettings import StoreManager, ShelfSettingException
from datetime import datetime
from classificationInference import VideoClassification, FrameSelector
from analyzeVideos import VideoManager, TOP_3_CLEAR, OPEN_BUT_NOT_TOP3_CLEAR
import configparser
from segmentationInference import SegmentationPredictor
from shelfPredictor import ShelfPredictor
from lensCorrector import LensCorrector
from perShelfPredictor import PerShelfChillerPredictor, PredictedChiller
import urllib.parse
from utils import get_grid_image

st.set_page_config(page_title="Sales Method One", layout="wide")
IS_IN_DOCKER = os.environ.get("RUNNING_IN_DOCKER", False)

@st.cache_resource
def get_logger():
    from loguru import logger

    log_file = LOG_DIR / "salesMethodOneUI.log"
    logger.add(
        log_file,
        format="{time}|{file}|{line}|{thread}|{level}|{message}",
        colorize=True,
    )
    return logger


def get_sales(video_path):
    if IS_IN_DOCKER:
        url = "http://analyzechiller:9002/salesMethodOneInference"
    else:
        url = "http://127.0.0.1:9002/salesMethodOneInference"
    video_path =  urllib.parse.quote(str(video_path))
    data = {"video_path": video_path}
    response = requests.post(url, json=data)
    response = response.json()
    return response

####################################################################################################################
# Initializations
####################################################################################################################
BASE_DIR = Path(__file__).parent.parent
LOG_DIR = BASE_DIR / "logs"
UPLOAD_DIR = BASE_DIR / "salesData"
STORE_MAPPER = StoreManager(raise_exception=False)

for dir in [UPLOAD_DIR, LOG_DIR]:
    if not dir.exists():
        dir.mkdir()

logger = get_logger()

####################################################################################################################
# common functions
####################################################################################################################
def get_files():
    files = [f for f in UPLOAD_DIR.iterdir() if f.is_file() and f.suffix in [".avi", ".mp4"]]
    return files

def upload(bytes_data, file_name): 
    global UPLOAD_DIR
    file_path = UPLOAD_DIR / file_name
    with open(file_path, "wb") as file:
        file.write(bytes_data)
        # st.info("")

def delete_files():
    files = get_files()
    deleted = len([f.unlink() for f in files])
    st.info(f"deleted [{deleted}] files")


####################################################################################################################
# User Interface
####################################################################################################################
files = get_files()
st.session_state.num_files = len(files)


with st.expander("Upload Videos"):
    uploaded_files = st.file_uploader(
        "Choose a mp4/avi file",
        accept_multiple_files=True,
        type=["mp4", "avi"],
    )

    for uploaded_file in uploaded_files:
        bytes_data = uploaded_file.read()
        upload(bytes_data, uploaded_file.name)

    names = [f.name for f in files]
    frame = pd.DataFrame.from_dict({"File Name": names})
    st.table(frame)
    reset_button = st.button("DeleteFiles")
    if reset_button:
        delete_files()

files = get_files()
if len(files) == 0:
    st.write("No files uploaded")
    st.stop()

names = [f.name for f in files]
selected_name = st.selectbox("Select file", names)
selected_i = names.index(selected_name)
selected_file = files[selected_i]

response = get_sales(selected_file)
if response is None:
    st.write("No response")
    st.stop()

if "sales_image" in response:
    sales_image = response["sales_image"]
    sales_image = decode_image(sales_image)
else:
    sales_image = None

if "sku_analysis_image" in response:
    sku_analysis_image = response["sku_analysis_image"]
    sku_analysis_image = decode_image(sku_analysis_image)
    sku_analysis_image = get_grid_image(sku_analysis_image)
else:
    sku_analysis_image = None

if "bottle_sold_image" in response:
    bottle_sold_image = response["bottle_sold_image"]
    bottle_sold_image = decode_image(bottle_sold_image)
    bottle_sold_image = get_grid_image(bottle_sold_image)
else:
    bottle_sold_image = None

if "sale_status" not in response:
    st.write("No sale status found")
    st.stop()

status = response["sale_status"]
if "sku_indexes" in response:
    sku_indexes = response["sku_indexes"]
else:
    sku_indexes = None

if "sku_codes" in response:
    sku_codes = response["sku_codes"]
else:
    sku_codes = None

st.write(f"Status: {status}")
if sku_codes is not None and len(sku_codes) > 0:
    st.write(f"Number of SKUs Sold: {len(sku_codes)}")

col1, col2, col3 = st.columns([10, 10, 4])
if bottle_sold_image is not None:
    col1.image(bottle_sold_image, caption="Bottle Sold Image", use_container_width=True)
if sku_analysis_image is not None:
    col2.image(sku_analysis_image, caption="SKU Analysis Image", use_container_width=True)

col3.write(f"Sales: {sku_indexes}: {sku_codes}")

if "sales_shelf_label" in response:
    sales_shelf_label = response["sales_shelf_label"]
    col3.write(f"Sale Shelf: {sales_shelf_label}")

if "sku_brands" in response and "sku_volumes" in response and "sku_types" in response:
    sku_brands = response["sku_brands"]
    sku_volumes = response["sku_volumes"]
    sku_types = response["sku_types"]
    for i, (sku_brand, sku_volume, sku_type) in enumerate(zip(sku_brands, sku_volumes, sku_types)):
        if sku_volume and sku_brand and len(sku_brand) > 0:
            col3.write(f"SKU Sold[{i}]: <{sku_brand:10s}> <{sku_volume:04d} ml> <{sku_type.upper()}>")
        else:
            sku_volume = str(sku_volume)
            sku_brand = 'Unknown'
            col3.write(f"SKU Sold[{i}]: <{sku_brand:10s}> < {sku_volume} ml> <{sku_type.upper()}>")

if sales_image is not None:
    st.image(sales_image, caption="Sales Image")
st.write(response)
