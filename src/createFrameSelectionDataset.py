#!/usr/bin/env python3
from shelfPredictor import ShelfPredictor
from pathlib import Path
import cv2
from matplotlib import pyplot as plt
import torch
import numpy as np
from loguru import logger
import gc
from argparse import ArgumentParser
from tqdm.auto import tqdm
from shelfSettings import StoreManager

STORE_MAPPER_WITHOUT_EXCEPTIONS = StoreManager(raise_exception=False)


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('-i', '--input-dir', help='input-dir with files to predict on (will be searched recursively)', required=False, default=None)
    parser.add_argument('-o', '--output-dir', help='Output directory to store the images', required=False, default=None)
    parser.add_argument('-m', '--model-path', help='Path to shelf model', required=False, default=None)
    parser.add_argument('-b', '--batch-size', help='Batch size for prediction', type=int, default=None)
    parser.add_argument('-t', '--trunc-index', help='index to truncate dataset', type=int, default=None)
    parser.add_argument('-r', '--randomize', help='shuffle the ordering of videos selected', action='store_true')
    return parser.parse_args()


class VideoShelfPredictor:
    def __init__(self, output_dir=None, model_path=None, batch_size=8):
        self.batch_size = batch_size
        if model_path is None:
            self.model_path = Path("/home/akshay/workspace/computervision/chillerinference/models/shelf_model/model/shelf_model.pth")
        else:
            self.model_path = model_path
        self.shelf_predictor = ShelfPredictor(model_path=self.model_path, threshold=0.8, sort_fn=None)
        if output_dir is None:
            if Path("/home/akshay/datasets").is_dir():
                self.output_dir = Path("/home/akshay/datasets/frameSelection/June-10")
            else:
                self.output_dir = Path("/media/akshay/datasets/coke/frameSelection/June-10")
        else:
            self.output_dir = output_dir
        self.good_dir = self.output_dir / "good"
        self.bad_dir = self.output_dir / "bad"
        if not self.good_dir.is_dir():
            self.good_dir.mkdir(parents=True)
        if not self.bad_dir.is_dir():
            self.bad_dir.mkdir(parents=True)

    def _predict_batch(self, frames, video_file):
        if len(frames) == 0:
            return [], []
        shelf_settings = STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(video_file)
        if shelf_settings is None:
            return [], []
        height, width = frames[0].shape[:-1]
        if height != 1600:
            frames = [cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE) for frame in frames]
        shelves, output_images = self.shelf_predictor.predict_batch(frames, shelf_heights=shelf_settings.shelf_heights, shelf_widths=shelf_settings.shelf_widths, overlap_margin=0.5)
        return shelves, output_images

    def predict_batch(self, video_file):
        frames = self._get_frames(video_file)
        logger.info(f"num frames: {len(frames)}")
        num_frames = len(frames)
        num_batches = num_frames // self.batch_size + 1 * ((num_frames % self.batch_size) > 0)

        shelves = []
        output_images = []
        
        for batch_num in range(num_batches):
            # if batch_num in [0, 1]:
            #     continue
            start = batch_num * self.batch_size
            end = (batch_num + 1) * self.batch_size
            if end > len(frames):
                end = len(frames)
            curr_batch = np.array(frames[start: end])
            curr_shelves, curr_outputs = self._predict_batch(curr_batch, video_file)
            shelves.extend(curr_shelves)
            output_images.extend(curr_outputs)
            logger.info(f"{start}: {end}: {curr_batch.shape}")
            gc.collect()

        return shelves, frames, output_images

    def predict(self, video_file):
        frames = self._get_frames(video_file)
        logger.info(f"num frames: {len(frames)}")

        shelves = []
        output_images = []
        
        shelf_settings = STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(video_file)
        if shelf_settings is None:
            return [], [], []
        for frame in frames:
            curr_shelves, curr_outputs = self.shelf_predictor.predict(frame, overlap_margin=0.5, shelf_heights=shelf_settings.shelf_heights, shelf_widths=shelf_settings.shelf_widths)
            shelves.append(curr_shelves)
            output_images.append(curr_outputs)

        return shelves, frames, output_images
    

    def _get_frames(self, video_file):
        cap = cv2.VideoCapture(str(video_file))
        if not cap.isOpened():
            logger.error(f"Unable to open video: {video_file}")
            # raise Exception(f"Unable to open video: {video_file}")
            return []
        frames = []
        while True:
            ret, frame = cap.read()
            if not ret:
                break
            frames.append(frame)
        cap.release()
        
        if len(frames) == 0:
            return []
        height, width = frames[0].shape[:-1]
        if height != 1600:
            frames = [cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE) for frame in frames]
        return frames

    def generate_data(self, video_file):
        if self.batch_size is None:
            predict_fn = self.predict
        else:
            predict_fn = self.predict_batch

        video_file = Path(video_file)
        first_file_name = f"{video_file.parent.name}-{video_file.stem}-00000.jpg"
        first_file_path = self.good_dir / first_file_name
        alt_first_file_path = self.bad_dir / first_file_name
        if first_file_path.is_file() or alt_first_file_path.is_file():
            logger.info(f"Skipping: {video_file}")
            return
        shelves, frames, output_images = predict_fn(video_file)
        for i, (shelves_r, frame) in enumerate(zip(shelves, frames)):
            file_name = f"{video_file.parent.name}-{video_file.stem}-{i:05d}.jpg"
            if len(shelves_r) >= 3:
                file_path = self.good_dir / file_name
            else:
                file_path = self.bad_dir / file_name
            cv2.imwrite(str(file_path), frame)

def main():
    args = parse_arguments()
    input_dir = args.input_dir
    output_dir = args.output_dir
    model_path = args.model_path
    batch_size = args.batch_size
    IS_SERVER = Path("/home/akshay/datasets").is_dir()

    logger.info(f"IS_SERVER: {IS_SERVER}, input_dir: {input_dir}, output_dir: {output_dir}, model_path: {model_path} batch_size: {batch_size} randomize: {args.randomize}")

    if IS_SERVER:
        if input_dir is None:
            input_dir = Path("/home/akshay/siteData")
        else:
            input_dir = Path(input_dir)
        if output_dir is None:
            output_dir = Path("/home/akshay/datasets/frameSelection/June-10")
        else:
            output_dir = Path(output_dir)
        if model_path is None:
            model_path = Path("/home/akshay/workspace/computervision/chillerinference/models/shelf_model/model/shelf_model.pth")
        else:
            model_path = Path(model_path)
    
    if not IS_SERVER:
        if input_dir is None:
            input_dir = Path("/media/akshay/datasets/coke/siteData")
        else:
            input_dir = Path(input_dir)
        if output_dir is None:
            output_dir = Path("/media/akshay/datasets/coke/frameSelection/June-10")
        else:
            output_dir = Path(output_dir)
        if model_path is None:
            model_path = Path("/home/akshay/workspace/computervision/cokeInference/models/shelf_model/model/shelf_model.pth")
        else:
            model_path = Path(model_path)
        
    video_files = sorted(list(input_dir.glob("**/*.avi")))
    if args.randomize:
        np.random.seed(42)
        np.random.shuffle(video_files)
    predictor = VideoShelfPredictor(output_dir=output_dir, model_path=model_path, batch_size=batch_size)

    video_files = [video_file for video_file in video_files if STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(video_file) is not None and cv2.VideoCapture(str(video_file)).isOpened()]
    logger.info(f"Total processible video files: {len(video_files)}")
    if args.trunc_index is not None:
        video_files = video_files[:args.trunc_index]

    for video_file in tqdm(video_files):
        predictor.generate_data(video_file)


if __name__ == "__main__":
    main()