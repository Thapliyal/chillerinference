from fastapi import FastAPI, HTTPException, Body
from fastapi.responses import JSONResponse
import traceback
from pathlib import Path
from loguru import logger
import cv2
from pydantic import BaseModel
from typing import List
import numpy as np
from classificationInference import FrameSelector, VideoDecodeError, VideoClassification
from analyzeVideos import CLOSED_DOOR
import os
import configparser

RUNNING_IN_DOCKER = os.getenv('RUNNING_IN_DOCKER')
cfg = configparser.ConfigParser()
cfg.read('inference.ini')
frame_selection_model_path = Path(cfg['DEFAULT']['frameselection_model_path'])
frame_selector = FrameSelector(model_path=frame_selection_model_path)
app = FastAPI()

def get_status(video_result: VideoClassification):
    if video_result is None or video_result.frame_classifications is None \
        or video_result.frame_classifications.frame_classifications is None \
            or len(video_result.frame_classifications.frame_classifications) == 0:
        return "noGoodFramesFound"
    closed_count = len([f for f in video_result.frame_classifications.frame_classifications if f.category == CLOSED_DOOR])
    closed_frac = closed_count / len(video_result.frame_classifications.frame_classifications)
    if closed_frac > 0.8:
        return "closedDoor"
    if video_result.frame_classifications.frame_classifications[0].category != CLOSED_DOOR:
        return "restocking"
    return "frameSelected"

@app.post("/select_frame")
async def select_frame(input_videos: list[str] = Body(..., embed=False), save_output: bool = True):
    all_results = []
    logger.info(f"Got a request: {input_videos}")
    for input_video in input_videos:
        result = {}
        result["input_video"] = input_video
        try:
            video_result: VideoClassification = frame_selector.select_from_video(input_video)
            status = get_status(video_result)
            if status in ["noGoodFramesFound", "closedDoor", "restocking"]:
                result["output_file"] = ""
                result["status"] = status
                result["error"] = ""
            else:
                if save_output:
                    input_video_path = Path(input_video)
                    if RUNNING_IN_DOCKER:
                        output_dir = input_video_path.parent / "frameSelection"
                    elif Path("/datadisk/sdc").is_dir():
                        output_dir = input_video_path.parent / "frameSelection"
                    elif Path("/home/akshay/siteData").is_dir():
                        output_dir = input_video_path.parent / "frameSelection"
                    else:
                        data_dir = Path(__file__).parent.parent.parent / "data"
                        output_dir =  data_dir / "frameSelection"
                    if not output_dir.is_dir():
                        output_dir.mkdir()
                
                    output_file = output_dir / f"{input_video_path.stem}.jpg"
                    logger.info(f"Saving selected frame to: {output_file}")
                    output_frame = video_result.frame_classifications.result_frame
                    output_frame = cv2.cvtColor(output_frame, cv2.COLOR_RGB2BGR)
                    cv2.imwrite(str(output_file), output_frame)
                    result["output_file"] = f"{output_file}"
                result["status"] = "frameSelected"
                result["frame_index"] = video_result.frame_classifications.result_index
                # json_results = "[" + ",".join([r.json() for r in frame_results]) + "]"
                result["video_result"] = video_result.model_dump_json()
                result["error"] = ""
        except Exception as e:
            exception_message = str(e)
            exception_type = type(e).__name__
            traceback_info = traceback.format_exc()
            logger.error(f"Exception Type: {exception_type}")
            logger.error(f"Exception Message: {exception_message}")
            logger.error(f"Traceback Info:\n{traceback_info}")
            result["status"] = "error"
            if isinstance(e, VideoDecodeError):
                result["error"] = exception_message
            else:
                raise HTTPException(status_code=500, detail=exception_message)
        all_results.append(result)
    return JSONResponse(content=all_results)                
