#!/usr/bin/env python3

from detectron2.utils.logger import setup_logger
import torch
setup_logger()

# import some common libraries
import numpy as np
import os, json, cv2, random
# from google.colab.patches import cv2_imshow

# import some common detectron2 utilities
from detectron2 import model_zoo
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog
from detectron2.structures import BoxMode
from detectron2.utils.visualizer import ColorMode
from pathlib import Path
from matplotlib import pyplot as plt
from dataclasses import dataclass
from enum import Enum
from PIL import Image, ImageDraw, ImageFont, ImageColor
import cv2
from typing import List
from itertools import cycle
from argparse import ArgumentParser
from loguru import logger
from pydantic import BaseModel
from typing import Optional, ClassVar
from utils import encode_image, get_contour_from_mask
from tqdm.auto import tqdm
from configparser import ConfigParser
from utils import Timer

## This is for inference, so can read from config
config = ConfigParser()
config.read('inference.ini')
LOG_BOTTLE_OVERLAP_MARGIN = config.getboolean('DEFAULT', 'log_bottle_overlap_margin')
logger.info(f"LOG_BOTTLE_OVERLAP_MARGIN: {LOG_BOTTLE_OVERLAP_MARGIN}")


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('-m', '--model-path', help='path to model file to use for predictions', required=True)
    parser.add_argument('-i', '--input-dir', help='input-dir(s) with files to predict on. Should have batch sub dirs under it', required=True, nargs='+')
    parser.add_argument('-o', '--output-dir', help='Output directory to store the predictions', required=True)
    parser.add_argument('-d', '--url-prefix', help='The prefix to add for the url', default='http://localhost:8000')
    parser.add_argument('-t', '--trunc-index', help='index to truncate dataset', type=int, default=None)
    parser.add_argument('-T', '--threshold', help='Threshold for predictions', type=float, default=0.98)
    parser.add_argument('-O', '--overlap-margin', help='Overlap margin for bounding boxes', type=float, default=0.4)
    parser.add_argument('-f', '--format', help='Output format', choices=['polygon', 'mask'], default='polygon')
    parser.add_argument('-R', '--replace-mapping', help='Replace mapping for labels in the format orig:replacement', nargs="+", default=[])
    return parser.parse_args()


def get_replace_dict(replace_mapping):
    if replace_mapping is None:
        return {}
    if isinstance(replace_mapping, dict):
        return replace_mapping
    if not isinstance(replace_mapping, list):
        raise ValueError(f'Invalid replace mapping: {replace_mapping}. Expected list of strings in "label:new_label" format.')
    replace_dict = {}
    for replace in replace_mapping:
        split = replace.split(':')
        if len(split) == 1:
            orig = split[0]
            replacement = ''
        elif len(split) == 2:
            orig, replacement = split
        else:
            logger.error(f'Invalid replace mapping: {replace}')
            raise ValueError(f'Invalid replace mapping: {replace}')
        replace_dict[orig] = replacement
    return replace_dict


def imshow(image, figsize=None):
    if figsize is None:
        figsize = (20,20)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.figure(figsize=figsize)
    plt.imshow(image)

def read_jsonl(json_path):
    with open(json_path, 'r') as file:
        lines = file.readlines()
        return [json.loads(line) for line in lines]
    

code2skuType_d = {
    'C': 'can',
    'B': 'pet',
    'T': 'tetra',
    'G': 'rgb',
}
sku_types = [v for k, v in code2skuType_d.items()]

code2brand_d = {
    '7U': '7Up',
    'AMC': 'AmulMasti',
    'AML': 'AmulLassi',
    'AQW': 'AquafinaWater',
    'BIW': 'BisleriWater',
    'C': 'Coke',
    'CH': 'Charged',
    'CZ': 'CokeZero',
    'CD': 'DietCoke',
    'DUS': 'DukeSoda',
    'FR': 'Frooti',
    'FT': 'Fanta',
    'KIS': 'KinleySoda',
    'KIW': 'KinleyWater',
    'LI': 'Limca',
    'LN': 'LahoriNeembu',
    'LZ': 'LahoriZeera',
    'MD': 'Mirinda',
    # 'MIR': 'Mirinda',
    'MM': 'MinuteMaid',
    'MT': 'Monster',
    'MZ': 'Maaza',
    'P': 'Pepsi',
    'PZ': 'PepsiZero',
    'PT': 'Predator',
    'RB': 'RedBull',
    'RM': 'RealMango',
    'RRM': 'RioRawMango',
    'RWB': 'RioWildBerry',
    'RZ': 'RimZim',
    'SL': 'Slice',
    'SP': 'Sprite',
    'SRW': 'SunrichWater',
    'ST': 'Sting',
    'SWGA': 'SchweppesGA',
    'SWTW': 'SchweppesTW',
    'TH': 'ThumsUp',
    'pet': 'pet'
}
brand2code_d = {v: k for k, v in code2brand_d.items()}

def code2brandVolumeType(code):
    if code.lower() in sku_types:
        return code.lower(), '', code.lower()
    
    if code and '-' in code:
        splits = code.split('-')
        if len(splits) == 2:
            # format G-rgb or G-ThumsUp
            sku_code, brand_code = splits
            volume_code = ''
        elif len(splits) == 3:
            sku_code, brand_code, volume_code = splits
        else:
            logger.warning(f'Invalid code: {code}')
            return '', '', code
        sku_type = code2skuType_d.get(sku_code, sku_code)
        brand = code2brand_d.get(brand_code, brand_code)
        if len(volume_code) == 0:
            volume = None
        else:
            volume = int(volume_code)
    else:
        brand = ''
        volume = ''
        if code:
            sku_type = code.lower()
    return brand, volume, sku_type

def brandVolumeType2code(brand, volume, sku_type):
    brand_code = brand2code_d.get(brand, brand)
    sku_type2prefix = {v: k for k, v in code2skuType_d.items()}
    sku_prefix = sku_type2prefix.get(sku_type.lower(), sku_type.lower())
    if volume is None or len(volume) == 0:
        code = f"{sku_prefix}-{brand_code}"
    else:
        code = f"{sku_prefix}-{brand_code}-{int(volume):04d}"
    return code


class BBox(BaseModel):
    x1: int
    y1: int
    x2: int
    y2: int

    @property
    def center(self):
        return (self.x1 + self.x2) // 2, (self.y1 + self.y2) // 2

    def has_overlap(self, bbox, margin):
        if (self.x1 > bbox.x2 or self.x2 < bbox.x1 or self.y1 > bbox.y2 or self.y2 < bbox.y1):
            return False
        # MARGIN = 0.05
        x1 = max(self.x1, bbox.x1)
        y1 = max(self.y1, bbox.y1)
        x2 = min(self.x2, bbox.x2)
        y2 = min(self.y2, bbox.y2)
        intersection = (x2 - x1) * (y2 - y1)
        area1 = (self.x2 - self.x1) * (self.y2 - self.y1)
        area2 = (bbox.x2 - bbox.x1) * (bbox.y2 - bbox.y1)
        if intersection / area1 > margin or intersection / area2 > margin:
            if LOG_BOTTLE_OVERLAP_MARGIN:
                logger.warning(f"intersection/area1: {intersection/area1:0.2f} intersection/area2: {intersection/area2:0.2f} margin: {margin:0.2f}")
            return True
        return False
    
    def to_dict(self):
        return {
            'x1': self.x1,
            'y1': self.y1,
            'x2': self.x2,
            'y2': self.y2
        }

MIN_BOTTLE_HEIGHT = 10
MIN_BOTTLE_WIDTH = 5

class Detection(BaseModel):
    bbox: BBox
    pred_score: float
    pred_class: int
    polygon: List[List[int]]
    mask: List[List[bool]]
    label: str

    def __str__(self):
        return f"Detection(detecton={self.label}, score={self.pred_score:0.2f}, bbox={self.bbox})"

    def to_prodigy_dict(self, labels, format, label_mapping):
        if format == 'polygon':
            return self._to_polygon_dict(labels, label_mapping=label_mapping)
        elif format == 'mask':
            return self._to_mask_dict(labels, label_mapping=label_mapping)
        else:
            raise ValueError(f'Unknown format: {format}')
    
    def _to_polygon_dict(self, labels, label_mapping=None):
        label = labels[self.pred_class]
        if label_mapping:
            label = label_mapping.get(label, label)

        return {
            'label': label,
            'points': self.polygon,
            'type': 'polygon',
        }
    
    def _to_mask_dict(self, labels, label_mapping=None):
        label = labels[self.pred_class]
        if label_mapping:
            label = label_mapping.get(label, label)

        x1, y1, x2, y2 = self.bbox.x1, self.bbox.y1, self.bbox.x2, self.bbox.y2
        points = [[x1, y1], [x2, y1], [x2, y2], [x1, y2]]
        w, h = (x2 - x1), (y2 - y1)
        center_x = x1 + w // 2
        center_y = y1 + h // 2
        mask = self.mask
        mask = np.array(mask, dtype=np.uint8) * 255
        mask = encode_image(mask)
        # mask = cv2.resize(mask, (w, h))
        # mask = mask * 255
        # mask = Image.fromarray(mask)
        # mask = mask.convert('L')
        # mask = mask.resize((w, h))
        # mask = np.array(mask)
        # mask = mask.tolist()
        return {
            'label': label,
            'mask': mask,
            'type': 'rect',
            'points': points,
            'x': x1,
            'y': y1,
            'width': w,
            'height': h,
            'center': [center_x, center_y]
        }

    @property
    def height(self):
        return self.bbox.y2 - self.bbox.y1
    
    @property
    def width(self):
        return self.bbox.x2 - self.bbox.x1
    
    @property
    def min_y(self):
        return min([y for x, y in self.polygon])
    
    @property
    def max_y(self):
        return max([y for x, y in self.polygon])
    
    @property
    def min_x(self):
        return min([x for x, y in self.polygon])
    
    @property
    def max_x(self):
        return max([x for x, y in self.polygon])
    
    def has_overlap(self, detection, margin=0.05):
        bbox = detection.bbox
        return self.bbox.has_overlap(bbox, margin=margin)

class PredictedBottle(Detection):
    brand: Optional[str] = None
    volume: Optional[str] = None
    row: Optional[str] = None
    sku_type: Optional[str] = None
    code: Optional[str] = None
    brand_confidence: Optional[float] = None
    depth: float = 0.0

    def to_prodigy_dict(self, labels=None):
        if labels:
            label = labels[self.pred_class]
        else:
            label = brandVolumeType2code(self.brand, self.volume, self.sku_type)

        return {
            'label': label,
            'points': self.polygon,
            'type': 'polygon'
        }
    
    def to_dict(self):
        d = {
            'bbox': self.bbox.to_dict(),
            'pred_score': self.pred_score,
            'brand': self.brand,
            'volume': self.volume,
            'depth': self.depth,
            }
        if self.sku_type:
            d['sku_type'] = self.sku_type
        if self.code:
            d['code'] = self.code
        return d
    
    def update_code(self):
        self.code = brandVolumeType2code(self.brand, self.volume, self.sku_type)

    def update_from_code(self):
        self.brand, self.volume, self.sku_type = code2brandVolumeType(self.code)
        # logger.error(f"Updated from code: {self.code} -> {self.brand}, {self.volume}, {self.sku_type}")

    def has_overlap(self, other, margin=0.05):
        return self.bbox.has_overlap(other.bbox, margin=0.5)
    
    def is_valid_polygon(self, shelf_idx, b_label, image):
        points = [[max(0, p1), max(0, p2)] for (p1, p2) in self.polygon]
        if isinstance(points, list):
            p = np.array(points).astype(int)
        elif isinstance(points, np.ndarray):
            p = points.astype(int)
        else:
            raise Exception(f"Invalid type for points: {type(points)} points: {points}")
        min_x, max_x, min_y, max_y = p[:, 0].min(), p[:, 0].max(), p[:, 1].min(), p[:, 1].max()
        height, width = image.shape[:2]
        if not (0 <= min_x <= width and 0 <= max_x <= width):
            logger.warning(f"{shelf_idx}: {b_label}: min max not in bounds: x: {min_x}:{max_x} width: {width}")
            return False
        if not (0 <= min_y <= height and 0 <= max_y <= height):
            logger.warning(f"{shelf_idx}: {b_label}: min max not in bounds: y: {min_y}:{max_y} height: {height}")
            return False
        if (max_x - min_x < MIN_BOTTLE_WIDTH):
            logger.warning(f"{shelf_idx}: {b_label}: bottle width: {max_x - min_x} < {MIN_BOTTLE_WIDTH}")
            return False
        if (max_y - min_y < MIN_BOTTLE_HEIGHT):
            logger.warning(f"{shelf_idx}: {b_label}: bottle height: {max_y - min_y} < {MIN_BOTTLE_HEIGHT}")
            return False
        logger.debug(f"{shelf_idx}: {b_label}: min_x: {min_x} max_x: {max_x} width: {width} min_y: {min_y} max_y: {max_y} height: {height}")
        return True


class SegmentationPredictor:
    def __init__(self, model_path, threshold=0.98, point_threshold=15, url_prefix='http://localhost:8000', sort_fn=None, label_mapping=None, debug=False):
        
        self.model_dir = model_path.parent.parent
        self.sort_fn = sort_fn
        with open(self.model_dir / 'config.json', 'r') as file:
            config = json.load(file)
            self.thing_classes = config['labels']
            self.name = config['name']

        # self.name = name
        # self.thing_classes = sorted(list(thing_classes))
        self.model_path = model_path
        self.score_threshold = threshold
        self.url_prefix = url_prefix
        self.label_mapping = get_replace_dict(label_mapping)

        self.cfg = self._get_cfg()
        self.cfg.MODEL.WEIGHTS = str(model_path) # path to the model we just trained
        self.cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = self.score_threshold
        self.POINT_THRESHOLD = point_threshold
        
        logger.info(f'Loading model from: {self.cfg.MODEL.WEIGHTS} to {self.cfg.MODEL.DEVICE}')

        self.shelf_metadata = {
            'name': f'{self.name}_train', 
            'thing_classes': self.thing_classes
        }
        self.predictor = DefaultPredictor(self.cfg)
        colors = list(ImageColor.colormap.keys())

        self.label2color = {}
        for label, color in zip(self.thing_classes, cycle(colors)):
            self.label2color[label] = color
            
        self.text_width = 1
        self.text_size = 20
        self.debug = debug
        
    def _get_cfg(self):
        cfg = get_cfg()
        cfg.merge_from_file(model_zoo.get_config_file("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml"))
        # cfg.DATASETS.TRAIN = ("shelf_train",)
        # cfg.DATASETS.TEST = ()
        cfg.DATALOADER.NUM_WORKERS = 2
        cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url("COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml")  # Let training initialize from model zoo
        cfg.SOLVER.IMS_PER_BATCH = 8  # This is the real "batch size" commonly known to deep learning people
        cfg.SOLVER.BASE_LR = 0.00025  # pick a good LR
        cfg.SOLVER.MAX_ITER = 300    # 300 iterations seems good enough for this toy dataset; you will need to train longer for a practical dataset
        cfg.SOLVER.STEPS = []        # do not decay learning rate
        cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 128   # The "RoIHead batch size". 128 is faster, and good enough for this toy dataset (default: 512)
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(self.thing_classes)  # only has one class (ballon). (see https://detectron2.readthedocs.io/tutorials/datasets.html#update-the-config-for-new-datasets)
        # NOTE: this config means the number of classes, but a few popular unofficial tutorials incorrect uses num_classes+1 here.
        
        os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7   # set a custom testing threshold
        cfg.MODEL.DEVICE = 'cuda' if torch.cuda.is_available() else 'cpu'
        # cfg.MODEL.DEVICE = 'cpu'
        return cfg

    def _draw_bboxes(self, image, detections):
        draw = ImageDraw.Draw(image)
        font = ImageFont.load_default()
        # print(f'bboxes: {bboxes} labels: {labels}')
        # if labels is None:
        #     labels = [None for i in range(len(bboxes))]
        for detection in detections:
            draw.rectangle(detection.bbox, outline="black")
            if detection.pred_class is None:
                text = 'unknown'
                label = text
            else:
                label = self.shelf_metadata['thing_classes'][detection.pred_class]
                pct = detection.pred_score * 100
                text = f"{label}: {pct:03.2f}%"
            color = self.label2color[label]
            # print(f'lookup: {lookup} color: {color}')
            draw.rectangle(detection.bbox, outline=color, width=self.text_width)
            if text:
                # print(f'drawing label: {label} bbox:{bbox} color: {self.label2color[label]}')
                font_path = os.path.join(cv2.__path__[0], 'qt', 'fonts', 'DejaVuSans.ttf')
                font = ImageFont.truetype(font_path, size=self.text_size)
                curr_height = int(0.75 * (detection.bbox[3] - detection.bbox[1]))
                curr_height = 5
                # print(curr_height, label)
                draw.text((detection.bbox[0] + 10, detection.bbox[1] + curr_height), text=text, fill=self.label2color[label],
                          font=font, stroke_width=1)
            # image = self._draw_polygons(image, detections)
        return image
    
    def _get_bool_mask(self, bool_list):
        bool_list = ~bool_list
        b = np.array(bool_list, dtype=np.uint8)
        b = np.where(b > 0, 255, 0)
        image = cv2.merge([b, b, b]).astype('uint8')
        # int_array = numpy_array.astype(np.uint8)
        image = Image.fromarray(image)  # Scale the values to 0-255 for binary representation
        return image    

    def _get_mask_from_polygon(self, image, detection, color):
        mask = Image.new("L", image.size, 0)
        draw = ImageDraw.Draw(mask)
        polygon = [x for pair in detection.polygon for x in pair]
        draw.polygon(polygon, fill='red', outline=3, width=3)
        return mask

    def _draw_polygons(self, image, detections):
        draw = ImageDraw.Draw(image)
        for detection in detections:
            if detection.polygon:
                image = image.convert('RGB')
                
                for (x1, y1), (x2, y2) in zip(detection.polygon[:-1], detection.polygon[1:]):
                    label = self.shelf_metadata['thing_classes'][detection.pred_class]
                    color = self.label2color[label]
                    draw.line((x1, y1, x2, y2), width=3, fill=color)
                    background = image.copy().convert('L')
                    # draw2 = ImageDraw.Draw(background)
                    # x_max, y_max = background.size
                    # draw2.rectangle([0, 0, x_max, y_max], fill=color)
                    # mask = self._get_bool_mask(detection.mask).convert('L')
                    mask = self._get_mask_from_polygon(image, detection, color)
                    image = Image.composite(image, background, mask)
        return image

    def _get_overlap(self, detections, new_detection: Detection, margin=0.05):
        detection: Detection
        for i, detection in enumerate(detections):
            if detection.has_overlap(new_detection, margin=margin):
                return detection, i
        return None, None
    
    def create_detection(self, bbox, pred_score, pred_class, pred_mask, polygon, label):
        d = Detection(bbox=bbox, pred_score=pred_score, pred_class=pred_class, polygon=polygon, mask=pred_mask, label=label)
        return d

    def _predict(self, image_or_path, overlap_margin=0.05):
        is_path = False
        if isinstance(image_or_path, Path) or isinstance(image_or_path, str):
            image_or_path = str(image_or_path)
            image = cv2.imread(str(image_or_path))
            is_path = True
        elif isinstance(image_or_path, Image.Image):
            image = np.array(image_or_path)
        elif isinstance(image_or_path, np.ndarray):
            image = image_or_path
        else:
            raise ValueError(f'Unknown type: {type(image_or_path)}')
        # format is documented at https://detectron2.readthedocs.io/tutorials/models.html#model-output-format
        output = self.predictor(image)  
        detections, new_instances, instances, pred_classes = self._process_output_tensors(output, overlap_margin=overlap_margin)
        if self.debug:
            if is_path:
                logger.debug(f"{image_or_path} initial instances: {len(instances)} filtered instances: {len(new_instances)} threshold: {self.score_threshold} scores: {new_instances.scores}")
            else:
                logger.debug(f"initial instances: {len(instances)} filtered instances: {len(new_instances)} threshold: {self.score_threshold} scores: {new_instances.scores}")
        
        orig_labels = [self.thing_classes[pred_class] for pred_class in pred_classes]
        filtered_labels = [d.label for d in detections]
        if self.debug:
            logger.debug(f"orig_labels: {orig_labels} ->  filtered_labels: {filtered_labels}")            
        
        return detections, image, new_instances
    
    def _predict_batch(self, images, overlap_margin):
        inputs = torch.tensor(images)
        model = self.predictor.model
        model.eval()
        inputs = inputs.permute(0, 3, 1, 2).to(self.cfg.MODEL.DEVICE)
        batch = [{"image": t} for t in inputs]
        batch_output = model(batch)
        if len(batch_output) == 0:
            logger.warning(f"batch_output is empty")
            return None, None, None
        for output, image in zip(batch_output, images):
            # detections, image, new_instances = self._process_tensors(output, overlap_margin=overlap_margin)
            detections, new_instances, instances, pred_classes = self._process_output_tensors(output, overlap_margin=overlap_margin)
            yield detections, image, new_instances
        
    def _process_output_tensors(self, output, overlap_margin):
        scores = output['instances'].scores
        pred_classes = output['instances'].pred_classes
        pred_boxes = output['instances']._fields['pred_boxes']
        pred_boxes = [p.tolist() for p in pred_boxes]
        pred_masks = output["instances"].pred_masks.detach().cpu()
        # logger.info(f"pred masks: {pred_masks.shape}")
        all_detections = []
        detections = []

        for pred_score, pred_class, pred_box, pred_mask in zip(scores, pred_classes, pred_boxes, pred_masks):
            label = self.thing_classes[pred_class]
            d = {param: int(p) for param, p in zip(['x1', 'y1', 'x2', 'y2'], pred_box)}
            bbox = BBox(**d)
            mask = np.array(pred_mask).astype(np.uint8).tolist()

            polygon = get_contour_from_mask(mask, convex=False)
            # logger.info(f"polygon: {polygon}")
            if polygon is None:
                logger.warning(f"Skipping detection due to invalid mask: {mask}")
                continue
            d = self.create_detection(bbox, pred_score.item(), pred_class.item(), mask, polygon, label)
            if d is not None:
                all_detections.append(d)

        if self.sort_fn:
            all_detections = sorted(all_detections, key=self.sort_fn)

        if self.debug:
            for i, d in enumerate(all_detections):
                logger.debug(f"Detection[{i}]: {d}")
            
        for i, d in enumerate(all_detections):
            # d = Detection(bbox=bbox, pred_score=pred_score.item(), pred_class=pred_class.item(), polygon=inst_contour, mask=inst_mask.tolist(), label=label)
            overlap, overlap_index = self._get_overlap(detections, d, margin=overlap_margin)
            if overlap is not None:
                logger.warning(f'Overlapping detection found between {d} and {overlap}')
                if overlap_index is not None and overlap.pred_score < d.pred_score:
                    logger.info(f"Replacing detection: {overlap} with {d}")
                    detections[overlap_index] = d
                else:
                    if self.debug:
                        logger.debug(f"Skipping detection: {d}")
                continue
            if d.pred_score > self.score_threshold:
                detections.append(d)
        instances = output['instances']
        new_instances = instances[instances.scores > self.score_threshold]
        # TODO: handle the case when there is filtering due to overlap of bounding boxes. Currently this is not handled in _predict
        # new_instances need to be filtered based on overlaps

        return detections, new_instances, instances, pred_classes
    
    def _detectron_draw(self, image, instances):
        if self.debug:
            logger.debug(f"Detectron Drawing {len(instances)} instances")
        v = Visualizer(image[:, :, ::-1],
                    metadata=self.shelf_metadata,
                    scale=1.0,
                    instance_mode=ColorMode.IMAGE_BW   # remove the colors of unsegmented pixels. This option is only available for segmentation models
        )
        # logger.info(f"instance type: {type(instances)}")
        # predictions = [i for i in instances["instances"].to("cpu") if i.score > self.score_threshold]
        instances = instances.to("cpu")
        instances.remove('scores')
        out = v.draw_instance_predictions(instances)
        return out

    def predict(self, image_or_path, output_image=False, overlap_margin=0.05, sort_fn=None):
        t = Timer("SegmentationPredictor.predict()")
        t.start()
        detections, image, instances = self._predict(image_or_path, overlap_margin=overlap_margin)
        if self.debug and (isinstance(image_or_path, Path) or isinstance(image_or_path, str)):
                logger.info(f"{image_or_path} detected {len(detections)} instances")
        if output_image:
            # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            viz = self._detectron_draw(image, instances)
            output_image = viz.get_image()
            # output_image = cv2.cvtColor(output_image, cv2.COLOR_BGR2RGB)
            output_image = Image.fromarray(output_image)
        else:
            output_image = None
        
        if sort_fn is not None:
            detections = sorted(detections, key=sort_fn)

        if self.debug:        
            logger.debug(f"image shape (wxh) in predict() before: ({image.shape[1]},{image.shape[0]}) after: {output_image.size}")
        t.stop()
        return detections, output_image

    def predict_batch(self, images, overlap_margin=0.05, output_image=True):
        batch_detections = []
        batch_images = []
        for detections, image, new_instances in self._predict_batch(images, overlap_margin=overlap_margin):
            batch_detections.append(detections)
            if output_image:
                # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                viz = self._detectron_draw(image, new_instances)
                output_image = viz.get_image()
                # output_image = cv2.cvtColor(output_image, cv2.COLOR_BGR2RGB)
                output_image = Image.fromarray(output_image)
                batch_images.append(output_image)
            else:
                output_image = None
        return batch_detections, batch_images

    def to_prodigy(self, image_path, overlap_margin, format):
        r = {}
        detections, image, _ = self._predict(image_path, overlap_margin=overlap_margin)
        height, width = image.shape[0:2]
        spans = []
        d: Detection
        for d in detections:
            span_dict = d.to_prodigy_dict(self.thing_classes, format=format, label_mapping=self.label_mapping)
            spans.append(span_dict)
        r['spans'] = spans
        r['path'] = str(image_path)
        prefix = str(image_path.parent.parent.parent.parent.parent)
        url = str(image_path).replace(prefix, self.url_prefix)
        r['image'] = url
        r['width'] = width
        r['height'] = height
        r['answer'] = 'accept'
        return r
    
class BottlePredictor(SegmentationPredictor):
    def create_detection(self, bbox, pred_score, pred_class, pred_mask, polygon, label):
        d = PredictedBottle(bbox=bbox, pred_score=pred_score, pred_class=pred_class, polygon=polygon, mask=pred_mask, label=label, brand=None, volume=None)
        return d

def process_batch_dir(batch_dir, output_dir, predictor, format, overlap_margin=0.05, trunc_index=None):
    input_images = sorted(list(batch_dir.rglob('*.jpg')))
    json_dicts = []
    if trunc_index is not None:
        logger.info(f'Truncating input images to: {trunc_index}')
        input_images = input_images[:trunc_index]

    for image_path in tqdm(input_images, desc=f'{batch_dir.name}'):
        detections, output_image = predictor.predict(image_path, output_image=True, overlap_margin=overlap_margin)
        # output_image_path = prediction_dir / image_path.name
        # output_image.save(output_image_path)
        json_d = predictor.to_prodigy(image_path=image_path, overlap_margin=overlap_margin, format=format)
        json_dicts.append(json_d)

    output_file = output_dir / f'{batch_dir.parent.name}-{batch_dir.name}.jsonl'
    with open(output_file, 'w') as file:
        for json_d in json_dicts:
            file.write(json.dumps(json_d) + '\n')
        logger.info(f'Wrote {len(json_dicts)} predictions to {output_file}')


def main():
    setup_logger()
    args = parse_arguments()
    input_dirs = [Path(d) for d in args.input_dir]
    assert len(input_dirs) > 0, "No input directories provided"
    for d in input_dirs:
        assert d.exists(), f"Input directory: {d} does not exist"

    model_path = Path(args.model_path)
    assert model_path.exists(), f"Model path: {model_path} does not exist"

    output_dir = Path(args.output_dir)
    if not output_dir.exists():
        output_dir.mkdir()

    predictor = SegmentationPredictor(model_path=model_path, url_prefix=args.url_prefix, threshold=args.threshold, 
                                      label_mapping=args.replace_mapping, debug=False)
    for input_dir in input_dirs:
        batch_dirs = sorted([d for d in input_dir.iterdir() if d.is_dir() and d.name[0] != '.'])
        for batch_dir in batch_dirs:
            process_batch_dir(batch_dir, output_dir, predictor, format=args.format, overlap_margin=args.overlap_margin, trunc_index=args.trunc_index)

if __name__ == '__main__':
    main()

##
# Sample params:
# ./segmentationInference.py -m /media/akshay/datasets/coke/models/experimental/bottles-June-19/sku/model_final.pth -l bottle can gbottle object tetra -i ~/Downloads/temp/June-18/test/input -o ~/Downloads/temp/June-18/test/output -T 0.6 -O 0.7 -t 10 -n test
#
##