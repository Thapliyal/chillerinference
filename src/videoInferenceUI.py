import streamlit as st
import requests
import json
import base64
import io
from PIL import Image
import numpy as np
from pathlib import Path
import cv2
import pandas as pd
from utils import decode_image, get_blur_value
from pydantic import BaseModel
import re
import os
from shelfSettings import StoreManager, ShelfSettingException
from datetime import datetime
from classificationInference import VideoClassification, FrameClassification
from analyzeVideos import VideoManager, TOP_3_CLEAR, OPEN_BUT_NOT_TOP3_CLEAR
from utils import get_frames
from typing import List, Optional
from functools import partial

STORE_MAPPER = StoreManager()
IS_IN_DOCKER = os.environ.get("RUNNING_IN_DOCKER", False)

st.set_page_config(page_title="Chiller Inference", layout="wide")

def analyze(image_path, shelf_heights=None, shelf_widths=None, video_dir=None, frame_name=None):
    with open(image_path, "rb") as file:
        if IS_IN_DOCKER:
            url = "http://analyzechiller:9000/analyzeChiller"
        else:
            url = "http://127.0.0.1:9000/analyzeChiller"
        if shelf_heights is None:
            shelf_heights = [24.7, 23, 32, 37, 40.5]
        if shelf_widths is None:
            shelf_widths = [50, 50, 50, 50, 50]

        files = {"file": file}
        data = {"shelf_heights": str(shelf_heights), "shelf_widths": str(shelf_widths), "video_dir": str(video_dir), 
                "frame_name": str(frame_name), "evaluate_depth": st.session_state.enable_depth}
        
        response = requests.post(url, files=files, data=data)
        return response
    
def _predict_frames(input_video: Path, port=9002, endpoint="select_frame"):
    if IS_IN_DOCKER:
        url = f"http://frameSelector:{port}/{endpoint}"
    else:
        url = f"http://127.0.0.1:{port}/{endpoint}"

    # Make sure the path is converted to a string and put it in a list
    data = [str(input_video)]

    # Define parameters; save_output seems to be controlled via query parameters
    params = {"save_output": False}

    try:
        # Use json=data to match the list expected by the server
        response = requests.post(url, json=data, params=params)
        response.raise_for_status()  # Raises HTTPError for bad HTTP response statuses
        json_response = response.json()
    except requests.RequestException as e:
        response = e.response
        if response is not None:
            logger.error("Status Code:", response.status_code)
            logger.error("Response Body:", response.text)
        else:
            logger.error(f"Request failed without a response: {e}")
        return None

    logger.info(f"json_response: {json_response}")
    # logger.info(f"video_result: {json_response[0]['video_result']}")
    return json_response

_predict_sales_frames = partial(_predict_frames, port=9002, endpoint="select_sales_frame")

def predict_from_cache(input_video: Path):
    video_manager: VideoManager = get_video_manager()
    status = video_manager.get_video_status(input_video)
    if status is None:
        return [], [], None, None
    

    # Found in cache
    if status not in [TOP_3_CLEAR]:
        return [], [], None, status
    
    frame_classifications = []
    frame = video_manager.frame
    result = frame[frame.file == str(input_video)]
    if len(result) == 0:
        # should never happen
        return [], [], None, None
    
    row = result.iloc[0]
    categories = row.category.tolist()
    confs = row.confidence.tolist()
    category_i = [i for i, c in enumerate(categories) if c in [TOP_3_CLEAR]]
    if len(category_i) == 0:
        return [], [], None, OPEN_BUT_NOT_TOP3_CLEAR
    category_i = sorted(category_i, key=lambda i: confs[i], reverse=True)
    max_index = category_i[0]
    logger.debug(f"frame classifications from cache: {input_video}: video_status: {status} max_index: {max_index}")
    for index, (category, score) in enumerate(zip(row.category, row.confidence)):
        frame_classifications.append(FrameClassification(category=category, score=score))
        logger.debug(f"[{index}]: category: {category} score: {score}")
    # logger.info(f"frame_classifications from cache: {frame_classifications}")
    return get_frames(input_video), frame_classifications, max_index, status

def predict_frames(input_video: Path):
    frames, frame_classifications, frame_index, status = predict_from_cache(input_video)
    # status = None
    if status is not None:
        return frames, frame_classifications, frame_index
    
    logger.info(f"Cache: not found for: {input_video}")
    # result not found in cache
    json_response = _predict_frames(input_video)
    if json_response is None:
        return [], [], None
    result = json_response[0]
    if result["status"] in ["noGoodFramesFound", "restocking"]:
        return [], [], None
    if result["status"] == "error":
        return [], [], None
    if result["status"] in ["frameSelected"]:
        frames = get_frames(input_video)
        if len(frames) == 0:
            return [], [], None
        video_result = VideoClassification.model_validate_json(result["video_result"])
        # logger.info(f"video_result: {video_result.frame_classifications}")
        return frames, video_result.frame_classifications.frame_classifications, result["frame_index"]
    return [], [], None

def predict_sales_frames(input_video: Path, frames: Optional[List[np.ndarray]] = None):
    json_response = _predict_sales_frames(input_video)
    if json_response is None:
        return [], [], None
    result = json_response[0]
    if result["status"] in ["noGoodFramesFound"]:
        return [], [], None
    if result["status"] == "error":
        return [], [], None
    if result["status"] == "frameSelected":
        if frames is None:
            frames = get_frames(input_video)
        if len(frames) == 0:
            return [], [], None
        video_result = VideoClassification.model_validate_json(result["video_result"])
        # logger.info(f"video_result: {video_result.frame_classifications}")
        return frames, video_result.frame_classifications.frame_classifications, result["frame_index"]
    return [], [], None

@st.cache_resource
def get_logger():
    from loguru import logger

    log_file = LOG_DIR / "videoInferenceUI.log"
    logger.add(
        log_file,
        format="{time}|{file}|{line}|{thread}|{level}|{message}",
        colorize=True,
    )
    return logger

def get_video_manager():
    return VideoManager()

def get_stores():
    global SITE_DIR
    dirs = sorted([d for d in SITE_DIR.iterdir() if d.is_dir() and d.name[0] != "."])
    dirs = [d for d in dirs if get_store_name(d) is not None]
    return dirs

def get_dates_for_store(store_dir):
    dates = sorted([d for d in store_dir.iterdir() if d.is_dir() and d.name[0] != "."])
    return dates

def get_video_files(date_dir):
    files = [f for f in sorted(date_dir.glob("*.avi")) if f.is_file() and f.name[0] != "." and cv2.VideoCapture(str(f)).isOpened()]
    filtered = []
    filtered_status = []
    for f in files:
        status = video_manager.get_video_status(f)
        if status in [None, TOP_3_CLEAR]:
            # logger.info(f"{f}: {status}")
            filtered.append(f)
            filtered_status.append(status)
    logger.info(f"Filtered: [{len(filtered)}/{len(files)}] for date: {date_dir.name}")
    return filtered

def get_timestamp_from_video(video_file):
    epoch_string = video_file.stem.split("_")[-2]
    epoch_time = float(epoch_string) / 1000
    return datetime.fromtimestamp(int(epoch_time)).strftime('%H:%M')

def get_store_name(store_dir):
    try:
        shelf_setting = STORE_MAPPER.get_shelf_settings(store_dir)
    except ShelfSettingException:
        return None
    if shelf_setting is None:
        return None
    if shelf_setting.asset_name is None:
        return shelf_setting.name
    else:
        return f"{shelf_setting.asset_name:3s}: {shelf_setting.name}"

def on_click_prev():
    if st.session_state.index == 0:
        return
    st.session_state.index -= 1
    logger.debug(f"index decremented to: {st.session_state.index}")


def on_click_next():
    if st.session_state.index == st.session_state.num_files - 1:
        return
    st.session_state.index += 1
    logger.debug(f"index incremented to: {st.session_state.index}")


def on_slider_change():
    st.session_state.index = st.session_state.file_slider
    logger.debug(f"index changed to: {st.session_state.index}")


def on_radio_change():
    st.session_state.cache_enabled = st.session_state.radio_cache_enabled


def change_language():
    pass
    st.session_state.language = st.session_state.lang_radio


def get_curr_path(files):
    if st.session_state.num_files == 0:
        return None
    curr_path = files[st.session_state.index]
    return curr_path

def on_video_selection_change():
    st.session_state.index = None

def on_date_selection_change():
    st.session_state.index = None

def on_store_selection_change():
    st.session_state.index = None

####################################################################################################################
# Initializations
####################################################################################################################
# initialize()
# args = get_args()
# g_debug = args.debug
g_debug = True

BASE_DIR = Path(__file__).parent.parent
LOG_DIR = BASE_DIR / "logs"
DATA_DIR = BASE_DIR / "data"
# SITE_DIR = DATA_DIR / "chillerdata"
if IS_IN_DOCKER:
    SITE_DIR = Path("/media/data")
else:
    if Path("/home/akshay/datasets").is_dir():
        SITE_DIR = Path("/home/akshay/siteData")
    else:
        SITE_DIR = Path('/media/akshay/datasets/coke/siteData')
for dir in [DATA_DIR, LOG_DIR]:
    if not dir.exists():
        dir.mkdir()

logger = get_logger()
video_manager = get_video_manager()
####################################################################################################################
# User Interface
####################################################################################################################
INDEX = "index"
if INDEX not in st.session_state:
    st.session_state.index = None
if "sales_index" not in st.session_state:
    st.session_state.sales_result_index = None

NUM_FILES = "num_files"
if NUM_FILES not in st.session_state:
    st.session_state.num_files = 0

store_dirs = get_stores()
store_names = [get_store_name(store) for store in store_dirs]
store_name2dir = {store_name: store_dir for store_name, store_dir in zip(store_names, store_dirs) if store_name is not None}
store_names = sorted([s for s in store_names if s is not None])
store_selection = st.selectbox("Select Store", store_names, index=0, on_change=on_store_selection_change)
# store_i = store_names.index(store_selection)
# store = store_dirs[store_i]
store = store_name2dir[store_selection]

store_dates = get_dates_for_store(store)
store_dates = sorted(store_dates, reverse=True)
store_date_names = [d.name for d in store_dates]
logger.debug(f"store_dates: {store_date_names}")
store_date_selection = st.selectbox("Select Date", store_date_names, index=0, on_change=on_date_selection_change)
store_i = store_date_names.index(store_date_selection)
store_date = store_dates[store_i]

video_files = get_video_files(store_date)
logger.info(f"store: {store} store_date: {store_date} video_files: {len(video_files)}")
if len(video_files) == 0:
    st.write("No video files found")
    st.stop()
time_stamps = [get_timestamp_from_video(video_file) for video_file in video_files]
video_file_selection = st.selectbox("Select Video", time_stamps, index=0, on_change=on_video_selection_change)
video_i = time_stamps.index(video_file_selection)
video_file = video_files[video_i]
st.write(f"Selected video file: {video_file}")
logger.info(f"Selected video file: {video_file}")

st.session_state.sales_result_index = None
frames, predictions, result_index = predict_frames(video_file)
if len(frames) == 0:
    st.write(":warning: No frames found in the video")
elif result_index is None:
    st.write("No good frames found in the video")
elif len(frames) > 0:
    if result_index is not None and st.session_state.index is None:
        st.session_state.index = result_index

    st.session_state.num_files = len(frames)

    # sales_frames, sales_predictions, sales_result_index = predict_sales_frames(video_file)
    # if sales_result_index is not None:
    #     st.session_state.sales_result_index = sales_result_index

    with st.expander("Details"):
        st.markdown("---")
        # if sales_result_index is None:
        #     st.write(":warning: No sales frames found in the video")
        # else:
        #     st.write(f"Sales Frame: {sales_result_index}")
        #     pred_string = ", ".join([f"{i:02d}-{p.category}" for i, p in enumerate(sales_predictions)])
        #     st.write(f"[{pred_string}]")

        # st.markdown("---")
        st.write(f"index: {st.session_state.index} num_files: {st.session_state.num_files} result_index: {result_index} predictions: {len(predictions)}")
        
        pred_string = ", ".join([f"{i:02d}-{p.category}" for i, p in enumerate(predictions)])
        st.write(f"[{pred_string}]")
        st.markdown("---")
    
    mac_id = f"{video_file.parent.parent.name}"
    timestamp = f"{video_file.parent.name}"
    frame_name = f"{video_file.parent.parent.name}/{video_file.parent.name}/{video_file.name}: frame_{st.session_state.index:02d}"
    video_dir = f"{video_file.parent.parent.name}/{video_file.parent.name}/{video_file.stem}"
    frame_name = f"frame_{st.session_state.index:02d}"

    col_prev, col_slider, col_next = st.columns([2, 10, 2])
    col_prev.button("Prev", on_click=on_click_prev)
    if st.session_state.num_files > 1:
        col_slider.slider(
            "Slider",
            min_value=0,
            max_value=st.session_state.num_files - 1,
            value=st.session_state.index,
            on_change=on_slider_change,
            key="file_slider",
        )
    col_next.button("Next", on_click=on_click_next)

    ######################################################################################################################
    # Image Analysis
    ######################################################################################################################
    if not "enable_depth" in st.session_state:
        st.session_state.enable_depth = False
    if not "jump_to_sales" in st.session_state:
        st.session_state.jump_to_sales = False
    c1, c2, c3, c4 = st.columns([1, 1, 1, 1])
    enable_depth_button = c2.checkbox("Enable Depth", value=st.session_state.enable_depth)
    if enable_depth_button != st.session_state.enable_depth:
        st.session_state.enable_depth = enable_depth_button
    jump_to_sales_button = c3.checkbox("Jump to Sales Frame", value=st.session_state.jump_to_sales)
    if jump_to_sales_button != st.session_state.jump_to_sales:
        st.session_state.jump_to_sales = jump_to_sales_button

    with open(video_file, "rb") as file:
        c4.download_button(
            label="Download Video File",
            data=file,
            file_name=f"{video_file.name}",  
            mime="application/octet-stream"  # Adjust MIME type as needed
        )

    st.caption(f"Current Frame: {frame_name}: {predictions[st.session_state.index].category}")

    if st.session_state.jump_to_sales:
        if st.session_state.sales_result_index is None:
            st.header("No sales frames found in the video")
        else:
            st.session_state.index = st.session_state.sales_result_index
    
    frame = frames[st.session_state.index]
    blur_value = get_blur_value(frame)
    curr_path = "/tmp/frame.jpg"
    cv2.imwrite(curr_path, frame)
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    c1.write(f"Blur Value: {blur_value}")

    col1, col2, col3 = st.columns([6, 6, 6])
    input_image = cv2.imread(str(curr_path))
    input_image = cv2.cvtColor(input_image, cv2.COLOR_BGR2RGB)
    logger.debug(f"curr_path: {curr_path}")
    # with col1:
    #     st.image(input_image, use_container_width=True)

    placeholder1 = col1.empty()
    placeholder1.image(input_image, use_container_width=True)

    placeholder2 = col2.empty()
    placeholder2.image(input_image, use_container_width=True)

    placeholder3 = col3.empty()

    # shelf_heights, shelf_widths = get_shelf_settings(curr_path, store_shelf_mappings)
    shelf_setting = STORE_MAPPER.get_shelf_settings(video_file)
    response = analyze(curr_path, shelf_heights=shelf_setting.shelf_heights, shelf_widths=shelf_setting.shelf_widths, video_dir=video_dir, frame_name=frame_name)
    status_code = response.status_code
    response = response.json()

    if "shelf_image" in response:
        shelf_image = response["shelf_image"]
        shelf_image = decode_image(shelf_image)
        if shelf_image is not None:
            placeholder1.image(shelf_image, use_container_width=True)


    if st.session_state.enable_depth:
        if "depth_image" in response:
            depth_image = response["depth_image"]
            depth_image = decode_image(depth_image)
            if depth_image is not None:
                placeholder2.image(depth_image, use_container_width=True)
    else:
        if "image" in response:
            image = response["image"]
            image = decode_image(image)
            if image is not None:
                placeholder2.image(image, use_container_width=True)

    # p3 = placeholder3
    with col3:
        if status_code != 200:
            st.write("Status Code: ", status_code)
            st.markdown("---")

        if "chiller" in response:
            chiller = response["chiller"]
            for shelf in chiller:
                if "label" in shelf:
                    st.write(shelf["label"])
                if "bottles" in shelf:
                    shelf_string = ""
                    for b, bottle in enumerate(shelf["bottles"]):
                        if "pred_score" not in bottle:
                            bottle["pred_score"] = "NA"
                        if "volume" not in bottle:
                            bottle["volume"] = "NA"
                        if "brand" not in bottle:
                            bottle["brand"] = "NA"
                        # st.write(bottle["brand"], bottle["volume"], bottle["pred_score"])
                        if bottle["volume"] == "":
                            shelf_string += f"{bottle['brand']}"
                        else:
                            # shelf_string += f"{bottle['brand']}"
                            shelf_string += f"{bottle['brand']}-{bottle['volume']} "
                        if b < len(shelf["bottles"]) - 1:
                            shelf_string += ", "
                    st.write(shelf_string)
                st.markdown("---")

            st.write(chiller)
    st.markdown("---")
