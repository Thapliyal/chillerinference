from pydantic import BaseModel
import re
from typing import List, Optional, Union
from datetime import date
from pathlib import Path
from loguru import logger
import pandas as pd
import ast


def load_shelf_settings_frame(file_path):
    assert file_path and file_path.is_file(), f"Unable to open shopConfig.csv: {file_path}. Please upload and try again."
    # if file_path is None:
    #     file_path = Path("/home/akshay/workspace/computervision/commonvision/data/shopConfig.csv")
    frame = pd.read_csv(file_path)
    frame.rename(columns={"retailer_name": "name"}, inplace=True)
    frame = frame[~ pd.isna(frame.shelf_widths)]
    frame = frame[frame.enabled == True]
    frame['regex'] = frame.name.apply(lambda x: re.sub('\s+', r'\\s+', x))
    frame['shelf_heights'] = frame['shelf_heights'].apply(ast.literal_eval)
    frame['shelf_widths'] = frame['shelf_widths'].apply(ast.literal_eval)
    frame['region'] = frame['region'].apply(lambda x: x.lower() if pd.notna(x) else x)
    return frame

def get_sort_order(setting):
    import re
    asset_name = setting.asset_name
    match = re.match("([A-Za-z]+)(\d+)", asset_name, re.IGNORECASE)
    if match is None:
        return setting.asset_name, setting.asset_name
    return match.group(1), int(match.group(2))

def load_shelf_settings(file_path):
    frame = load_shelf_settings_frame(file_path)
    dicts = frame.to_dict(orient='records')
    settings = [StoreShelfSettings(**d) for d in dicts]
    settings = [s for s in settings if s.enabled]
    settings = sorted(settings, key=lambda x: get_sort_order(x))
    return settings

class StoreShelfSettings(BaseModel):
    name: str
    asset_name: Optional[str] = None
    regex: str
    mac_id: Optional[str] = None
    shelf_heights: list[float]
    shelf_widths: list[float]
    start_date: Optional[date] = None
    end_date: Optional[date] = None
    region: Optional[str] = None
    enabled: Optional[bool] = False

# store_shelf_mappings = [StoreShelfSettings(**d) for d in store_shelf_mappings_d]
SHOP_CONFIG_PATH = Path(__file__).parent.parent / "data" / "shopConfig.csv"

class DatasetFileRange(BaseModel):
    start: int
    end: int 

class DatasetShelfMapping(BaseModel):
    dataset: str
    shelf_setting: StoreShelfSettings
    range: Optional[DatasetFileRange] = None

class ShelfSettingException(Exception):
    pass

WESTERN_BEFORE_MAY_15 = StoreShelfSettings(name="Lab1 Western 14 Caser Mumbai", 
                                           regex="IMAGE", 
                                           shelf_heights=[33.5, 29, 40, 27, 29], 
                                           shelf_widths=[50, 50, 50, 50, 50], 
                                           start_date="2024-04-01", end_date="2024-05-15",
                                           region="lab")

WESTERN_AFTER_MAY_15 = StoreShelfSettings(name="Lab1 Western 14 Caser Mumbai", 
                                           regex="IMAGE", 
                                           shelf_heights=[24.7, 23, 32, 37, 40.5], 
                                           shelf_widths=[50, 50, 50, 50, 50],
                                           start_date="2024-05-01", end_date=None,
                                           region="lab")

FRIGO_BEFORE_MAY_15 = StoreShelfSettings(name="Lab2 Frigo 9 Caser Mumbai", 
                                           regex="IMAGE", 
                                           shelf_heights=[18.5, 17.5, 22, 28.5, 38], 
                                           shelf_widths=[50, 50, 50, 50, 50], 
                                           start_date="2024-04-01", end_date="2024-05-15",
                                           region="lab")

FRIGO_AFTER_MAY_15 = StoreShelfSettings(name="Lab2 Frigo 9 Caser Mumbai", 
                                           regex="IMAGE", 
                                           shelf_heights=[20, 21, 21, 26, 37], 
                                           shelf_widths=[50, 50, 50, 50, 50],
                                           start_date="2024-05-01", end_date=None,
                                           region="lab")


dataset_shelf_mappings = [
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=FRIGO_BEFORE_MAY_15, range=DatasetFileRange(start=0, end=499)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=FRIGO_BEFORE_MAY_15, range=DatasetFileRange(start=500, end=579)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=WESTERN_BEFORE_MAY_15, range=DatasetFileRange(start=580, end=999)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=WESTERN_BEFORE_MAY_15, range=DatasetFileRange(start=1000, end=1182)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=FRIGO_BEFORE_MAY_15, range=DatasetFileRange(start=1183, end=1499)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=FRIGO_BEFORE_MAY_15, range=DatasetFileRange(start=1500, end=1542)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=WESTERN_BEFORE_MAY_15, range=DatasetFileRange(start=1543, end=1907)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=FRIGO_BEFORE_MAY_15, range=DatasetFileRange(start=1908, end=1999)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=FRIGO_BEFORE_MAY_15, range=DatasetFileRange(start=2000, end=2086)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=WESTERN_BEFORE_MAY_15, range=DatasetFileRange(start=2087, end=2499)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=WESTERN_BEFORE_MAY_15, range=DatasetFileRange(start=2500, end=2548)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=FRIGO_BEFORE_MAY_15, range=DatasetFileRange(start=2549, end=2808)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=WESTERN_BEFORE_MAY_15, range=DatasetFileRange(start=2809, end=2999)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=WESTERN_BEFORE_MAY_15, range=DatasetFileRange(start=3000, end=3089)),
    DatasetShelfMapping(dataset='Set19-May-11', shelf_setting=FRIGO_BEFORE_MAY_15, range=DatasetFileRange(start=3090, end=3409)),

    DatasetShelfMapping(dataset='Set20-May-14', shelf_setting=WESTERN_BEFORE_MAY_15, range=DatasetFileRange(start=0, end=519)),
    DatasetShelfMapping(dataset='Set20-May-14', shelf_setting=FRIGO_BEFORE_MAY_15, range=DatasetFileRange(start=520, end=1583)),
    DatasetShelfMapping(dataset='Set20-May-14', shelf_setting=WESTERN_BEFORE_MAY_15, range=DatasetFileRange(start=1584, end=1896)),

    DatasetShelfMapping(dataset='Set28-June-10', shelf_setting=WESTERN_AFTER_MAY_15, range=None),
    DatasetShelfMapping(dataset='Set29-June-10', shelf_setting=FRIGO_AFTER_MAY_15, range=None),
        
    DatasetShelfMapping(dataset='Set31-June-12', shelf_setting=WESTERN_AFTER_MAY_15, range=None),
    DatasetShelfMapping(dataset='Set32-June-12', shelf_setting=FRIGO_AFTER_MAY_15, range=None),
    DatasetShelfMapping(dataset='Set33-June-22', shelf_setting=WESTERN_AFTER_MAY_15, range=None),
    DatasetShelfMapping(dataset='Set34-June-22', shelf_setting=FRIGO_AFTER_MAY_15, range=None),
    DatasetShelfMapping(dataset='Set35-June-22', shelf_setting=WESTERN_AFTER_MAY_15, range=None),
    DatasetShelfMapping(dataset='Set36-June-22', shelf_setting=FRIGO_AFTER_MAY_15, range=None),

]
class StoreManager:
    def __init__(self, raise_exception=True, debug=False, silent=False):
        logger.info(f"Initializing StoreManager with raise_exception: {raise_exception}")
        self.debug = debug
        self.silent = silent
        store_shelf_mappings = load_shelf_settings(SHOP_CONFIG_PATH)
        self.store_mappings = store_shelf_mappings

        if debug:
            for mapping in self.store_mappings:
                logger.info(f"Loaded store: {mapping.asset_name:6s}: {mapping.mac_id:12s}: {mapping.name}")
            logger.info("")

        self.dataset_mappings = dataset_shelf_mappings
        self.raise_exception = raise_exception

    def is_asset_of_interest(self, asset_name):
        if asset_name[0] in ['V', 'P']:
            return True
        if asset_name[0:2] in ['MH', 'BH']:
            return True
        if asset_name in ["DemoUnitPune", "Rajal1", "Rajal2", "Milan1", "Milan2", "LabFrigo"]:
            return True
        return False

    def reload(self):
        store_shelf_mappings = load_shelf_settings(SHOP_CONFIG_PATH)
        self.store_mappings = store_shelf_mappings

    def get_shelf_settings_from_dataset(self, dataset_name, file_path):
        file_path = Path(file_path)
        for d in self.dataset_mappings:
            if d.dataset == dataset_name:
                if d.range is None:
                    return d.shelf_setting
                file_num = int(file_path.stem.split("_")[-1])
                if d.range.start <= file_num <= d.range.end:
                    return d.shelf_setting
        if self.raise_exception:
            raise ShelfSettingException(f"Unable to find shelf settings for dataset: {dataset_name} file: {file_path}")
        else:
            if not self.silent:
                logger.warning(f"Unable to find shelf settings for dataset: {dataset_name} file: {file_path}")
            return None
    
    def get_shelf_setting_from_store(self, file_path):
        file_path = Path(file_path)
        # stores have two patterns
        # 1. using mac id: eg. C82E18230DD8_1717236630792_93200012.jpg
        # 2. using store name eg. Vijaylakshmi General Store_2024-05-29_13-03-47_00018.jpg
        # TODO: need to extract dates from each and compare against db
        for mapping in self.store_mappings:
            if re.search(mapping.regex, file_path.stem, re.IGNORECASE):
                if self.debug:
                    logger.debug(f"found regex: {mapping.regex} in file: {file_path.stem} mapping: {mapping}")
                return mapping
            regex = re.sub('\'', '', mapping.regex)
            if re.search(regex, file_path.stem, re.IGNORECASE):
                if self.debug:
                    logger.debug(f"found regex: {mapping.regex} in file: {file_path.stem} mapping: {mapping}")
                return mapping
        for mapping in self.store_mappings:
            if mapping.mac_id and mapping.mac_id.lower() in file_path.stem.lower():
                if self.debug:
                    logger.debug(f"found mac id: {mapping.mac_id} in file: {file_path.stem} mapping: {mapping}")
                return mapping
        
        ## TODO: with asset names BH1 and BH10 will both batch file with BH10
        search_results = []
        for mapping in self.store_mappings:
            if mapping.asset_name and mapping.asset_name.lower() in file_path.stem.lower():
                if self.debug:
                    logger.debug(f"found asset name: {mapping.asset_name} in file: {file_path.stem} mapping: {mapping}")
                search_results.append(mapping)
        # assuming store_mappings are sorted, return the highest match
        if len(search_results) > 0:
            return search_results[-1]
            
        if '_' in file_path.stem:
            store_name = file_path.stem.split('_')[0].strip()
            # TODO: remove such crazy hardcoding once shopConfig is automated
            store_name = store_name.replace('2024', '')
            store_name = store_name.replace('2025', '')
            for mapping in self.store_mappings:
                mapping_name = mapping.name.strip()
                # TODO: remove such crazy hardcoding once shopConfig is automated
                mapping_name = mapping_name.replace('_2024', '')
                mapping_name = mapping_name.replace('_2025', '')
                if store_name.lower() in mapping_name.lower() or mapping_name.lower() in store_name.lower():
                    if self.debug:
                        logger.debug(f"found store name: {store_name} in file: {file_path.stem} mapping: {mapping}")
                    return mapping
                
        if self.raise_exception:
            raise ShelfSettingException(f"Unable to find shelf settings for store: {file_path}")
        else:
            logger.warning(f"Unable to find shelf settings for store: {file_path}")
            return None

    def get_shelf_settings(self, file_path):
        file_path = Path(file_path)
        # dataset collection software always generates files with 2024_ prefix
        # if it is renamed by preprocessing it will have IMAGE prefix
        # everything else is assumed to be store data
        if file_path.name[:5] in ["IMAGE", "2024-"]:
            p = str(file_path)
            m = re.search(r"datasets.*?\/(Set\d+.*?)\/", p)
            if m:
                dataset_name = m.group(1)
                setting = self.get_shelf_settings_from_dataset(dataset_name, file_path)
            else:
                if self.raise_exception:
                    raise ShelfSettingException(f"Unable to find shelf settings for dataset: {file_path}")
                return None
        else:
            setting = self.get_shelf_setting_from_store(file_path)
        return setting
    
    def get_mac_id_from_store_name(self, name):
        for mapping in self.store_mappings:
            if name.lower() in mapping.name.lower():
                if self.debug:
                    logger.debug(f"Matching name: [{name}] with: [{mapping.name}] mac_id: [{mapping.mac_id}]")
                return mapping.mac_id
        return None
    
    def get_mac_id_from_asset_name(self, asset_name):
        for mapping in self.store_mappings:
            if asset_name.lower() == mapping.asset_name.lower():
                if self.debug:
                    logger.debug(f"Matching name: [{asset_name}] with: [{mapping.asset_name}] mac_id: [{mapping.mac_id}]")
                return mapping.mac_id
        return None
    
    def get_asset_name_from_mac_id(self, mac_id):
        for mapping in self.store_mappings:
            if mac_id.lower() == mapping.mac_id.lower():
                return mapping.asset_name
        return None
    
    def get_mac_id_from_path(self, path, site_dir, raise_exception=True):
        if site_dir is None:
            site_dir = "siteData"
        sub_path = str(path).split(str(site_dir))[-1]
        if sub_path is None:
            if raise_exception:
                raise Exception(f"Unable to extract mac_id from path: {path}")
            else:
                return None
        return sub_path.split('/')[1]
    
    def get_store_names_from_path(self, path, site_dir=None):
        mac_id = self.get_mac_id_from_path(path, site_dir, raise_exception=True)
        for mapping in self.store_mappings:
            if mapping.mac_id == mac_id:
                return mapping.name, mapping.asset_name
        return None, None
    
    def get_mac_ids(self):
        return [s.mac_id for s in self.store_mappings if s.enabled]
