import torch
from torchvision.transforms import v2
from PIL import Image

IMG_WIDTH = 224
IMG_HEIGHT = 224

def ar_resize_height(image, new_height):
    width, height = image.size
    aspect_ratio = width / height
    new_width = int(aspect_ratio * new_height)
    resized_image = image.resize((new_width, new_height), Image.LANCZOS)
    return resized_image

def resize_and_pad(image, target_size, min_height, max_height):
    if min_height == max_height or image.height < (target_size * 0.75):
        resized_image = image
    else:
        new_height = torch.randint(min_height, max_height + 1, (1,)).item()
        resized_image = ar_resize_height(image, new_height)
        # random_resize_flag = torch.randint(0, 2, (1,)).item()
        # if random_resize_flag == 0:
        #     # min_height = max(min_height, int(image.height * 0.75))
        #     # max_height = min(max_height, int(image.height * 1.25))
        #     new_height = torch.randint(min_height, max_height + 1, (1,)).item()
        #     resized_image = ar_resize_height(image, new_height)
        # else:
        #     resized_image = ar_resize_height(image, target_size)

    new_width, new_height = resized_image.size
    if new_height > target_size:
       new_height = target_size
       resized_image = ar_resize_height(resized_image, target_size)
       new_width, new_height = resized_image.size
    
    # Create a new image with the target size and paste the resized image into it
    new_image = Image.new("RGB", (target_size, target_size))
    paste_x = (target_size - new_width) // 2
    paste_y = (target_size - new_height) // 2
    new_image.paste(resized_image, (paste_x, paste_y))
    
    return new_image

def random_resize_and_pad_60_to_224(img):
    return resize_and_pad(img, 224, 60, 224)

def random_resize_and_pad_100_to_224(img):
    return resize_and_pad(img, 224, 100, 224)

def fixed_resize_and_pad_transform(img):
    return resize_and_pad(img, 224, 224, 224)

DEFAULT_TRAIN_TRANSFORMS = v2.Compose([
        v2.RandomApply([v2.GaussianBlur(kernel_size=5, sigma=(1, 8.0))], p=0.5),
        v2.Lambda(random_resize_and_pad_60_to_224),    
        v2.RandomRotation(20),
        v2.ColorJitter(brightness=0.1, contrast=0.4, saturation=0.1),
        v2.PILToTensor(),
        v2.ToDtype(torch.float32, scale=True),
    ])

RGB_TRAIN_TRANSFORMS = v2.Compose([
        v2.RandomApply([v2.GaussianBlur(kernel_size=7, sigma=(1, 8.0))], p=0.05),
        v2.Lambda(random_resize_and_pad_100_to_224),    
        v2.RandomApply([v2.RandomRotation(5), v2.RandomPerspective()], p=0.25),
        v2.ColorJitter(brightness=(0.4, 1.2), contrast=(0.08, 1.2), saturation=(0.1, 1.3)),
        v2.PILToTensor(),
        v2.ToDtype(torch.float32, scale=True),
    ])

VOLUME_TRAIN_TRANSFORMS = v2.Compose([
        v2.Resize(IMG_WIDTH),
        v2.RandomPerspective(),
        v2.RandomRotation(10),
        v2.GaussianBlur(7, sigma=(1, 10.0)),
        v2.ColorJitter(brightness=0.6, contrast=0.5),
        v2.Resize((IMG_WIDTH, IMG_HEIGHT)),
        v2.PILToTensor(),
        v2.ToDtype(torch.float32, scale=True),
    ])

BASIC_TRAIN_TRANSFORMS = v2.Compose([
    # v2.RandomRotation(10),
    v2.Lambda(fixed_resize_and_pad_transform),
    v2.PILToTensor(),
    v2.ToDtype(torch.float32, scale=True),
    ])

VALIDATION_TRANSFORMS = v2.Compose([
    v2.Lambda(fixed_resize_and_pad_transform),
    v2.PILToTensor(),
    v2.ToDtype(torch.float32, scale=True),
    ])
