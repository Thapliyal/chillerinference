#!/usr/bin/env python3

from pathlib import Path
import shutil
from argparse import ArgumentParser
from tqdm.auto import tqdm
from loguru import logger


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", type=str, help="Input folder path")
    parser.add_argument("-s", "--splits", help="The names of the dir(s) to split into", required=True, nargs="+")
    parser.add_argument("-t", "--test", action="store_true", help="Only simulate, do not actually split the files")
    return parser.parse_args()


def main():
    args = parse_args()

    input_folder = Path(args.input)
    if not input_folder.is_dir():
        raise ValueError(f"Input folder: {input_folder} doesn't exist")
    
    test_mode = args.test
    
    splits = args.splits
    num_splits = len(splits)
    files = list(input_folder.glob("*.jpg"))
    files_per_split = len(files) // num_splits

    logger.info(f"Found {len(files)} files in {input_folder}. Files per split: {files_per_split}")
    if not test_mode:
        for i, split in enumerate(splits):
            start_index = i * files_per_split
            end_index = start_index + files_per_split
            if i == num_splits - 1:
                end_index = len(files)
            batch_files = files[start_index: end_index]
            split_folder = input_folder / split
            if not split_folder.is_dir():
                split_folder.mkdir()
            for file in tqdm(batch_files, desc=f"Copying files to {split}"):
                file.rename(split_folder / file.name)

if __name__ == "__main__":
    main()
