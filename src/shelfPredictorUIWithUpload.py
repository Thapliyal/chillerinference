import streamlit as st
import requests
import json
import base64
import io
from PIL import Image, ImageDraw
import numpy as np
from pathlib import Path
import cv2
import pandas as pd
from pydantic import BaseModel
import re
# from frameSelector.classificationInference import ClassificationPredictor
from shelfPredictor import ShelfPredictor, PredictedShelf
from commonUI import ItemNavigator
from loguru import logger
from shelfSettings import StoreManager, WESTERN_AFTER_MAY_15

STORE_MAPPER_WITHOUT_EXCEPTIONS = StoreManager(raise_exception=False)

# st.set_page_config(page_title="FrameSelection", layout="wide")
st.set_page_config(page_title="ShelfPredictorWithUpload", layout="centered")

@st.cache_resource
def get_model():
    curr_dir = Path(__file__).parent.parent
    model_path = curr_dir / "models" / "shelf_model" / "model" / "shelf_model.pth"
    model = ShelfPredictor(model_path, clip_index=5, threshold=0.5)
    return model

@st.cache_resource
def get_logger(app_name):
    BASE_DIR = Path(__file__).parent.parent
    LOG_DIR = BASE_DIR / "logs"
    log_file = LOG_DIR / f"{app_name}.log"
    logger.add(
        log_file,
        format="{time}|{file}|{line}|{thread}|{level}|{message}",
        colorize=True,
    )
    return logger


class ShelfPredictorImageApp:
    def __init__(self, upload_dir):
        self.predictor = get_model()
        self.widget = ItemNavigator(upload_dir, display_image_callback=self.process_image, uploadable=True)

    def process_image(self, input_image, item):
        placeholder = st.empty()
        st.write(f"{item.name}")
        shelf_setting = STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(item)
        if shelf_setting is None:
            shelf_setting = WESTERN_AFTER_MAY_15
        shelves, output_image = self.predictor.predict(input_image, shelf_heights=shelf_setting.shelf_heights, shelf_widths=shelf_setting.shelf_widths)
        
        shelf: PredictedShelf
        for shelf in shelves:
            draw = ImageDraw.Draw(output_image)
            corners = [(x, y) for x, y in shelf.corners]
            draw.polygon(corners, outline=(255, 0, 0), width=4)
            angle1, angle2 = shelf.get_shelf_angles()
            w1, w2 = shelf.get_shelf_widths()
            st.write(f"Shelf: {shelf.label}, Angles: [{angle1:0.2f}:{angle2:0.2f}] Widths: {w1:0.2f}:{w2:0.2f}")
        st.image(output_image, use_column_width=True)
        st.markdown("---")


    def start_app(self):
        self.widget.displayUI()


app_name = "shelfPredictorWithUpload"

BASE_DIR = Path(__file__).parent.parent
LOG_DIR = BASE_DIR / "logs"
DATA_DIR = BASE_DIR / "data"
UPLOAD_DIR = DATA_DIR / f"{app_name}"
logger = get_logger(app_name)
logger.debug(f"UPLOAD_DIR: {UPLOAD_DIR}")
for dir in [DATA_DIR, LOG_DIR, UPLOAD_DIR]:
    if not dir.exists():
        dir.mkdir()

app = ShelfPredictorImageApp(UPLOAD_DIR)
app.start_app()
