#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
from pathlib import Path
import cv2
from utils import decode_image, get_contour_from_mask, ProdigyLoader
from shelfSettings import StoreManager
from lensCorrector import LensCorrector
from shelfPredictor import PredictedShelf, PredictedBottle, PredictedChiller, ChillerComparison
from segmentationInference import BBox
from tqdm.auto import tqdm
from types import GeneratorType
from loguru import logger
import configparser
from chillerPredictor import PerShelfChillerPredictor
import sys
from pydantic import BaseModel
from typing import List
import pickle
import gc
from argparse import ArgumentParser
from collections import Counter
import re
import pandas as pd
from datetime import datetime
from utils import Timer, update_dict_paths, get_sub_image_from_points, get_number_of_lines
from concurrent.futures import ProcessPoolExecutor, as_completed

STORE_MAPPER_WITHOUT_EXCEPTIONS = StoreManager(raise_exception=False)

def parse_args():
    parser = ArgumentParser()
    parser.add_argument('-i', '--input-dir', help='Input dir with shelf and bottle jsonl(s)', required=False, default=None)
    parser.add_argument('-o', '--output-dir', help='Output dir to store the accuracy, if not provided will be stored in model dir', required=False, default=None)
    parser.add_argument('-t', '--trunc-index', help='Truncation index', required=False, type=int, default=None)
    # parser.add_argument('-r', '--recreate', help='Recreate the database file if it exists', required=False, action='store_true', default=False)
    return parser.parse_args()


def get_predictor(fixed_width_transform=True):
    cfg = configparser.ConfigParser()
    cfg.read('inference.ini')
    shelf_model_path = Path(cfg['DEFAULT']['shelf_model_path'])
    bottle_model_path = Path(cfg['DEFAULT']['bottle_model_path'])
    brand_models_dir = Path(cfg['DEFAULT']['brand_models_dir'])
    volume_model_path = Path(cfg['DEFAULT']['volume_model_path'])
    bottle_model_threshold = float(cfg['DEFAULT']['bottle_model_threshold'])
    
    predictor = PerShelfChillerPredictor(shelf_model_path=shelf_model_path, bottle_model_path=bottle_model_path, 
                                            brand_models_dir=brand_models_dir, volume_model_path=volume_model_path, 
                                            shelf_clip_index=5, bottle_model_threshold=bottle_model_threshold, 
                                            fixed_width_transform=fixed_width_transform, debug=False)        
    return predictor


class Prodigy2Chiller:
    def __init__(self, undistort=True):
        # self.shelf_dicts = shelf_dicts
        self.STORE_MAPPING = StoreManager(raise_exception=False)
        self.lens_corrector = LensCorrector()
        # self.dataset_dir_replace_sources = ['/home/akshay/datasets/accuracy', '/home/akshay/datasets/200DLens/distorted']
        self.dataset_dir_replace_sources = []
        if Path('/home/akshay/datasets').exists():
            self.dataset_dir_replace_dest = '/home/akshay/datasets/accuracy'
        else:
            self.dataset_dir_replace_dest = '/home/akshay/workspace/labelling/coke/datasets/200DLens/distorted'
        logger.info(f"dataset_dir_replace_dest: {self.dataset_dir_replace_dest}")
        self.MIN_BOTTLE_HEIGHT = 10
        self.MIN_BOTTLE_WIDTH = 5
        self.undistort = undistort

    def get_path(self, image_path):
        for src in self.dataset_dir_replace_sources:
            if src in image_path:
                new_path = image_path.replace(src, self.dataset_dir_replace_dest)
                logger.info(f"old path: {image_path} --------> new path: {new_path}")
                return new_path
        return image_path

    # def _get_shelf_dict(self, image_path):
    #     for d in self.shelf_dicts:
    #         if d["path"] == image_path:
    #             return d
    #     return None

    def _get_shelf_params(self, d, heights, widths):
        t = Timer("Prodigy2Chiller._get_shelf_params")
        t.start()
        image_path = d["path"]
        logger.info(f"heights: {len(heights)} widths: {len(widths)} spans:{len(d['spans'])}")
        if len(widths) != len(d["spans"]):
            for span in d["spans"]:
                logger.info(f"span: {span}")
        assert len(heights) == len(widths)
        # image_path = image_path.replace(self.dataset_dir_replace_src, self.dataset_dir_replace_dest)
        image_path = self.get_path(image_path)
        if Path(image_path).exists() is False:
            raise Exception(f"Image not found: {image_path}")
        image = cv2.imread(image_path)
        # image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        shelf_settings = self.STORE_MAPPING.get_shelf_settings(image_path)
        if self.undistort:
            mapped_image, i, roi = self.lens_corrector.correct(image)
        else:
            mapped_image = image

        # height = 350
        # width = 800
        shelves = []
        for i, s in enumerate(d["spans"]):
            if i > len(heights) - 1 or i > len(widths) - 1:
                # GT has this info, but prediction doesn't have this shelf
                height = 350
                width = 800
                logger.warning(f"{Path(image_path).name}: Prediction missing shelf: shelves index: {i} > heights ind: {len(heights) - 1} or  widths ind: {len(widths) - 1}")
            else:
                height = heights[i]
                width = widths[i]
            polygon = s['points']
            label = s['label']
            polygon = [[int(p[0]), int(p[1])] for p in polygon]
            if self.undistort:
                mapped_polygon = self.lens_corrector.correct_points(polygon)
            else:
                mapped_polygon = polygon
            p = PredictedShelf(mapped_image, mapped_polygon, label, height=height, width=width,
                               shelf_height=shelf_settings.shelf_heights[0], shelf_width=shelf_settings.shelf_widths[0])
            shelves.append(p)
        if len(shelves) == 0:
            logger.error(f"{Path(image_path).name}: No shelves found")
            return None, None, image_path
        shelves = sorted(shelves, key=lambda x: x.min_y)
        combined = np.concatenate([s.transformed_image for s in shelves])
        t.stop()
        return shelves, combined, image_path

    def _get_polygon(self, span):
        if 'mask' not in span:
            return None
        mask = decode_image(span['mask'])
        if len(mask.shape) == 3:
            mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
        contour = get_contour_from_mask(mask, convex=False)
        if contour is None:
            return None
        if self.undistort:
            # this is undistortion for bottle
            contour = self.lens_corrector.correct_points(contour)
        contour = np.array(contour)
        return contour

    def _is_inside(self, points, shelf):
        corners = shelf.corners
        c_x = [c[0] for c in corners]
        c_y = [c[1] for c in corners]
        c_min_x = min(c_x)
        c_max_x = max(c_x)
        c_min_y = min(c_y)
        c_max_y = max(c_y)

        s_height = c_max_y - c_min_y
        margin = s_height * 0.01

        x = points[:, 0]
        y = points[:, 1]
        x_min, x_max, y_min, y_max = x.min(), x.max(), y.min(), y.max()
        x_mid = (x_min + x_max) / 2
        y_mid = (y_min + y_max) / 2
        status = (c_min_x < x_mid < c_max_x) and (c_min_y - margin < y_mid < c_max_y + margin)
        return status

    def _get_mask_from_contour(self, contour, mapped_image):
        mask = np.zeros_like(mapped_image).astype(np.uint8)
        mask = cv2.drawContours(mask, [contour], -1, (255, 255, 255), thickness=cv2.FILLED)
        return mask

    def _get_bottle(self, contour, label_code, mapped_image):
        x_min = contour[:, :, 0].min()
        x_max = contour[:, :, 0].max()
        y_min = contour[:, :, 1].min()
        y_max = contour[:, :, 1].max()

        if y_max - y_min < self.MIN_BOTTLE_HEIGHT or x_max - x_min < self.MIN_BOTTLE_WIDTH:
            logger.warning(f"Bottle[{label_code}] is too small: {y_max - y_min}x{x_max - x_min} ... skipping")
            return None
        
        bbox = BBox(x1=x_min, y1=y_min, x2=x_max, y2=y_max)
        mask = self._get_mask_from_contour(contour, mapped_image)

        mask = cv2.cvtColor(mask, cv2.COLOR_RGB2GRAY)
        mask = mask > 0
        polygon = contour.reshape(-1, 2).tolist()
        b = PredictedBottle(bbox=bbox, pred_score=1, pred_class=-1, polygon=polygon, mask=mask, label='temp', code=label_code, brand=None, volume=None)
        b.update_from_code()
        return b

    def _find_bottle_contours_for_shelf(self, b_spans, shelf, image_path):
        b_contours = []
        b_labels = []
        remaining_b_spans = []
        for i, b_span in enumerate(b_spans):
            contour = self._get_polygon(b_span)
            if contour is None:
                logger.error(f"{Path(image_path).name}: {shelf.label}: Contour[{i}] not found for: {b_span}")
                continue
            if self._is_inside(contour, shelf):
                b_contours.append(contour)
                b_labels.append(b_span["label"])
            else:
                remaining_b_spans.append(b_span)
        return b_contours, b_labels, remaining_b_spans

    def _warp_points(self, b_contours, shelf):
        new_contours = []
        for b_contour in b_contours:
            new_points = cv2.perspectiveTransform(b_contour.reshape(-1, 1, 2).astype(np.float64), shelf.M)
            new_points = new_points.reshape((-1, 1, 2)).astype(np.int32)
            new_contours.append(new_points)
        return new_contours

    def _update_shelf(self, idx, shelf, b_spans, image_path):
        b_contours, b_labels, remaining_b_spans = self._find_bottle_contours_for_shelf(b_spans, shelf, image_path)
        bgr_image = shelf.transformed_image.copy()

        # this is perspective transform for shelf
        b_contours = self._warp_points(b_contours, shelf)
        b_contours = [b_contour.reshape(-1, 1, 2).astype(np.int32) for b_contour in b_contours]
        
        mapped_image = bgr_image

        bottles = []
        for b_i, (b_contour, b_label) in enumerate(zip(b_contours, b_labels)):
            b: PredictedBottle = self._get_bottle(b_contour, b_label, mapped_image)
            if b is None:
                logger.warning(f"{Path(image_path).name}: {shelf.label}: Bottle[{b_label}] is None ... skipping")
                continue
            if not b.is_valid_polygon(idx, b_label, mapped_image):
                continue
            bottles.append(b)
        #     im = cv2.polylines(image, [b_contour], isClosed=True, color=(255, 0, 0), thickness=1)

        shelf.bottles = sorted(bottles, key=lambda b: b.bbox.x1)
        shelf.bottle_image = shelf.draw_bottles()

        bottle_string = ", ".join([b.code for b in shelf.bottles])
        logger.info(f"GT[{shelf.label}]: [{bottle_string}]")

        return shelf, remaining_b_spans

    def get_shelves(self, bottle_d, shelf_d, heights, widths):
        if "spans" not in shelf_d:
            logger.error(f"Spans not found for shelf_d: {shelf_d['path']}")
            return None, None, None
        
        # undistort is being done inside _get_shelf_params
        shelves, combined, image_path = self._get_shelf_params(shelf_d, heights, widths)
        if shelves is None or len(shelves) == 0:
            logger.error(f"{Path(image_path).name}: No shelves found")
            return None, None, None

        if "spans" not in bottle_d:
            logger.error(f"Spans not found for bottle_d: {image_path}")
            return None, None, None

        b_spans = bottle_d["spans"]
        b_spans = sorted(b_spans, key=lambda x: np.array(x["points"])[:, 1].mean())

        image = cv2.imread(image_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        if self.undistort:
            image, _, _ = self.lens_corrector.correct(image)
        shelf_output_image = image.copy()

        updated_shelves = []
        # for idx, shelf in enumerate(shelves):
        #     shelf, b_spans = self._update_shelf(idx, shelf, b_spans, image_path)
        #     updated_shelves.append(shelf)
        
        
        with ProcessPoolExecutor() as executor:
            # Submit tasks using the process pool
            futures = {executor.submit(self._update_shelf, idx, shelf, b_spans, image_path): idx
                       for idx, shelf in enumerate(shelves)}

            for future in as_completed(futures):
                idx = futures[future]
                shelf_updated, _ = future.result()
                corners = np.array(shelf_updated.corners)
                min_y = corners[:, 1].min()
                max_y = corners[:, 1].max()
                min_x = corners[:, 0].min()
                max_x = corners[:, 0].max()
                if min_y < 0 or min_x < 0 or max_y > image.shape[0] or max_x > image.shape[1]:
                    logger.warning(f"shelf[{shelf_updated.label}] is out of bounds: x[{min_x}:{max_x}] y[{min_y}: {max_y}] image xy: {image.shape[1]}x{image.shape[0]} .. skipping")
                    continue
                updated_shelves.append(shelf_updated)

        updated_shelves = sorted(updated_shelves, key=lambda x: x.min_y)

        for shelf_i, shelf in enumerate(updated_shelves):
            corners = np.array(shelf.corners)
            logger.debug(f"shelf[{shelf_i}] corners: {corners} image: {image.shape}")
            cv2.polylines(shelf_output_image, [corners], isClosed=True, color=(0, 255, 0), thickness=2)

        return updated_shelves, image_path, shelf_output_image

    def get_chiller(self, bottle_d, shelf_d, heights, widths):
        t = Timer("Prodigy2Chiller.get_chiller")
        t.start()
        logger.info(f"heights: {heights} widths: {widths}")
        shelves, image_path, shelf_output_image = self.get_shelves(bottle_d, shelf_d, heights, widths)
        if shelves is None:
            return None
        chiller = PredictedChiller(shelves=shelves, shelf_output_image=shelf_output_image, image_path=image_path)
        t.stop()
        return chiller
    
    def get_gt_chiller(self, bottle_d, shelf_d, path_prefix=None, path_prefix_replacement=None):
        bottle_d = update_dict_paths(bottle_d, path_prefix, path_prefix_replacement)
        shelf_d = update_dict_paths(shelf_d, path_prefix, path_prefix_replacement)

        image_path = bottle_d["path"]
        shelf_settings = STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(image_path)
        if shelf_settings is None:
            logger.error(f"Shelf settings not found for: {image_path}")
            return None
        heights = [350 for _ in shelf_settings.shelf_heights]
        widths = [800 for _ in shelf_settings.shelf_widths]
        return self.get_chiller(bottle_d, shelf_d, heights, widths)
    
    def _skip_file(self, path, skip_files):
        for skip_file in skip_files:
            if Path(path).stem.lower() in skip_file.lower():
                return True
        return False
    
    def yield_gt_chillers(self, bottle_jsonls, shelf_jsonls, search_path=None, start_i=None, replace_mapping=None, skip_files=None):
        loader = ProdigyLoader(replace_mapping=replace_mapping)
        num_lines = get_number_of_lines(bottle_jsonls)
        for bottle_i, bottle_d in enumerate(loader.yield_dicts(bottle_jsonls)):
            if bottle_d is None:
                logger.error(f"bottle_d is None for bottle_i: {bottle_i}")
                continue
            if bottle_i % 100 == 0:
                gc.collect()
            path = str(bottle_d["path"])
            if skip_files is not None:
                if self._skip_file(path, skip_files):
                    logger.debug(f"Skipping bottle json[{bottle_i}]: {path} because it is in skip_files: {skip_files}")
                    yield None
                    continue
                # else:
                #     logger.error(f"Processing bottle json[{bottle_i}]: {Path(path).stem.lower()} because it is not in skip_files:")
                #     for i, skip_file in enumerate(skip_files[:10]):
                #         logger.error(f"skip_files[{i}]: {skip_file.lower()}")
                #     import sys
                #     sys.exit(1)
            if start_i is not None:
                if bottle_i < start_i:
                    logger.debug(f"Skipping bottle json[{bottle_i}]: {path} before start_i: {start_i}")
                    yield None
                    continue
            if search_path is not None:
                if search_path.lower() not in str(path).lower():
                    logger.debug(f"Skipping bottle json[{bottle_i}]: {path} does not match search_path: {search_path}")
                    yield None
                    continue
                # else:
                #     logger.info(f"Processing bottle json[{bottle_i}]: {path} matches search_path: {search_path}")
            
            logger.info(f"------------- Processing GT_Chiller[{bottle_i}/{num_lines}] -------------")
            logger.info(f"path: {path}")
            shelf_d = loader.find_matching_shelf_dict(shelf_jsonls, path)
            if shelf_d is None:
                logger.error(f"{path}: No shelves found")
                yield None
                continue
            yield self.get_gt_chiller(bottle_d, shelf_d)


class AccuracyChecker:
    def __init__(self, replace_mapping=None):
        self.loader = ProdigyLoader(replace_mapping=replace_mapping)
        self.predictor = get_predictor()        
        self.STORE_MAPPER_WITHOUT_EXCEPTIONS = STORE_MAPPER_WITHOUT_EXCEPTIONS

    def _is_in_filterlist(self, path, filter_list):
        for f in filter_list:
            if f.lower() in path.lower():
                return True
        return False
    
    def get_region_filtered_bottle_dicts(self, bottle_dicts, region_filter):
        if region_filter is None:
            return bottle_dicts
        new_bottle_dicts = []
        before = len(bottle_dicts)
        for bottle_d in bottle_dicts:
            image_path = bottle_d["path"]
            shelf_settings = self.STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(image_path)
            if shelf_settings is None:
                logger.error(f"Shelf settings not found for: {image_path}")
                continue
            if shelf_settings.region in region_filter:
                new_bottle_dicts.append(bottle_d)
        after = len(new_bottle_dicts)
        logger.info(f"Filtering based on region filter: {region_filter} Before: {before} After: {after}")
        return new_bottle_dicts
    
    def get_dicts_from_jsonls(self, bottle_files, shelf_files, region_filter=None):
        shelf_files = self._get_files(shelf_files)
        bottle_files = self._get_files(bottle_files)
        
        shelf_dicts, _ = self.loader.load_files(shelf_files)
        bottle_dicts, _ = self.loader.load_files(bottle_files)

        logger.info(f"Found [{len(shelf_dicts)}] shelf_dicts and [{len(bottle_dicts)}] bottle_dicts")

        # processor = Prodigy2Chiller(shelf_dicts)

        ## TODO: remove dicts which do not have both shelf and bottle dicts
        if region_filter is not None:
            if not isinstance(region_filter, list):
                region_filter = [region_filter]
            bottle_dicts = self.get_region_filtered_bottle_dicts(bottle_dicts, region_filter)
        return bottle_dicts, shelf_dicts

    def _update_dict(self, d, path_prefix, path_prefix_replacement):
        return update_dict_paths(d, path_prefix, path_prefix_replacement)
    
    def yield_chillers(self, bottle_jsonls, shelf_jsonls, filter_list=None, disable_logger=True, 
                       search=None, region_filter=None, path_prefix=None, path_prefix_replacement=None,
                       skip_list=None):
        processor = Prodigy2Chiller()
        num_lines = get_number_of_lines(bottle_jsonls)
        for bottle_i, bottle_d in enumerate(self.loader.yield_dicts(bottle_jsonls)):
            logger.info(f"*************** Chiller[{bottle_i}/{num_lines}] ***************")
            # logger.info(f"skip list: {skip_list}")
            t = Timer(f"AccuracyChecker.yield_chillers[{bottle_i}]")
            t.start()
            path = bottle_d["path"]
            shelf_d = self.loader.find_matching_shelf_dict(shelf_jsonls, path)
            if shelf_d is None:
                logger.error(f"{path}: No shelves found")
                logger.error(f"shelf_jsonls: {shelf_jsonls}")
                continue
            bottle_d = self._update_dict(bottle_d, path_prefix=path_prefix, path_prefix_replacement=path_prefix_replacement)
            shelf_d = self._update_dict(shelf_d, path_prefix=path_prefix, path_prefix_replacement=path_prefix_replacement)

            path = bottle_d["path"]
            if not Path(path).is_file():
                raise Exception(f"Unable to open file: {path}")

            if skip_list and str(path) in skip_list:
                logger.warning(f"Skipping bottle[{bottle_i}]: already processed image: {path}")
                continue
            # logger.info(f"path: {path} not in skip list: {skip_list}")

            shelf_settings = self.STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(path)
            if shelf_settings is None:
                logger.error(f"Shelf settings not found for: {path}")
                continue

            logger.info(f"Processing chiller[{bottle_i}/{num_lines}]: {path}")

            if disable_logger:
                logger.remove()
                logger.add(sys.stdout, level="ERROR")

            chiller = self.predictor.predict(path, shelf_heights=shelf_settings.shelf_heights, shelf_widths=shelf_settings.shelf_widths)
            heights = [shelf.new_height for shelf in chiller.shelves]
            widths = [shelf.new_width for shelf in chiller.shelves]
            chiller_gt = processor.get_chiller(bottle_d, shelf_d, heights, widths)

            if disable_logger:
                logger.remove()
                logger.add(sys.stdout, level="DEBUG")

            t.stop()
            yield chiller_gt, chiller

    # def yield_chillers_from_dicts(self, bottle_dicts, shelf_dicts, filter_list=None, disable_logger=True, search=None):
    #     processor = Prodigy2Chiller(shelf_dicts)

    #     if disable_logger:
    #         logger.remove()
    #         logger.add(sys.stdout, level="ERROR")

    #     for bottle_d in bottle_dicts:
    #         image_path = bottle_d["path"]
    #         image_path = processor.get_path(image_path)
    #         if search is not None:
    #             if search.lower() not in image_path.lower():
    #                 continue
    #         if filter_list is not None:
    #             if not self._is_in_filterlist(image_path, filter_list):
    #                 continue
    #         shelf_mapping = self.STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(image_path)
    #         if shelf_mapping is None:
    #             logger.error(f"Shelf mapping not found for: {image_path}")
    #             continue
    #         chiller = self.predictor.predict(image_path, shelf_heights=shelf_mapping.shelf_heights, shelf_widths=shelf_mapping.shelf_widths)
    #         heights = [shelf.new_height for shelf in chiller.shelves]
    #         widths = [shelf.new_width for shelf in chiller.shelves]
    #         chiller_gt = processor.get_chiller(bottle_d, heights, widths)
    #         # image_path = chiller_gt.image_path
    #         yield (chiller_gt, chiller)

    #     if disable_logger:
    #         logger.remove()
    #         logger.add(sys.stdout, level="DEBUG")
    
    def _get_files(self, input_fh):
        if isinstance(input_fh, GeneratorType):
            return sorted(list(input_fh))
        if isinstance(input_fh, str):
            input_fh = Path(input_fh)
        
        if isinstance(input_fh, Path):
            if input_fh.is_file():
                return [input_fh]
            elif input_fh.is_dir():
                files = sorted(list(input_fh.glob('*.jsonl')))
                return files
        raise Exception(f"Invalid input type: {input_fh}")
    

class ChillerComparisonIterator:
    def __init__(self, anno_dir, region_filter):
        self.anno_dir = anno_dir
        self.region_filter = region_filter
        self.accuracy_checker = AccuracyChecker()
        self.bottle_files = anno_dir.glob("*bottle*.jsonl")
        self.shelf_files = anno_dir.glob("*shelf*.jsonl")

        bottle_dicts, shelf_dicts = self.accuracy_checker.get_dicts_from_jsonls(self.bottle_files, self.shelf_files, region_filter=self.region_filter)
        self.length = len(bottle_dicts)

    def __len__(self):
        return self.length

    def __iter__(self):
        return self

    def __next__(self):
        raise StopIteration
    
def generate_gt_and_preds(bottle_jsonls, shelf_jsonls, pickle_file, region=None, 
                 path_prefix=None, path_prefix_replacement=None, trunc_index=None):
    if region is None:
        region = None
    else:
        if not isinstance(region, list):
            region = [region]

    # Load already processed image paths
    processed_paths = set()
    if pickle_file.exists():
        logger.info(f"Loading already processed images from: {pickle_file}")
        with open(pickle_file, 'rb') as file:
            while True:
                try:
                    result = pickle.load(file)
                    path = str(result['image_path'])
                    if path_prefix is not None and path_prefix_replacement is not None:
                        path = path.replace(path_prefix, path_prefix_replacement)
                    processed_paths.add(path)
                except EOFError:
                    break
        logger.info(f"Loaded {len(processed_paths)} already processed images from: {pickle_file}")

    tot = 0
    count = 0
    i = len(processed_paths)  # Start counter from number of processed images

    a = AccuracyChecker()
    with open(pickle_file, 'ab') as file:
        for chiller_i, (chiller_gt, chiller_pred) in enumerate(a.yield_chillers(bottle_jsonls, shelf_jsonls, region_filter=region, 
                                                         path_prefix=path_prefix, path_prefix_replacement=path_prefix_replacement, 
                                                         skip_list=processed_paths, disable_logger=False)):
            if chiller_gt is None or chiller_pred is None:
                logger.error(f"chiller_gt or chiller_pred is None ... skipping")
                gc.collect()
                continue

            # if chiller_i < i:
            #     logger.error(f"Skipping chiller_i: {chiller_i} already processed image: {chiller_gt.image_path}")
            #     gc.collect()
            #     break

            comparison: ChillerComparison = chiller_gt.compare(chiller_pred)
            comparison_type = 'coke_only'
            # comparison_type = 'catalog_only'
            shelf_clip_index = 3
            acc, _correct, _total, _mappings = comparison.get_accuracy(comparison_type, shelf_clip_index=shelf_clip_index)
            result = {}
            result['image_path'] = chiller_gt.image_path
            result['acc'] = acc
            result['comparison'] = comparison
            # results.append(result)
            
            tot += acc 
            count += 1
            logger.opt(colors=True).info(f"\n<green>[{i}]: {Path(chiller_gt.image_path).name}: {acc:4.2f} %</green>")
            pickle.dump(result, file)
            file.flush()
            logger.opt(colors=True).info(f"<yellow>==============================================</yellow>")
            i+=1
            gc.collect()

            if trunc_index and i >= trunc_index:
                logger.info(f"Truncating at index: {trunc_index}")
                break

def get_acc_dict_from_result(store_man: StoreManager, result, region, correct, totals):
    image_path = result['image_path']
    shelf_settings = store_man.get_shelf_settings(image_path)
    if region and shelf_settings.region != region:
        return None, None
    comparison: ChillerComparison = result['comparison']
    for shelf in comparison.shelves:
        for bottle in shelf.all:
            bottle.gt.update_from_code()    
    shelf_clip_index = 3
    comparison_type = 'coke_only'
    acc, _correct, _total, _mappings = comparison.get_accuracy(comparison_type, shelf_clip_index=shelf_clip_index)
    correct, totals = comparison.get_counts_per_brand(comparison_type=comparison_type, shelf_clip_index=shelf_clip_index, 
                                                      correct_counts=correct, total_counts=totals)
    asset_name = shelf_settings.asset_name
    asset_region = shelf_settings.region
    return acc, asset_name, asset_region, image_path, correct, totals

def get_acc_dicts_from_results(results, region):
    store_man = StoreManager(raise_exception=False)
    total_value = 0
    num_items = 0
    acc_dict = {}
    acc_path_dict = {}
    correct = Counter()
    totals = Counter()
    for r_i, result in enumerate(results):
        acc, asset_name, asset_region, image_path, correct, totals = get_acc_dict_from_result(store_man, result, region, correct=correct, totals=totals)
        if asset_name is None:
            logger.error(f"asset_name is None for result[{r_i}]: {image_path} ... skipping")
            continue
        if asset_name not in acc_dict:
            acc_dict[asset_name] = []
        if asset_name not in acc_path_dict:
            acc_path_dict[asset_name] = []
        acc_dict[asset_name].append(acc)
        acc_path_dict[asset_name].append(image_path)
        total_value += acc
        num_items += 1
    return acc_dict, acc_path_dict


def load_results(pickle_file):
    store_man = StoreManager(raise_exception=False, silent=True)
    total_value = 0
    num_items = 0
    acc_dict = {}
    acc_path_dict = {}
    correct = Counter()
    totals = Counter()
    asset_name_2_region = {}
    region = None

    with open(pickle_file, 'rb') as file:
        while True:
            try:
                result = pickle.load(file)
                acc, asset_name, asset_region, image_path, correct, totals = get_acc_dict_from_result(store_man, result, region, correct=correct, totals=totals)
                if asset_name is None:
                    continue
                if asset_name not in acc_dict:
                    acc_dict[asset_name] = []
                if asset_name not in acc_path_dict:
                    acc_path_dict[asset_name] = []
                if asset_name not in asset_name_2_region:
                    asset_name_2_region[asset_name] = asset_region
                acc_dict[asset_name].append(acc)
                acc_path_dict[asset_name].append(image_path)
                total_value += acc
                num_items += 1

                if num_items % 100 == 0:
                    gc.collect()
            except EOFError:
                break  # Stop when end of file is reached

    gc.collect()
    logger.info(f"Loaded {num_items} results from: {pickle_file}")
    logger.info(f"Correct: {correct}")
    logger.info(f"Totals: {totals}")
    return acc_dict, acc_path_dict, asset_name_2_region, correct, totals

def get_accuracy_frames(acc_dict, acc_path_dict, asset_name_2_region, correct, totals, trunc_index):
    AV_THRESHOLD = 100
    SKIP = []
    total = 0
    num_items = 0

    data_frame = pd.DataFrame(columns=['asset_name', 'region', 'paths', 'values'])
    for k in sorted(acc_dict.keys(), key=lambda x: int(re.split('[A-Z]+', x)[1])):
        if k in SKIP:
            continue
        acc_list = acc_dict[k]
        # acc_list = [a for a in acc_list if a > 0]
        region = asset_name_2_region[k]
        values = sorted(acc_list, reverse=True)
        paths = acc_path_dict[k][:len(acc_list)]
        acc_unsorted = acc_list
        paths = sorted(paths, key=lambda x: acc_unsorted[paths.index(x)], reverse=True)
        # print(values, paths)
        # break
        if trunc_index:
            local_sum = sum(values[:trunc_index])
            local_num_items = len(values[:trunc_index])
        else:
            local_sum = sum(values)
            local_num_items = len(values)
        
        data_frame.loc[len(data_frame)] = [k, region, paths, values]
        
        local_av = local_sum / local_num_items
        total += local_sum
        num_items += local_num_items
        local_av = local_sum / local_num_items
        values_str = ", ".join([f"{v:06.02f}" for v in values[:trunc_index]])
        paths_str = ", ".join([f"{Path(p).name}" for p in paths[:trunc_index]])
        # print(f"{k:4s}", f"[{values_str}]: {local_av:06.2f}")
        if local_av < AV_THRESHOLD:
            print(f"{k:4s}: [{values_str}]: {local_av: 06.2f}%")

    sku_types, brands, corrects_list, totals_list = [], [], [], []
    for k in totals.keys():
        c = correct[k]
        if "-" in k:
            splits = k.split("-")
            sku_type = splits[0]
            brand = splits[1]
        else:
            sku_type = brand = k
        sku_types.append(sku_type)
        brands.append(brand)
        corrects_list.append(c)
        totals_list.append(totals[k])

    frame = pd.DataFrame(columns=['sku_type', 'brand', 'correct', 'totals'])
    frame['sku_type'] = sku_types
    frame['brand'] = brands
    frame['correct'] = corrects_list
    frame['totals'] = totals_list
    frame['accuracy'] = 100 * frame['correct']/frame['totals']

    brand_frame = frame.sort_values(by=['sku_type', 'accuracy'], ascending=[True, False])
    
    sorted_data_frame = data_frame.sort_values(by=['region', 'asset_name'], ascending=[True, True])
    sorted_brand_frame = brand_frame[['sku_type', 'brand', 'correct', 'totals', 'accuracy']]    
    return sorted_data_frame, sorted_brand_frame

def read_accuracy_frame(frame_path):
    """
    Read and parse the accuracy frame CSV, properly handling list columns.
    
    Args:
        frame_path (Path): Path to the CSV file
        
    Returns:
        pd.DataFrame: Processed DataFrame with properly parsed lists
    """
    # Read the CSV
    df = pd.read_csv(frame_path)
    
    # Convert string representations of lists to actual lists
    def parse_list_string(s):
        try:
            # Remove brackets and split by comma
            return eval(s)  # Using eval since the lists contain float values
        except:
            logger.error(f"Failed to parse list string: {s}")
            return []
    
    # Parse the paths and values columns
    df['paths'] = df['paths'].apply(parse_list_string)
    df['values'] = df['values'].apply(parse_list_string)
    
    # Validate the parsed data
    invalid_rows = df[df['values'].apply(lambda x: len(x) == 0)].index
    if len(invalid_rows) > 0:
        logger.warning(f"Found {len(invalid_rows)} rows with empty values lists")
    
    logger.info(f"Loaded DataFrame with {len(df)} rows from {frame_path}")
    return df

def calculate_or_load_accuracies(pickle_file, frame_path, brand_frame_path, region=None, trunc_index=None):
    total = 0
    num_items = 0
    num_stores = 0
    trunc_index = None
    if not (frame_path.exists() and brand_frame_path.exists()):
        acc_dict, acc_path_dict, asset_name_2_region, correct, totals = load_results(pickle_file)
        df, brand_df = get_accuracy_frames(acc_dict, acc_path_dict, asset_name_2_region, correct, totals, trunc_index=trunc_index)
        logger.info(f"Saved {(len(df))} records to: {frame_path}")
        df.to_csv(frame_path, index=False)
        logger.info(f"Saved {(len(brand_df))} records to: {brand_frame_path}")
        brand_df.to_csv(brand_frame_path, index=False)
        num_items = sum(len(values) for values in df['values'])
    else:
        df = read_accuracy_frame(frame_path)
        brand_df = pd.read_csv(brand_frame_path)
        num_items = sum(len(values) for values in df['values'])
        logger.info(f"Loaded {len(df)} records with {num_items} total accuracy values from: {frame_path}")
        logger.info(f"Loaded {len(brand_df)} records with {num_items} brand accuracy values from: {brand_frame_path}")
    
    return df, brand_df

def print_stats(average_path, df, brand_df):
    # iterate over brand_df and log the accuracy for each brand and sku_type
    prev_sku_type = None
    SEP = "-"*80

    if brand_df is None or len(brand_df) == 0:
        logger.error(f"brand accuracy frame is None or empty")
    else:
        for index, row in brand_df.iterrows():
            sku_type = row['sku_type']
            if sku_type != prev_sku_type:
                logger.opt(colors=True).info(f"<green>{SEP}</green>")
            logger.opt(colors=True).info(f"<green>{row['sku_type']:5s}[{row['brand']:12s}]: {row['accuracy']:04.2f}%: {row['correct']}/{row['totals']}</green>")
            prev_sku_type = sku_type

        if prev_sku_type is not None:
            logger.opt(colors=True).info(f"<green>{SEP}</green>")
    
    sku_type_avg = brand_df.groupby('sku_type')['accuracy'].mean()
    for sku_type, avg in sku_type_avg.items():
        logger.opt(colors=True).info(f"<green>{sku_type:5s}: {avg:04.2f}%</green>")

    # iterate over df and log the accuracy for each asset_name and region
    prev_region = None

    if df is None or len(df) == 0:
        logger.error(f"store accuracy frame is None or empty")
    else:
        for index, row in df.iterrows():
            region = row['region']
            values = row['values']
            avg = sum(values) / len(values)
            if prev_region is not None and region != prev_region:
                logger.opt(colors=True).info(f"<green>{SEP}</green>")
            values_str = ", ".join([f"{v:06.02f}" for v in values])
            logger.opt(colors=True).info(f"<green>{region:12s}[{row['asset_name']:4s}: {avg:04.2f}%]: [{values_str}] </green>")
            prev_region = region
    
        if prev_region is not None:
            logger.opt(colors=True).info(f"<green>{SEP}</green>")

    num_items = sum(len(values) for values in df['values'])

    if num_items == 0:
        logger.warning(f"No items found")
    else:
        
        total = 0
        total_num_items = 0
        with open(average_path, 'w') as file:
            # generate the average for each region in the dataframe using groupby
            df_grouped = df.groupby('region')
            for region, group in df_grouped:
                # Sum all values in the lists for this region
                region_total = sum(sum(values) for values in group['values'])
                total += region_total
                # Count total number of values across all lists
                region_num_items = sum(len(values) for values in group['values'])
                file.write(f"Accuracy for {region}: {region_total/region_num_items: 06.2f}\n")
                logger.opt(colors=True).info(f"<red>Accuracy for {region}: {region_total/region_num_items: 06.2f}% </red>")
                total_num_items += region_num_items

            final_avg = total / total_num_items
            file.write(f"Overall Average Accuracy: {final_avg: 06.2f}")
        logger.opt(colors=True).info(f"<red><i>Overall Average Accuracy: {final_avg: 06.2f}%</i></red>")
        logger.opt(colors=True).info(f"<green>accuracy summary written to: {average_path}</green>")


def main():
    args = parse_args()
    if args.input_dir is None:
        input_dir = Path(__file__).parent.parent / 'data' / 'accuracy' / 'datasets'
        input_dir = Path("/home/akshay/workspace/labelling/coke/datasets/exported/200DLens/November-21-accuracy")
    else:
        input_dir = Path(args.input_dir)
    
    if not input_dir.is_dir():
        raise ValueError(f"Unable to open input_dir: {input_dir}")
    
    if args.output_dir is None:
        output_dir = Path(__file__).parent.parent / 'models' / 'accuracy' / 'output'
        if not output_dir.is_dir():
            output_dir.mkdir(parents=True)
    else:
        output_dir = Path(args.output_dir)
        
    if not output_dir.is_dir():
        output_dir.mkdir()

    trunc_index = args.trunc_index
    
    # input_dir = Path("/home/akshay/workspace/labelling/coke/datasets/exported/200DLens/November-21-accuracy")
    # output_dir = Path('/home/akshay/workspace/computervision/cokeInference/data/accuracyResults')

    path_prefix = '/home/akshay/datasets'
    if Path(path_prefix).is_dir():
        path_prefix = None
        path_prefix_replacement = None
    else:
        path_prefix = '/home/akshay/datasets'
        path_prefix_replacement = '/home/akshay/workspace/labelling/coke/datasets'

    bottle_jsonls = sorted(list(input_dir.glob("*bottle*.jsonl")))
    shelf_jsonls = sorted(list(input_dir.glob("*shelf*.jsonl")))

    if not output_dir.is_dir():
        output_dir.mkdir()

    region = None
    if region is None:
        pickle_file = output_dir / Path(f"{input_dir.name}.pickle")
    else:
        pickle_file = output_dir / Path(f"{input_dir.name}-{region}.pickle")
    logger.info(f"pickle_file: {pickle_file}")

    now = datetime.now()
    formatted_date = now.strftime("%Y-%m-%d")
    frame_path = pickle_file.parent / f'{pickle_file.stem}-accuracyFrame-{formatted_date}.csv'
    brand_frame_path = pickle_file.parent / f'{pickle_file.stem}-brandFrame-{formatted_date}.csv'
    average_path = pickle_file.parent / f'{pickle_file.stem}-averageAccuracy-{formatted_date}.txt'

    if not frame_path.is_file():
        generate_gt_and_preds(bottle_jsonls=bottle_jsonls, shelf_jsonls=shelf_jsonls, pickle_file=pickle_file, 
                 path_prefix=path_prefix, path_prefix_replacement=path_prefix_replacement, 
                 trunc_index=trunc_index)    
    
    df, brand_df = calculate_or_load_accuracies(pickle_file=pickle_file, frame_path=frame_path, brand_frame_path=brand_frame_path)
    print_stats(average_path, df, brand_df)


if __name__ == '__main__':
    main()
