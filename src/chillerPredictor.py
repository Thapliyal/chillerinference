#!/usr/bin/env python3

from pathlib import Path
import configparser
from argparse import ArgumentParser
import json
from loguru import logger
import cv2

from shelfSettings import StoreManager
from classificationInference import VideoClassification, ClassificationPredictor, StatusErrors, VideoDecodeError, UnableToOpenFile, FrameSelector
from shelfPredictor import ShelfPredictor, PredictedShelf, PredictedChiller
from perShelfPredictor import PerShelfChillerPredictor
from analyzeVideos import VideoManager

STORE_MAPPER = StoreManager()
STORE_MAPPER_WITHOUT_EXCEPTIONS = StoreManager(raise_exception=False)

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('-i', '--input-dir', help='Dir with image or video files', required=True)
    parser.add_argument('-o', '--output-dir', help='Directory to save images', required=False, default='/tmp/extractedBottleImages')
    return parser.parse_args()

def get_chiller_predictor(fixed_width_transform=True):
    cfg = configparser.ConfigParser()
    cfg.read('inference.ini')
    shelf_model_path = Path(cfg['DEFAULT']['shelf_model_path'])
    bottle_model_path = Path(cfg['DEFAULT']['bottle_model_path'])
    brand_models_dir = Path(cfg['DEFAULT']['brand_models_dir'])
    volume_model_path = Path(cfg['DEFAULT']['volume_model_path'])
    bottle_model_threshold = float(cfg['DEFAULT']['bottle_model_threshold'])
    
    predictor = PerShelfChillerPredictor(shelf_model_path=shelf_model_path, bottle_model_path=bottle_model_path, 
                                         brand_models_dir=brand_models_dir, volume_model_path=volume_model_path, 
                                         shelf_clip_index=5, bottle_model_threshold=bottle_model_threshold, 
                                         fixed_width_transform=fixed_width_transform, debug=False)
    return predictor

def get_frame_selector():
    frame_selector = FrameSelector()
    return frame_selector

def save_to_json(dicts, output_file):
    with open(output_file, 'w') as f:
        for d in dicts:
            line = json.dumps(d)
            f.write(f"{line}\n")
    logger.info(f"Saved [{len(dicts)}] records to: {output_file}")

def main():
    frame_selector = get_frame_selector()
    video_path = Path("/home/akshay/Downloads/temp/August-12/rajal/C82E18224378_1722232986142_6551.avi")
    result: VideoClassification = frame_selector.select_from_video(video_path)
    predictor = get_chiller_predictor()
    shelf_mapping = STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(video_path)
    image = result.frame_classifications.result_frame
    bgr = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    output_dir = Path('/tmp/extractedBottleImages')
    if not output_dir.exists():
        output_dir.mkdir()
    image_path = output_dir / f"{video_path.stem}-{result.frame_classifications.result_index:02d}.jpg"
    cv2.imwrite(str(image_path), bgr)
    chiller = predictor.predict(image_path, shelf_heights=shelf_mapping.shelf_heights, shelf_widths=shelf_mapping.shelf_widths)
    d = chiller.to_prodigy(shelf_only=False)
    dicts = []
    dicts.append(d)
    save_to_json(dicts, '/tmp/chiller.jsonl')

if __name__ == '__main__':
    main()
