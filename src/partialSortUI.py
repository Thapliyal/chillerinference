from commonUI import ItemNavigator
from pathlib import Path
from loguru import logger
import streamlit as st
from classificationInference import CategoryBasedSorter
from functools import partial
import cv2

####################################################################################################################
# Initializations
####################################################################################################################
@st.cache_resource
def get_logger():
    from loguru import logger

    log_file = LOG_DIR / "partialSortUI.log"
    logger.add(
        log_file,
        format="{time}|{file}|{line}|{thread}|{level}|{message}",
        colorize=True,
    )
    return logger

@st.cache_resource
def get_sorter(model_path: Path):
    return CategoryBasedSorter(model_path)

BASE_DIR = Path(__file__).parent.parent
LOG_DIR = BASE_DIR / "logs"
DATA_DIR = BASE_DIR / "data"
if not DATA_DIR.is_dir():
    DATA_DIR.mkdir()
DATA_DIR = DATA_DIR / "partialSort"
if not DATA_DIR.is_dir():
    DATA_DIR.mkdir()

logger = get_logger()

if "model_path" not in st.session_state:
    st.session_state.model_path = Path('/home/akshay/workspace/computervision/chillerinference/models/frame_selection/frameSelection.pth')

if "input_dir" not in st.session_state:
    st.session_state.input_dir = Path('/home/akshay/datasets/frameSelection/ritwik-unsorted')

if "output_dir" not in st.session_state:
    st.session_state.output_dir = DATA_DIR / st.session_state.input_dir.stem

output_dir = st.session_state.output_dir
if not output_dir.is_dir():
    output_dir.mkdir()
    logger.info(f"Created output directory: {output_dir}")

####################################################################################################################
# User Interface
####################################################################################################################

def update_progress(progress_bar, progress):
    progress_bar.progress(progress)

model_path = Path(st.text_input("Model Path", st.session_state.model_path))
st.session_state.model_path = model_path
input_dir = Path(st.text_input("Input Directory", st.session_state.input_dir))
st.session_state.input_dir = input_dir
st.write(f"Output Directory: {output_dir}")

threshold = st.slider("Threshold", 0.0, 1.0, 0.85, step=0.05)
sort_button = st.button("Sort")
if sort_button:
    logger.info(f"Sorting: {input_dir}")
    progress_bar = st.progress(0)
    sorter = get_sorter(model_path)
    # progress_callback = lambda x: progress_bar.progress(int(x))
    progress_callback = partial(update_progress, progress_bar)
    sorter.sort(input_dir, output_dir, progress_callback=progress_callback, threshold=threshold)
