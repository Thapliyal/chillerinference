import streamlit as st
import requests
import json
import base64
import io
from PIL import Image
import numpy as np
from pathlib import Path
import cv2
import pandas as pd
from utils import decode_image
from pydantic import BaseModel
import re
import os
from shelfSettings import StoreManager, WESTERN_AFTER_MAY_15

STORE_MAPPER_WITHOUT_EXCEPTIONS = StoreManager(raise_exception=False)
IS_IN_DOCKER = os.environ.get("RUNNING_IN_DOCKER", False)

st.set_page_config(page_title="Chiller Inference", layout="wide")

def analyze(image_path, shelf_heights=None, shelf_widths=None, video_dir=None, frame_name=None, evaluate_depth=False):
    with open(image_path, "rb") as file:
        if IS_IN_DOCKER:
            url = "http://analyzechiller:9000/analyzeChiller"
        else:
            url = "http://127.0.0.1:9000/analyzeChiller"
        if shelf_heights is None:
            shelf_heights = [24.7, 23, 32, 37, 40.5]
        if shelf_widths is None:
            shelf_widths = [50, 50, 50, 50, 50]
        files = {"file": file}
        data = {
                "shelf_heights": str(shelf_heights), 
                "shelf_widths": str(shelf_widths), 
                "video_dir": str(video_dir), 
                "frame_name": str(frame_name),
                "evaluate_depth": str(evaluate_depth)
            }
        
        response = requests.post(url, files=files, data=data)
        return response


@st.cache_resource
def get_logger():
    from loguru import logger

    log_file = LOG_DIR / "inferenceUI.log"
    logger.add(
        log_file,
        format="{time}|{file}|{line}|{thread}|{level}|{message}",
        colorize=True,
    )
    return logger

def upload(bytes_data, file_name): 
    global UPLOAD_DIR
    file_path = UPLOAD_DIR / file_name
    with open(file_path, "wb") as file:
        file.write(bytes_data)
        # st.info("")


def get_files():
    global UPLOAD_DIR
    return [f for f in sorted(UPLOAD_DIR.glob("*")) if f.is_file() and f.name[0] != "."]


def delete_files():
    files = get_files()
    deleted = len([f.unlink() for f in files])
    st.info(f"deleted [{deleted}] files")


def on_click_prev():
    if st.session_state.index == 0:
        return
    st.session_state.index -= 1
    logger.debug(f"index decremented to: {st.session_state.index}")


def on_click_next():
    if st.session_state.index == st.session_state.num_files - 1:
        return
    st.session_state.index += 1
    logger.debug(f"index incremented to: {st.session_state.index}")


def on_slider_change():
    st.session_state.index = st.session_state.file_slider
    logger.debug(f"index changed to: {st.session_state.index}")


def on_radio_change():
    st.session_state.cache_enabled = st.session_state.radio_cache_enabled


def change_language():
    pass
    st.session_state.language = st.session_state.lang_radio


def get_curr_path(files):
    if st.session_state.num_files == 0:
        return None
    curr_path = files[st.session_state.index]
    return curr_path

####################################################################################################################
# Initializations
####################################################################################################################
# initialize()
# args = get_args()
# g_debug = args.debug
g_debug = True

BASE_DIR = Path(__file__).parent.parent
LOG_DIR = BASE_DIR / "logs"
DATA_DIR = BASE_DIR / "data"
UPLOAD_DIR = DATA_DIR / "chillerdata"
for dir in [DATA_DIR, LOG_DIR, UPLOAD_DIR]:
    if not dir.exists():
        dir.mkdir()

logger = get_logger()

####################################################################################################################
# User Interface
####################################################################################################################
INDEX = "index"
if INDEX not in st.session_state:
    st.session_state.index = 0

NUM_FILES = "num_files"
if NUM_FILES not in st.session_state:
    st.session_state.num_files = 0

files = get_files()
st.session_state.num_files = len(files)


with st.expander("Upload Images"):
    uploaded_files = st.file_uploader(
        "Choose a jpg file",
        accept_multiple_files=True,
        type=["jpg"],
    )

    for uploaded_file in uploaded_files:
        bytes_data = uploaded_file.read()
        upload(bytes_data, uploaded_file.name)

    names = [f.name for f in files]
    frame = pd.DataFrame.from_dict({"File Name": names})
    st.table(frame)
    reset_button = st.button("DeleteFiles")
    if reset_button:
        delete_files()

col_prev, col_slider, col_next = st.columns([2, 10, 2])
col_prev.button("Prev", on_click=on_click_prev)
if st.session_state.num_files > 1:
    col_slider.slider(
        "Slider",
        min_value=0,
        max_value=st.session_state.num_files - 1,
        value=st.session_state.index,
        on_change=on_slider_change,
        key="file_slider",
    )
col_next.button("Next", on_click=on_click_next)

if len(files) > 0:
    curr_path = get_curr_path(files)
    if Path(curr_path).is_file():
        file_placeholder = st.empty()
        st.markdown("---")
        col1, col2, col3 = st.columns([6, 6, 6])
        input_image = cv2.imread(str(curr_path))
        input_image = cv2.cvtColor(input_image, cv2.COLOR_BGR2RGB)
        logger.debug(f"curr_path: {curr_path}")
        

        placeholder1 = col1.empty()
        placeholder1.image(input_image, use_container_width=True)

        placeholder2 = col2.empty()
        placeholder2.image(input_image, use_container_width=True)

        placeholder3 = col3.empty()
        # placeholder3.image(input_image, use_container_width=True)

        # shelf_heights, shelf_widths = get_shelf_settings(curr_path, store_shelf_mappings)
        shelf_setting = STORE_MAPPER_WITHOUT_EXCEPTIONS.get_shelf_settings(curr_path)
        logger.info(f"Shelf Setting: {shelf_setting}")
        if shelf_setting is None:
            shelf_setting = WESTERN_AFTER_MAY_15

        file_placeholder.write(f"Store[{shelf_setting.asset_name}]: {curr_path}")
        response = analyze(curr_path, shelf_heights=shelf_setting.shelf_heights, shelf_widths=shelf_setting.shelf_widths)
        status_code = response.status_code
        response = response.json()

        # if "shelf_image" in response:
        #     shelf_image = response["shelf_image"]
        #     shelf_image = decode_image(shelf_image)
        #     if shelf_image is not None:
        #         placeholder1.image(shelf_image, use_container_width=True)

        if "grid_image" in response:
            grid_image = response["grid_image"]
            grid_image = decode_image(grid_image)
            if grid_image is not None:
                placeholder1.image(grid_image, use_container_width=True)

        if "image" in response:
            image = response["image"]
            image = decode_image(image)
            if image is not None:
                placeholder2.image(image, use_container_width=True)

        col3.write(f"Status Code: {status_code}")
        col3.markdown("---")

        if "chiller" in response:
            chiller = response["chiller"]
            for shelf in chiller:
                if "label" in shelf:
                    col3.write(shelf["label"])
                if "bottles" in shelf:
                    shelf_string = ""
                    for b, bottle in enumerate(shelf["bottles"]):
                        if "pred_score" not in bottle:
                            bottle["pred_score"] = "NA"
                        if "volume" not in bottle:
                            bottle["volume"] = "NA"
                        if "brand" not in bottle:
                            bottle["brand"] = "NA"
                        # st.write(bottle["brand"], bottle["volume"], bottle["pred_score"])
                        shelf_string += f"{bottle['brand']}-{bottle['volume']} "
                        if b < len(shelf["bottles"]) - 1:
                            shelf_string += ", "
                    col3.write(shelf_string)
                col3.markdown("---")

            col3.write(chiller)
            col3.markdown("---")

