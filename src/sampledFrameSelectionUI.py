import streamlit as st
import requests
import base64
from datetime import datetime, timedelta
from io import BytesIO
from PIL import Image
from utils import decode_image


# App title and description
st.title("Best Image Finder")
st.subheader("View the best image for a given MAC ID and date range")

# Define the API endpoint URL - change this to your actual API URL
API_BASE_URL = "http://localhost:9001"  # Change to your actual API URL

# Input form
with st.form("image_request_form"):
    default_mac_id = 'AC1518E88084'
    # MAC ID input
    mac_id = st.text_input("MAC ID", help="Enter the MAC ID to query", value=default_mac_id)
    
    # Date range selection
    col1, col2 = st.columns(2)
    with col1:
        # Default from_date is 7 days ago
        # default_from = (datetime.now() - timedelta(days=7)).strftime('%Y-%m-%d')
        default_from = '2025-02-01'
        from_date = st.date_input("From Date", 
                                value=datetime.strptime(default_from, '%Y-%m-%d'),
                                help="Select start date for the search")
    
    with col2:
        # Default to_date is today
        default_to = '2025-02-02'
        to_date = st.date_input("To Date", 
                              value=datetime.strptime(default_to, '%Y-%m-%d'),
                              help="Select end date for the search")
    
    # Submit button
    submit_button = st.form_submit_button("Get Best Image")

# Handle form submission
if submit_button:
    # Show loading spinner
    with st.spinner("Fetching the best image..."):
        try:
            # Format dates to string
            from_date_str = from_date.strftime('%Y-%m-%d')
            to_date_str = to_date.strftime('%Y-%m-%d')
            
            # Prepare API URL with query parameters
            api_url = f"{API_BASE_URL}/get_best_image_for_mac_id"
            params = {
                "mac_id": mac_id,
                "from_date": from_date_str,
                "to_date": to_date_str
            }
            
            # Make the request
            response = requests.get(api_url, params=params)
            
            # Check for successful response
            if response.status_code == 200:
                data = response.json()
                
                if data.get('image') is None:
                    st.warning("No image found for the specified criteria.")
                else:
                    image = decode_image(data['image'])
                    image = Image.fromarray(image)
                    st.image(image, caption=f"Best image for MAC ID: {mac_id}", use_container_width=True)
            
            elif response.status_code == 404:
                st.error(f"Not found: {response.json().get('detail', 'No image found')}")
            else:
                st.error(f"Error: {response.status_code} - {response.text}")
                
        except requests.exceptions.RequestException as e:
            st.error(f"Failed to connect to the server: {e}")
        except Exception as e:
            st.error(f"An error occurred: {e}")

# Add additional information
st.sidebar.header("About")
st.sidebar.info(
    "This app queries the best image for a given MAC ID "
    "within a specified date range. The image is retrieved "
    "from the API and displayed here."
)