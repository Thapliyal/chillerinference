from PIL import Image
from typing import Union
import numpy as np
import io
import base64
from loguru import logger
import cv2
from pathlib import Path
from PIL import Image, ImageOps
from collections import Counter
from detectron2.structures import BoxMode
from tqdm import tqdm
import json
from io import BytesIO
import matplotlib.pyplot as plt
from time import time
from loguru import logger
from datetime import datetime, timedelta
import os
from functools import partial
from concurrent.futures import ThreadPoolExecutor, as_completed


def encode_image(image: Union[np.ndarray, Image.Image]) -> str:
    if image is None:
        return ""
    if isinstance(image, np.ndarray):
        image = Image.fromarray(image)
    elif not isinstance(image, Image.Image):
        logger.error(f"Invalid image type: {type(image)}")
        raise ValueError("Invalid image type")
    bytes = io.BytesIO()
    image = image.convert('RGB')
    image.save(bytes, format='PNG')
    bytes = bytes.getvalue()
    data = "data:image/png;base64," + base64.encodebytes(bytes).decode(encoding='ascii')
    return data

def is_data_uri(data: str) -> bool:
    return data.startswith("data:image/png;base64,") or data.startswith("data:image/jpeg;base64,")


def decode_image(data: str, as_numpy=True) -> Union[np.ndarray, Image.Image]:
    if len(data) == 0:
        return None
    data = data.split(",")[1]
    data = base64.b64decode(data)
    image = Image.open(io.BytesIO(data)).convert('RGB')
    if as_numpy:
        image = np.array(image)
    return image

def pad_pil(image, size=224):
    w, h = image.size
    if max(w, h) > size:
        if w > h:
            new_w = size
            new_h = int(h * (new_w / w))
        else:
            new_h = size
            new_w = int(w * (new_h / h))
        image = image.resize((new_w, new_h), Image.LANCZOS)
    
    # Calculate padding to center the image
    left = (size - image.width) // 2
    top = (size - image.height) // 2
    right = size - image.width - left
    bottom = size - image.height - top

    # Pad the image
    image = ImageOps.expand(image, (left, top, right, bottom), fill='black')
    return image

def pad_numpy(image, size=224):
    image = Image.fromarray(image)
    image = pad_pil(image, size)
    return np.array(image)

def load_image(image_or_path: Union[str, Image.Image, np.ndarray, Path], bgr=True, apply_clahe=False) -> np.ndarray:
    if isinstance(image_or_path, str) or isinstance(image_or_path, Path):
        image = cv2.imread(str(image_or_path))
        if not bgr:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    elif isinstance(image_or_path, np.ndarray):
        image = image_or_path
    elif isinstance(image_or_path, Image.Image):
        image = np.array(Image.open(image_or_path))
        if bgr:
            # PIL image is in RGB format
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    else:
        raise ValueError(f"image_or_path should be a path or a numpy array, found: {type(image_or_path)}")
    
    if apply_clahe:
        image = clahe(image)
    return image

class UnableToOpenFile(Exception):
    pass

def get_video_frames(input_video):
    input_video_path = Path(input_video)
    if not input_video_path.is_file():
        logger.error(f"Unable to open: {input_video}")
        raise UnableToOpenFile(f"Unable to open: {input_video_path.name}")
    
    cap = cv2.VideoCapture(str(input_video))
    if not cap.isOpened():
        logger.error(f"Unable to open: {input_video}")
        raise UnableToOpenFile(f"Unable to open: {input_video_path.name}")

    video_frames = []
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        video_frames.append(frame)
    cap.release()

    if len(video_frames) == 0:
        logger.error(f"No frames found in: {input_video}")
        raise UnableToOpenFile(f"No frames found in: {input_video_path.name}")
    
    height, width = video_frames[0].shape[:-1]
    if height != 1600:
        video_frames = [cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE) for frame in video_frames]
    return video_frames


def clahe(rgb_image):
    if isinstance(rgb_image, Image.Image):
        is_pil = True
        rgb_image = np.array(rgb_image)
    else:
        is_pil = False

    gridsize = 4
    lab = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2LAB)
    lab_planes = list(cv2.split(lab))
    clahe = cv2.createCLAHE(clipLimit=0.6,tileGridSize=(gridsize,gridsize))
    lab_planes[0] = clahe.apply(lab_planes[0])
    lab = cv2.merge(lab_planes)
    rgb = cv2.cvtColor(lab, cv2.COLOR_LAB2RGB)

    if is_pil:
        rgb = Image.fromarray(rgb)
    return rgb

def sharpen(image):
    image = cv2.GaussianBlur(image, (0, 0), 3)
    cv2.addWeighted(image, 1.5, image, -0.5, 0, image)

def get_blur_value(image):
    if isinstance(image, str) or isinstance(image, Path):
        image = cv2.imread(str(image))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    elif isinstance(image, Image.Image):
        image = np.array(image)
    elif isinstance(image, np.ndarray):
        pass
    else:
        raise ValueError(f"Invalid image type: {type(image)}")
    gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    return cv2.Laplacian(gray, cv2.CV_64F).var()        

def get_contour_from_mask(mask, convex=True):
    mask = np.array(mask).astype(np.uint8)
    contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    if len(contours) == 0:
        return None
        
    # Get the largest contour based on area
    largest_contour = max(contours, key=cv2.contourArea)
    largest_contour = largest_contour.squeeze(axis=1)
    # logger.info(f"largest_contour: {largest_contour.tolist()}")
    if convex:
        largest_contour = cv2.convexHull(largest_contour)
        largest_contour = largest_contour.squeeze(axis=1)
    # logger.info(f"largest_contour: {largest_contour.tolist()}")
    return largest_contour.tolist()


def get_sub_image_from_points(image, points, debug=False):
    if isinstance(image, Image.Image):
        image = np.array(image)
    points = [[max(0, p1), max(0, p2)] for (p1, p2) in points]
    if isinstance(points, list):
        p = np.array(points).astype(int)
    elif isinstance(points, np.ndarray):
        p = points.astype(int)
    else:
        raise Exception(f"Invalid type for points: {type(points)} points: {points}")
    x_min, x_max, y_min, y_max = p[:, 0].min(), p[:, 0].max(), p[:, 1].min(), p[:, 1].max()
    height, width = image.shape[:2]
    logger.debug(f"bbox: [{y_min}:{y_max}:{height}][{x_min}:{x_max}:{width}]  image shape: {image.shape}")
    # logger.debug(f"points: {p.tolist()}")
    mask = np.zeros_like(image)
    mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
    mask = cv2.drawContours(mask, np.array([p]), 0, color=(255), thickness=-1)
    bottle = cv2.bitwise_and(image, image, mask=mask)
    bottle_roi = bottle[y_min: y_max, x_min: x_max]
    if debug:
        logger.opt(colors=True).debug(f"<red>bottle_roi: {bottle_roi.shape}</red>")
    # bottle_roi = pad_numpy(bottle_roi)
    return bottle_roi


def get_number_of_lines(jsonls):
    count = 0
    for jsonl in jsonls:
        with open(jsonl, 'r') as file:
            lines = file.readlines()
            count += len(lines)
    return count


class ProdigyLoader:
    def __init__(self, collapsed_label=None, replace_mapping=None, rel_path_prefix=None, new_abs_path_prefix=None, add_segmentation=True, skip_missing=False, allow_negative_labels=False):
        self.collapsed_label = collapsed_label
        self.replace_mapping = replace_mapping
        self.rel_path_prefix = rel_path_prefix
        self.new_abs_path_prefix = new_abs_path_prefix
        self.add_segmentation = add_segmentation
        self.skip_missing = skip_missing
        self.allow_negative_labels = allow_negative_labels
        # self.lens_corrector = LensCorrector()

    def load(self, jsonl_path):
        jsonl_path = Path(jsonl_path)
        if jsonl_path.is_dir():
            json_dicts = self._read_jsonl_dir(jsonl_path)
        else:
            json_dicts = self._read_jsonl(jsonl_path)

        json_dicts = self._replace_labels(json_dicts)
        labels = self._get_labels(json_dicts)
        labels = sorted(list(set(labels)))
        return json_dicts, labels
    
    def load_files(self, jsonl_paths):
        json_dicts = []
        labels = []
        for jsonl_path in jsonl_paths:
            jd, l = self.load(jsonl_path)
            json_dicts.extend(jd)
            labels.extend(l)
        labels = sorted(list(set(labels)))
        return json_dicts, labels
    
    def yield_dicts(self, jsonl_paths):
        for jsonl_path in jsonl_paths:
            with open(jsonl_path, 'r') as file:
                for i, line in enumerate(file):
                    try:
                        line = json.loads(line)
                        line = self._replace_labels(line)
                        yield line
                    except json.JSONDecodeError as e:
                        logger.error(f"{jsonl_path.name}: line[{i}]: json decode failure ... skipping")
                        continue

    def _read_jsonl(self, json_path):
        with open(json_path, 'r') as file:
            lines = file.readlines()
            return [json.loads(line) for line in lines]
        
    def _read_jsonl_dir(self, json_dir):
        jsonl_files = list(json_dir.glob('*.jsonl'))
        json_dicts = []
        for jsonl_file in jsonl_files:
            json_dicts.extend(self._read_jsonl(jsonl_file))
        return json_dicts
    
    def _find_mapped_label(self, label):
        for k, v in self.replace_mapping.items():
            if k == label:
                return v
        return label
    
    def _replace_mapping_for_json_dict(self, json_dict):
        if self.replace_mapping is None:
            return json_dict
        for span in json_dict['spans']:
            span['label'] = self._find_mapped_label(span['label'])
        return json_dict
    
    def _replace_mapping(self, json_dicts):
        if self.replace_mapping is None:
            return json_dicts

        logger.info(f'Label replacement mapping: ')
        for k, v in self.replace_mapping.items():
            logger.info(f'Replacing [{k}] with [{v}]')

        for json_d in json_dicts:
            if self.allow_negative_labels:
                if 'spans' not in json_d:
                    json_d['spans'] = []
            if 'spans' not in json_d:
                continue
            json_d = self._replace_mapping_for_json_dict(json_d)

        return json_dicts
    
    def _replace_labels(self, json_dicts):
        json_dicts = self._replace_mapping(json_dicts)
        if self.collapsed_label is None:
            return json_dicts
        logger.info(f"Collapsing all labels into: {self.collapsed_label}")
        for json_d in json_dicts:
            if 'spans' not in json_d:
                continue
            for span in json_d['spans']:
                span['label'] = self.collapsed_label
        return json_dicts

    def _get_labels(self, json_dicts):
        # labels = sorted(list(set([span['label'] for json_d in json_dicts for span in json_d['spans'] if 'spans' in json_d])))
        labels = []
        for json_d in json_dicts:
            if 'spans' not in json_d:
                # logger.warning(f'No spans found in json: {json_d}')
                continue
            for span in json_d['spans']:
                if 'label' not in span:
                    # logger.warning(f'No label found in span: {span}')
                    continue
                labels.append(span['label'])

        return sorted(list(set(labels)))

    def _fix_point(self, point):
        point = int(point) if point > 0 else 0
        return point
        
    def get_dataset_dicts(self, json_dicts, labels=None):
        dataset_dicts = []
        skipped = 0
        image_id = 0
        if labels is None:
            labels = self._get_labels(json_dicts)
        label2id = {label: i for i, label in enumerate(labels)}

        replaced_indicator = False
        c = Counter()

        for i, line in enumerate(tqdm(json_dicts)):
            if "answer" in line and line["answer"] != "accept":
                skipped += 1
                continue
            record = {}
            if "text" in line:
                record["image_id"] = line["text"]
            record["height"] = line["height"]
            record["width"] = line["width"]

            if 'image' in line:
                del line['image']

            if self.rel_path_prefix and self.new_abs_path_prefix:
                p = line['path']
                if self.rel_path_prefix in p:
                    index = p.find(self.rel_path_prefix)
                    p = p[index:]
                new_path = Path(p.replace(self.rel_path_prefix, self.new_abs_path_prefix))
                if self.skip_missing:
                    if not new_path.is_file():
                        logger.warning(f'File not found: [{new_path}]')
                        skipped += 1
                        continue
                else:
                    assert new_path.is_file(), f'File not found: [{new_path}]'
                record["file_name"] = str(new_path)
                image = cv2.imread(str(new_path))
                height, width = image.shape[0: 2]
                
                line['path'] = str(new_path)
                if not replaced_indicator:
                    logger.info(f'Replaced [{p}] with path: [{line["path"]}]')
                replaced_indicator = True
            else:
                new_path = Path(line["path"])
                assert new_path.is_file(), f'File not found: [{new_path}]'
                record["file_name"] = str(new_path)
                image = cv2.imread(str(new_path))
                height, width = image.shape[0: 2]
            assert height == record["height"], f'Height mismatch: {new_path}: [{height}] != [{record["height"]}]'
            assert width == record["width"], f'Width mismatch: {new_path}: [{width}] != [{record["width"]}]'
            record['image_id'] = image_id
            image_id += 1

            objects = []
            if "spans" in line:
                for span in line["spans"]:
                    orig_points = span["points"]
                    if len(orig_points) == 0 or not isinstance(orig_points[0], list):
                        logger.warning(f'Invalid points: {orig_points} file: {record["file_name"]}')
                        continue
                    points = [[self._fix_point(x + 0.5), self._fix_point(y + 0.5)] for x, y in orig_points]
                    if len(points) <= 3:
                        continue
                    x_points = [self._fix_point(x) for x, y in orig_points]
                    y_points = [self._fix_point(y) for x, y in orig_points]
                    bbox = [np.min(x_points), np.min(y_points), np.max(x_points), np.max(y_points)]
                    if self.add_segmentation:
                        flattened_points = [coordinate for point in points for coordinate in point]
                        # print(f"flattened points: {flattened_points}")
                        obj = {
                            "bbox": bbox,
                            "bbox_mode": BoxMode.XYXY_ABS,
                            "segmentation": [flattened_points],
                            "category_id": label2id[span['label']],
                        }
                    else:
                        obj = {
                            "bbox": bbox,
                            "bbox_mode": BoxMode.XYXY_ABS,
                            "category_id": label2id[span['label']],
                        }
                    c.update([span['label']])
                    objects.append(obj)
            else:
                if not self.allow_negative_labels:
                    logger.warning(f'No spans found in line: [{i+1}]')
                    skipped += 1
                    continue
            record["annotations"] = objects
            dataset_dicts.append(record)
        logger.info(f'Skipped [{skipped}] records, added [{len(dataset_dicts)}] records. Total records: [{len(json_dicts)}]')
        logger.info(f"Counts: {c.most_common()}")

        return dataset_dicts
    
    def get_keypoint_dataset_dicts(self, json_dicts, labels):
        dataset_dicts = self.get_dataset_dicts(json_dicts, labels)
        filtered = []
        for d in tqdm(dataset_dicts):
            anno = d['annotations']
            points = anno[0]['segmentation'][0]
            del anno[0]['segmentation']
            keypoints = []
            
            for i in range(0, len(points), 2):
                x = points[i]
                y = points[i+1]
                keypoints.extend([x, y, 2])
            
            if len(keypoints) != 12 * 3:
                logger.warning(f'Invalid number of keypoints: {len(keypoints)} for file: {d["file_name"]}')
                continue

            anno[0]['keypoints'] = keypoints
            anno[0]['bbox_mode'] = int(anno[0]['bbox_mode'])
            anno[0]['bbox'] = [int(p) for p in anno[0]['bbox']]
            d['annotations'] = anno
            filtered.append(d)
        return filtered
    
    def find_matching_shelf_dict(self, shelf_jsonls, search_path):
        for d in self.yield_dicts(shelf_jsonls):
            path = d["path"]
            if path == search_path:
                return d
        return None

    
def get_frames(input_video, rgb=False):
    cap = cv2.VideoCapture(str(input_video))
    if not cap.isOpened():
        logger.error(f"Unable to open: {input_video}")
        return []

    frames = []
    while True:
        ret, frame = cap.read()
        if not ret:
            break
        frames.append(frame)
    cap.release()

    if len(frames) == 0:
        return []
    
    height, width = frames[0].shape[:-1]
    if height != 1600:
        frames = [cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE) for frame in frames]

    if rgb:
        frames = [cv2.cvtColor(frame, cv2.COLOR_BGR2RGB) for frame in frames]
    return frames


def get_grid_image(rgb_image):
    # Create a matplotlib figure
    fig, ax = plt.subplots()
    ax.imshow(rgb_image)
    
    # Enable grid with lighter style
    ax.grid(True, linestyle='--', alpha=0.5)  # Light grid with transparency
    
    # Set axis labels with smaller font size
    # ax.set_xlabel('X axis', fontsize=10)
    # ax.set_ylabel('Y axis', fontsize=10)

    # Reduce border thickness
    ax.spines['top'].set_linewidth(0.5)
    ax.spines['right'].set_linewidth(0.5)
    ax.spines['left'].set_linewidth(0.5)
    ax.spines['bottom'].set_linewidth(0.5)

    # Optional: remove axes for a cleaner look
    # ax.set_axis_off()

    # Save the figure to a buffer
    buf = BytesIO()
    fig.savefig(buf, format="png", bbox_inches='tight', pad_inches=0.05)  # Further reduce padding
    buf.seek(0)

    # Convert buffer to PIL Image and then to numpy array
    image = Image.open(buf)
    return np.array(image)


class Timer:
    def __init__(self, name=None):
        self.name = name
        self.start_time = None
        self.end_time = None
        self.time = 0

    def reset(self):
        self.start_time = None
        self.end_time = None
        self.time = 0

    def __enter__(self):
        self.start()
    
    def start(self):
        self.start_time = time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()
    
    def stop(self, suffix=None):
        if self.start_time:
            self.stop_time = time()
            self.time = self.stop_time - self.start_time
            hours, mins, secs = 0, 0, 0

            remaining = self.time
            if remaining > 3600:
                hours = remaining // 3600
                remaining = remaining % 3600
            if remaining > 0:
                mins = remaining // 60
                remaining = remaining % 60
            if remaining > 0:
                secs = remaining
            if suffix:
                logger.opt(colors=True).info(f'<yellow>Timer[{self.name}]: {suffix}: {self.time:2.2f} seconds [{hours:3.0f} hrs {mins:3.0f} mins {secs:3.0f} secs]</yellow>')
            else:
                logger.opt(colors=True).info(f'<yellow>Timer[{self.name}]: {self.time:2.2f} seconds [{hours:3.0f} hrs {mins:3.0f} mins {secs:3.0f} secs]</yellow>')


def async_timer(func):
    async def inner(*args, **kwargs):
        with Timer(func.__qualname__) as t:
            return await func(*args, **kwargs)
    return inner


def timer(func):
    def inner(*args, **kwargs):
        with Timer(func.__qualname__) as t:
            return func(*args, **kwargs)
    return inner


def get_recent_files_fast(folder_path, days=15, ext=None):
    cutoff_date = datetime.now() - timedelta(days=days)
    recent_files = []

    def scan_dir(path):
        with os.scandir(path) as entries:
            for entry in entries:
                if entry.is_file():
                    if ext and Path(entry).suffix not in ext:
                        continue
                    file_mtime = datetime.fromtimestamp(entry.stat().st_mtime)
                    if file_mtime >= cutoff_date:
                        recent_files.append(entry.path)
                elif entry.is_dir():
                    scan_dir(entry.path)  # Recursively scan subdirectories

    scan_dir(folder_path)

    recent_files = [Path(f) for f in recent_files]
    return recent_files


def update_dict_paths(prodigy_dict, path_prefix, path_prefix_replacement):
    if prodigy_dict is None:
        return prodigy_dict
    if path_prefix is None or path_prefix_replacement is None:
        return prodigy_dict
    path = str(prodigy_dict['path'])
    if path_prefix in path:
        path = path.replace(path_prefix, path_prefix_replacement)
        prodigy_dict['path'] = path
    if 'meta' in prodigy_dict:
        prodigy_dict['meta']['path'] = path
        prodigy_dict['meta']['file'] = Path(path).name
    return prodigy_dict


def _file_in_date_range(file, start_date, end_date):
    if start_date and not isinstance(start_date, datetime):
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
    if end_date and not isinstance(end_date, datetime):
        end_date = datetime.strptime(end_date, '%Y-%m-%d')
        
    try:
        mod_time = datetime.fromtimestamp(file.stat().st_mtime)
        if start_date is not None and end_date is not None:
            return start_date <= mod_time <= end_date
        elif start_date is not None:
            return mod_time >= start_date
        elif end_date is not None:
            return mod_time <= end_date
        else:
            return True
    except Exception:
        return False
    
def _files_in_date_range(files, start_date, end_date):
    return [file for file in files if _file_in_date_range(file, start_date, end_date) and file.suffix.lower() in [".mp4", ".avi"]]

def _get_files_from_dir(dir_path):
    # Generator that yields files one by one
    for file in dir_path.rglob("*"):
        if file.is_file():
            yield file

def _get_batch_files_from_dir(dir_path, batch_size):
    # Generator that yields files batch by batch
    batch = []
    i = 0
    for i, file in enumerate(dir_path.rglob("*")):
        if not file.is_file():
            continue
        batch.append(file)
        if len(batch) % batch_size == 0:
            yield batch
            batch = []
    if len(batch) > 0:
        yield batch

def get_filtered_files(file_dir, start_date, end_date, batch_size=10000, max_workers=None):
    """
    Find files modified within a date range using parallel processing.
    
    Args:
        file_dir: Path to directory to search
        start_date: Minimum file modification date to include
        end_date: Maximum file modification date to include
        batch_size: Number of files to process in each batch (smaller batches may be more memory efficient)
        max_workers: Number of parallel workers (None uses default based on CPU count)
    
    Returns:
        List of files within the date range
    """
    t = Timer("get_filtered_files")
    t.start()
    logger.info(f"Scanning [{file_dir}] for files modified between [{start_date}] and [{end_date}]")
    results = []
    files_in_date_range = partial(_files_in_date_range, start_date=start_date, end_date=end_date)
    
    # Use CPU count if max_workers not specified
    if max_workers is None:
        max_workers = min(32, os.cpu_count() + 4)
        
    with ThreadPoolExecutor(max_workers=max_workers) as executor:
        current_batch = []
        futures = []
        
        processed_files = 0
        for batch_i, batch in enumerate(_get_batch_files_from_dir(file_dir, batch_size)):
            future = executor.submit(files_in_date_range, batch)
            futures.append(future)
            processed_files += len(batch)
            logger.info(f"Scanned [{processed_files}] files, submitted [{len(futures)}] batches")
            
        # Collect results with optional progress tracking
        total_futures = len(futures)
        for i, future in enumerate(as_completed(futures)):
            batch_results = future.result()
            results.extend(batch_results)
            # Optional progress for large result sets
            if (i+1) % 100 == 0:
                logger.info(f"Filtered [{len(results)}/{processed_files}] files, submitted [{i+1}/{total_futures}] batches")
        logger.info(f"Filtered [{len(results)}/{processed_files}] files, submitted [{len(futures)}] batches")

    t.stop()
    return results


# Alternative implementation for very large directories that uses os.walk directly
def get_filtered_files_walk(file_dir, start_date, end_date, max_workers=None):
    """Alternative implementation using os.walk which may be faster for very large directories"""
    matching_files = []
    file_dir = Path(file_dir)
    
    if max_workers is None:
        max_workers = min(32, os.cpu_count() + 4)
    
    def process_dir(dir_path):
        dir_matches = []
        try:
            for root, _, files in os.walk(dir_path):
                for filename in files:
                    file_path = Path(root) / filename
                    if _file_in_date_range(file_path, start_date, end_date):
                        dir_matches.append(file_path)
        except Exception as e:
            logger.error(f"Error processing directory {dir_path}: {e}")
        return dir_matches
    
    # Process top-level directories in parallel
    with ThreadPoolExecutor(max_workers=max_workers) as executor:
        futures = []
        try:
            # Submit each top-level directory as a separate task
            for item in os.scandir(file_dir):
                if item.is_dir():
                    futures.append(executor.submit(process_dir, item.path))
                elif item.is_file():
                    # Handle top-level files directly
                    file_path = Path(item.path)
                    if _file_in_date_range(file_path, start_date, end_date):
                        matching_files.append(file_path)
        except Exception as e:
            logger.error(f"Error scanning directory {file_dir}: {e}")
            
        # Collect results
        for future in as_completed(futures):
            try:
                matching_files.extend(future.result())
            except Exception as e:
                logger.error(f"Error processing future: {e}")
                
    return matching_files


def get_samples_from_row(row, images_per_video, categories, debug=False):
    video_path = Path(row['file'])
    if not video_path.is_file():
        raise Exception(f"Unable to find: {video_path}")
    frame_prediction_indexes = row['category']
    frame_predictions_str = "[" + ",".join([f"{c}" for c in row['category']]) + "]"
    if debug:
        logger.info(f"{video_path}: frame_predictions: {frame_predictions_str}")
    frame_conf = row['confidence']
    frame_prediction_indexes = [i for i, p in enumerate(frame_prediction_indexes) if p in categories]
    if debug:
        logger.info(f"fpi: {frame_prediction_indexes}")
    conf = [frame_conf[i] for i in frame_prediction_indexes]
    conf_string = "[" + ",".join([f"{c:.2f}" for c in conf]) + "]"
    if debug:
        logger.info(f"conf: {conf_string}")
    conf_i = [i for i, c in enumerate(conf)]
    sorted_conf_i = sorted(conf_i, key=lambda x: conf[x], reverse=True)
    if debug:
        logger.info(f"sorted conf_i: {sorted_conf_i}")
    frame_prediction_indexes = [frame_prediction_indexes[i] for i in sorted_conf_i]
    if debug:
        logger.info(f"sorted fpi: {frame_prediction_indexes}")

    sampled_indexes = frame_prediction_indexes[:images_per_video]
    return sampled_indexes

