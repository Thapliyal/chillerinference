#!/usr/bin/env python

from PIL import Image
import numpy as np
import torch
from pathlib import Path
import cv2
import pandas as pd
from ast import literal_eval
from loguru import logger
from matplotlib import pyplot as plt
from datetime import datetime
from tqdm.auto import tqdm
from argparse import ArgumentParser
import sys
import configparser

from shelfSettings import StoreManager
from classificationInference import VideoClassification, ClassificationPredictor, StatusErrors, VideoDecodeError
from utils import UnableToOpenFile, get_video_frames
from shelfPredictor import ShelfPredictor, PredictedShelf, PredictedChiller
from perShelfPredictor import PerShelfChillerPredictor
from analyzeVideos import VideoManager, TOP_3_CLEAR, ALL_SKUS_CLEAR, OPEN_DOOR_CATEGORIES, VIDEO_SAMPLING_CATEGORIES, ALL_VIDEO_CATEGORIES, ALL_FRAMESELECTOR_CATEGORIES
from pydantic import BaseModel, model_validator
from functools import partial
from typing import List, Optional

STORE_MAPPER = StoreManager()

def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument('-s', '--sample-size', help='Number of samples per store', required=False, default=2, type=int)
    parser.add_argument('-i', '--images-per-video', help='Number of images per video', required=False, default=3, type=int)
    parser.add_argument('-f', '--from-date', help='From date for the sample', required=False, default='2024-07-01')
    parser.add_argument('-r', '--region-filter', help='Region Filter - A list of regions to filter', required=False, default=None, nargs='+')
    parser.add_argument('-m', '--mode', help='Extraction Mode', required=False, choices=['accuracy', 'images', 'bottles', 'vimages', 'videos'], default='images')
    parser.add_argument('-t', '--to-date', help='From date for the sample', required=False, default=None)
    parser.add_argument('-o', '--output-dir', help='Directory to save images', required=False, default='/tmp/extractedBottleImages')
    parser.add_argument('-T', '--test', help="Test mode. Only show stats, don't creat images", action='store_true', default=False)
    parser.add_argument('-S', '--site-asset-names', help="Site asset names", required=False, default=None, nargs='+')
    parser.add_argument('-c', '--categories', help=f"The categories to consider. Choose from: {ALL_FRAMESELECTOR_CATEGORIES}", required=False, nargs='+', default=None)
    parser.add_argument('-v', '--video-status-categories', help=f"The video status categories to consider. Choose from: {ALL_VIDEO_CATEGORIES}", required=False, nargs='+', default=None)
    parser.add_argument('-R', '--remove-restocking', help="Remove restocking videos", action='store_true', default=False)
    parser.add_argument('-C', '--create-dirs', help="Create these empty directories (only valid in vimages)", nargs='+', default=None)
    parser.add_argument('-d', '--debug', help="Debug mode", action='store_true', default=False)
    parser.add_argument('-k', '--save-image-threshold', help="Threshold for saving images under sub folders for brand", type=float, default=None)
    return parser.parse_args()

def get_chiller_predictor():
    cfg = configparser.ConfigParser()
    cfg.read('inference.ini')
    shelf_model_path = Path(cfg['DEFAULT']['shelf_model_path'])
    bottle_model_path = Path(cfg['DEFAULT']['bottle_model_path'])
    brand_models_dir = Path(cfg['DEFAULT']['brand_models_dir'])
    volume_model_path = Path(cfg['DEFAULT']['volume_model_path'])
    bottle_model_threshold = float(cfg['DEFAULT']['bottle_model_threshold'])
    
    predictor = PerShelfChillerPredictor(shelf_model_path=shelf_model_path, bottle_model_path=bottle_model_path, 
                                         brand_models_dir=brand_models_dir, volume_model_path=volume_model_path, 
                                         shelf_clip_index=5, bottle_model_threshold=bottle_model_threshold, debug=False)
    return predictor

class VideoPredictor:
    def __init__(self, chiller_predictor: PerShelfChillerPredictor, video_manager: VideoManager, debug = False, save_image_threshold=0.8):
        self.chiller_predictor = chiller_predictor
        self.video_manager = video_manager
        self.debug = debug
        self.save_image_threshold = save_image_threshold

    @staticmethod
    def get_video_frames(input_video, indexes=None):
        # frames = VideoPredictor._get_video_frames(input_video)
        frames = get_video_frames(input_video)
        if indexes is None:
            return frames
        
        return [frames[i] for i in indexes]

    @staticmethod
    def _get_video_frames(input_video):
        input_video_path = Path(input_video)
        if not input_video_path.is_file():
            logger.error(f"Unable to open: {input_video}")
            raise UnableToOpenFile(f"Unable to open: {input_video_path.name}")
        
        cap = cv2.VideoCapture(str(input_video))
        if not cap.isOpened():
            logger.error(f"Unable to open: {input_video}")
            raise UnableToOpenFile(f"Unable to open: {input_video_path.name}")

        video_frames = []
        while True:
            ret, frame = cap.read()
            if not ret:
                break
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            video_frames.append(frame)
        cap.release()

        if len(video_frames) == 0:
            logger.error(f"No frames found in: {input_video}")
            raise UnableToOpenFile(f"No frames found in: {input_video_path.name}")
        
        height, width = video_frames[0].shape[:-1]
        if height != 1600:
            video_frames = [cv2.rotate(frame, cv2.ROTATE_90_CLOCKWISE) for frame in video_frames]
        return video_frames

    @staticmethod
    def _load_image(video_path, index):
        images = VideoPredictor.get_video_frames(video_path, [index])
        image = images[0]
        return Image.fromarray(image)

    @staticmethod
    def get_sampled_images_from_row(row, images_per_video=1, categories=None, debug=False):
        # if categories is None:
        #     categories = ['top3Clear']
        video_path = Path(row['file'])
        if not video_path.is_file():
            raise Exception(f"Unable to find: {video_path}")
        frame_prediction_indexes = row['category']
        frame_predictions_str = "[" + ",".join([f"{c}" for c in row['category']]) + "]"
        if debug:
            logger.info(f"{video_path}: frame_predictions: {frame_predictions_str}")
        frame_conf = row['confidence']
        frame_prediction_indexes = [i for i, p in enumerate(frame_prediction_indexes) if p in categories]
        if debug:
            logger.info(f"fpi: {frame_prediction_indexes}")
        conf = [frame_conf[i] for i in frame_prediction_indexes]
        conf_string = "[" + ",".join([f"{c:.2f}" for c in conf]) + "]"
        if debug:
            logger.info(f"conf: {conf_string}")
        conf_i = [i for i, c in enumerate(conf)]
        sorted_conf_i = sorted(conf_i, key=lambda x: conf[x], reverse=True)
        if debug:
            logger.info(f"sorted conf_i: {sorted_conf_i}")
        frame_prediction_indexes = [frame_prediction_indexes[i] for i in sorted_conf_i]
        if debug:
            logger.info(f"sorted fpi: {frame_prediction_indexes}")
        # frame_prediction_indexes = sorted(frame_prediction_indexes, key=lambda x: conf[x], reverse=True)
        # logger.info(f"sorted fpi: {frame_prediction_indexes}")
        # middle = int(len(frame_predictions) // 2)
        # index = frame_predictions[middle]
        # image = self._load_image(video_path, index)

        images = []
        sampled_indexes = frame_prediction_indexes[:images_per_video]
        for index in sampled_indexes:
            image = VideoPredictor._load_image(video_path, index)
            images.append(image)
        logger.info(f"video_path: {video_path} num actual images: {len(images)} num expected images: {images_per_video}")
        logger.info(f"-"*50)
        return images, sampled_indexes, video_path

    def get_chillers(self, row, images_per_video=1):
        chillers = []
        frame_indexes = []

        images, sampled_indexes, video_path = self.get_sampled_images_from_row(row, images_per_video=images_per_video, categories=[TOP_3_CLEAR], debug=self.debug)
        shelf_settings = STORE_MAPPER.get_shelf_settings(video_path)
        for iteration, (index, image) in enumerate(zip(sampled_indexes, images)):
            logger.info(shelf_settings)
            image_path = "/tmp/imageForPrediction.jpg"
            image.save(image_path)
            chiller = self.chiller_predictor.predict(image_path, shelf_heights=shelf_settings.shelf_heights, shelf_widths=shelf_settings.shelf_widths)
            chillers.append(chiller)
            frame_indexes.append(index)
            banner = "-"*20
            logger.opt(colors=True).debug(f"<yellow> {banner} Chiller[{iteration}/{len(images) - 1}] sampled index: {index} {banner}</yellow>")
        assert len(chillers) == len(frame_indexes)
        return chillers, frame_indexes, video_path

    def save_bottle_sub_images(self, row, output_dir, images_per_video):
        chillers, frame_indexes, video_path = self.get_chillers(row, images_per_video=images_per_video)
        chiller: PredictedChiller
        num_images = 0
        for iteration, (chiller, frame_index) in enumerate(zip(chillers, frame_indexes)):
            if not chiller:
                logger.error(f"Unable to predict chiller for: {row['file']}")
                continue
            video_path = Path(row['file'])
            mac_id, timestamp, seq = video_path.stem.split('_')
            _, asset_name = STORE_MAPPER.get_store_names_from_path(video_path)
            store_dir = output_dir/f"{asset_name}_{mac_id}"
            if not store_dir.is_dir():
                store_dir.mkdir()
            prefix = f"{timestamp}_{seq}-f{frame_index:02d}"
            sub_images_saved = chiller.save_bottle_sub_images(output_dir=store_dir, prefix=prefix, threshold=self.save_image_threshold)   
            num_images += sub_images_saved
            banner = "_"*15
            logger.opt(colors=True).debug(f"<yellow> {banner} {video_path.name}:{video_path.parent.parent.name}: curr_sub_images[{sub_images_saved}]: sub_images_for_store[{num_images}]: Sample[{iteration}/{len(chillers) - 1}] Frame[{frame_index}] {banner}</yellow>")
        return num_images

    @staticmethod
    def generate_images(output_dir, dataframe, images_per_video, categories):
        images_saved = 0
        for index, row in tqdm(dataframe.iterrows()):
            images, sampled_indexes, video_path = VideoPredictor.get_sampled_images_from_row(row, images_per_video=images_per_video, categories=categories)
            mac_id, timestamp, seq = video_path.stem.split('_')
            _, asset_name = STORE_MAPPER.get_store_names_from_path(video_path)
            for i, image in enumerate(images):
                file_path = output_dir/f"{asset_name}_{timestamp}-f{sampled_indexes[i]:02d}.jpg"
                image.save(file_path)
                images_saved += 1
        return images_saved
                    

    def generate_sub_images(self, store_index, total_stores, output_dir, dataframe, images_per_video):
        total_images = 0
        for i, (index, row) in tqdm(enumerate(dataframe.iterrows())):
            num_images = self.save_bottle_sub_images(row, output_dir, images_per_video)
            total_images += num_images
            logger.opt(colors=True).debug(f"<yellow>store index[{store_index:02d}/{total_stores-1:02d}]: sample[{i+1:02d}/{len(dataframe):02d}]: index: {index}: sub_images: [{num_images}] Total sub_images: {total_images} </yellow>")
        return total_images
    
class CatNode(BaseModel):
    indexes: List[int] = []
    confidence: List[float]
    max_conf: Optional[float] = -1

    @model_validator(mode='after')
    def compute_derived_params(cls, self):
        self.max_conf = max([self.confidence[i] for i in self.indexes])
        return self

def get_cat_indexes(allowed_categories, x):
    indexes = []
    category = x['category']
    confidence = x['confidence']
    if category is None or confidence is None:
        return None

    if allowed_categories is None:
        return CatNode(indexes=list(range(len(category))), confidence=confidence)
    
    for i, (cat, conf) in enumerate(zip(category, confidence)):
        if cat in allowed_categories:
            indexes.append(i)
            
    # confs = [confidence[i] for i in indexes]
    return CatNode(indexes=indexes, confidence=confidence)

def update_group_frame_for_accuracy(frame, categories):
    frame = frame.copy()
    fn = partial(get_cat_indexes, allowed_categories=categories)
    frame['cat_node'] = frame.apply(lambda row: fn(x=row), axis=1)
    frame['sort_key'] = frame.cat_node.apply(lambda x: x.max_conf)
    frame = frame.sort_values(by="sort_key", ascending=False)
    return frame    

def get_filtered_frame(video_manager: VideoManager, from_date, to_date, site_asset_names=None, region_filter=None, sample_size=None, 
                       images_per_video=None, categories=None, remove_restocking=False, video_status_categories=None, 
                       sort_for_accuracy=False, debug=False):
    filtered = video_manager.get_filtered_frame(from_date=from_date, to_date=to_date, site_asset_names=site_asset_names, 
                                                region_filter=region_filter, remove_restocking=remove_restocking, 
                                                video_status_categories=video_status_categories, allowed_categories=categories)
    grouped = filtered.groupby('store_asset_name')
    keys = grouped.groups.keys()
    total = 0
    num_stores = 0
    total_with_sampling = 0

    image_count = 0
    for k in keys:
        num_stores += 1
        group = grouped.get_group(k)
        length = len(group)
        logger.info(f"{k:4}: {length} videos")    
        total += length
        if sample_size:
            curr_sample_size = min(sample_size, length)
        else:
            curr_sample_size = length
        total_with_sampling += curr_sample_size
        
        for index, row in group.sample(curr_sample_size, random_state=42).iterrows():
            # logger.info(f"{index:4}: {row['file']}")
            curr_categories = row['category']
            if curr_categories is not None:
                curr_images = len(curr_categories)
            else:
                video_path = Path(row['file'])
                try:
                    frames = VideoPredictor.get_video_frames(video_path, indexes=None)
                    curr_images = len(frames)
                except UnableToOpenFile as e:
                    logger.error(f"Unable to open file: {video_path}")
                    curr_images = 0
                    continue
                # video = row['file']
                # logger.info(f"{video}: curr_images: {curr_images}")
            if categories:
                curr_categories = [c for c in curr_categories if c in categories]
                curr_images = min(len(curr_categories), images_per_video)
            image_count += curr_images
        
    # logger.info(f"image_count: {image_count}")
    
    to_date_str = f"to: {to_date}" if to_date else ""
    logger.opt(colors=True).info(f"<green>Total videos found: {total} sampled_total: {total_with_sampling} from: {from_date} {to_date_str} Total stores: {num_stores}</green>")

    if sort_for_accuracy:
        pass
    else:
        if images_per_video:
            # logger.opt(colors=True).info(f"<green>Expected Images: {images_per_video * total_with_sampling} actual: {image_count}</green>")
            logger.opt(colors=True).info(f"<green>Expected Sampled Images: {image_count}</green>")
    
    return filtered

def extract(video_manager, top3, output_dir, sample_size, images_per_video, save_image_threshold):
    chiller_predictor = get_chiller_predictor()
    video_predictor = VideoPredictor(chiller_predictor, video_manager, save_image_threshold=save_image_threshold)

    grouped = top3.groupby('store_asset_name')
    keys = grouped.groups.keys()
    num_stores = 0
    total_images = 0
    num_keys = len(keys)
    for i, k in enumerate(keys):
        num_stores += 1
        group = grouped.get_group(k)
        length = len(group)
        curr_sample_size = sample_size
        if length < sample_size:
            logger.info(f"{k:4}: found [{length}] videos, reducing sample size to [{length}]")
            curr_sample_size = length
        group = group.sample(curr_sample_size, random_state=42)
        num_sub_images = video_predictor.generate_sub_images(i, num_keys, output_dir, group, images_per_video)
        total_images += num_sub_images
        logger.opt(colors=True).info(f"<yellow>[{i:02d}/{num_keys:02d}]: {k:4}: </yellow> found [{length}] videos, sampling [{len(group)}] videos. Generated num_sub_images: {num_sub_images}")
        logger.info(f"-"*120)
    logger.opt(colors=True).info(f"<yellow>Total images generated: {total_images}</yellow>")

def extract_images(filtered_frame, output_dir, sample_size, images_per_video, categories):
    grouped = filtered_frame.groupby('store_asset_name')
    keys = grouped.groups.keys()
    total_images_generated = 0
    num_stores = 0
    for k in keys:
        num_stores += 1
        group = grouped.get_group(k)
        length = len(group)
        curr_sample_size = sample_size
        if length < sample_size:
            curr_sample_size = length
            logger.info(f"{k:4}: found only [{length}] videos, reducing sample size to [{length}]")
        group = group.sample(curr_sample_size, random_state=42)
        curr_images_generated = VideoPredictor.generate_images(output_dir, group, images_per_video, categories=categories)
        total_images_generated += curr_images_generated
        logger.info(f"{k:4}: found [{length}] videos, sampled [{len(group)}] videos, generated [{curr_images_generated}] images")
        logger.info(f"-"*120)
    logger.opt(colors=True).info(f"<yellow>Total images generated: {total_images_generated}</yellow>")

def extract_video_images(filtered_frame, sample_size, output_dir, empty_sub_dirs=None, categories=None):
    grouped = filtered_frame.groupby('store_asset_name')
    keys = grouped.groups.keys()
    num_stores = 0
    num_images = 0
    for k in keys:
        num_stores += 1
        group = grouped.get_group(k)
        length = len(group)
        curr_sample_size = sample_size
        if length < sample_size:
            curr_sample_size = length
            logger.info(f"{k:4}: found only [{length}] videos, reducing sample size to [{length}]")
        group = group.sample(curr_sample_size, random_state=42)
        logger.info(f"{k:4}: found [{length}] videos, sampling [{len(group)}] videos")
        for index, row in tqdm(group.iterrows()):
            video_path = Path(row['file'])
            video_categories = row['category']
            # print(f"video_path: {video_path}")
            mac_id = video_path.stem.split('_')[0]
            _, asset_name = STORE_MAPPER.get_store_names_from_path(video_path)
            store_dir = output_dir/f"{asset_name}_{mac_id}"
            if not store_dir.is_dir():
                store_dir.mkdir()
            video_dir = store_dir / f"{video_path.stem}"
            if not video_dir.is_dir():
                video_dir.mkdir()
            if empty_sub_dirs:
                for sub_dir in empty_sub_dirs:
                    sub_dir_path = video_dir / sub_dir
                    if not sub_dir_path.is_dir():
                        sub_dir_path.mkdir()
            video_images = VideoPredictor.get_video_frames(video_path, None)
            for i, (image, category) in enumerate(zip(video_images, video_categories)):
                if categories:
                    if category not in categories:
                        continue
                file_path = video_dir / f"{video_path.stem}_{i:02d}.jpg"
                image = Image.fromarray(image)
                image.save(file_path)
                num_images += 1
    logger.opt(colors=True).info(f"<yellow>Total images generated: {num_images}</yellow>")    

def extract_topn(filtered_frame, sample_size, images_per_video, output_dir, categories):
    grouped = filtered_frame.groupby('store_asset_name')
    keys = grouped.groups.keys()
    num_stores = 0
    num_images = 0
    for k in keys:
        num_stores += 1
        group = grouped.get_group(k)
        group = update_group_frame_for_accuracy(group, categories)
        length = len(group)
        curr_sample_size = sample_size
        if length < sample_size:
            curr_sample_size = length
            logger.info(f"{k:4}: found only [{length}] videos, reducing sample size to [{length}]")

        group = group.head(curr_sample_size)
        
        logger.info(f"{k:4}: found [{length}] videos, sampling [{len(group)}] videos")
        max_confs = group.cat_node.apply(lambda x: x.max_conf).to_list()
        logger.info(f"max_confs: {max_confs}")

        for _, row in tqdm(group.iterrows()):
            video_path = Path(row['file'])
            if row['cat_node'] is None:
                logger.error(f"cat_node is None for: {video_path} ... skipping")
                continue
            
            indexes = row['cat_node'].indexes
            # confidences = [row['cat_node'].confidence[i] for i in indexes]
            confidences = row['cat_node'].confidence
            # logger.debug(f"confidences: {confidences}")
            # logger.debug(f"indexes: {indexes}")
            sorted_indexes = sorted(indexes, key=lambda index: confidences[index], reverse=True)
            # logger.debug(f"sorted_indexes: {sorted_indexes}")
            sorted_confidences = [row['cat_node'].confidence[i] for i in sorted_indexes][:images_per_video]
            # logger.debug(f"sorted_confidences: {sorted_confidences}")

            max_conf = row['cat_node'].max_conf
            # trunc_path_str = "....." + "/".join(str(video_path).split('/')[-3:])
            trunc_path_str = video_path.name
            logger.info(f"{trunc_path_str}: sampled indexes: {sorted_indexes} max_conf: {max_conf} confidences: {sorted_confidences}")
            frame_categories = [row['category'][i] for i in sorted_indexes]
            # print(f"video_path: {video_path}")
            mac_id = video_path.stem.split('_')[0]
            _, asset_name = STORE_MAPPER.get_store_names_from_path(video_path)
            store_dir = output_dir/f"{asset_name}_{mac_id}"
            if not store_dir.is_dir():
                store_dir.mkdir()
            video_images = VideoPredictor.get_video_frames(video_path, sorted_indexes)
            for i, (index, image, category) in enumerate(zip(sorted_indexes, video_images, frame_categories)):
                # TODO: update this to pick up this data from the cat_node field
                if categories:
                    if category not in categories:
                        continue
                file_path = store_dir / f"{video_path.stem}_{index:02d}.jpg"
                image = Image.fromarray(image)
                image.save(file_path)
                num_images += 1
                if i >= images_per_video - 1:
                    break
    logger.opt(colors=True).info(f"<yellow>Total images generated: {num_images}</yellow>")    


def extract_videos(filtered_frame, sample_size, output_dir, empty_sub_dirs=None):
    grouped = filtered_frame.groupby('store_asset_name')
    keys = grouped.groups.keys()
    num_stores = 0
    num_videos = 0
    for k in keys:
        num_stores += 1
        group = grouped.get_group(k)
        length = len(group)
        curr_sample_size = sample_size
        if length < sample_size:
            curr_sample_size = length
            logger.info(f"{k:4}: found only [{length}] videos, reducing sample size to [{length}]")
        group = group.sample(curr_sample_size, random_state=42)
        logger.info(f"{k:4}: found [{length}] videos, sampling [{len(group)}] videos")
        for index, row in tqdm(group.iterrows()):
            video_path = Path(row['file'])
            # print(f"video_path: {video_path}")
            mac_id = video_path.stem.split('_')[0]
            _, asset_name = STORE_MAPPER.get_store_names_from_path(video_path)
            output_video_path = output_dir/f"{asset_name}_{mac_id}_{video_path.name}"
            shutil.copy(video_path, output_video_path)
            num_videos += 1
    logger.opt(colors=True).info(f"<yellow>Total videos generated: {num_videos}</yellow>")    


def main():
    args = parse_arguments()
    test_mode = args.test
    from_date = args.from_date
    to_date = args.to_date
    sample_size = args.sample_size
    images_per_video = args.images_per_video
    categories = args.categories
    video_status_categories = args.video_status_categories
    if args.mode == 'vimages':
        # sample_size = 100000
        # images_per_video = 100000
        if video_status_categories is None:
            video_status_categories = VIDEO_SAMPLING_CATEGORIES
            categories = None
    else:
        if args.mode != 'images' and categories is None:
            categories = [ALL_SKUS_CLEAR, TOP_3_CLEAR]
            video_status_categories = [ALL_SKUS_CLEAR, TOP_3_CLEAR]
        
    site_asset_names = args.site_asset_names
    output_dir = Path(args.output_dir)
    if not output_dir.is_dir():
        output_dir.mkdir()
        
    logger.opt(colors=True).info(f"<green> Sample size: {sample_size} images per video: {images_per_video} from date: {from_date} to date: {to_date} output dir: {args.output_dir} test mode: {test_mode} remove_restocking: {args.remove_restocking}</green>")
    logger.opt(colors=True).info(f"<green> mode: {args.mode} categories to filter: {categories} video_status_categories: {args.video_status_categories}</green>")
    video_manager = VideoManager()

    filtered_frame = get_filtered_frame(video_manager, from_date=from_date, to_date=to_date, site_asset_names=site_asset_names, 
                                        region_filter=args.region_filter, sample_size=sample_size, 
                                        images_per_video=images_per_video, categories=categories, 
                                        remove_restocking=args.remove_restocking, video_status_categories=video_status_categories,
                                        debug=args.debug)

    if not test_mode:
        logger.info(f"-"*120)
        if args.mode == 'images':
            extract_images(filtered_frame, output_dir, sample_size, images_per_video=images_per_video, categories=categories)
        elif args.mode == 'bottles':
            extract(video_manager, filtered_frame, output_dir, sample_size, images_per_video=images_per_video, save_image_threshold=args.save_image_threshold)
        elif args.mode == 'vimages':
            extract_video_images(filtered_frame, sample_size, output_dir, args.create_dirs, categories=categories)
        elif args.mode == 'accuracy':
            extract_topn(filtered_frame, sample_size, images_per_video, output_dir, categories=categories)
        else:
            logger.error(f"Invalid mode: {args.mode}")

if __name__ == '__main__':
    main()
