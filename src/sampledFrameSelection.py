from pathlib import Path
from videoSampler import get_filtered_frame
from analyzeVideos import VideoManager
from shelfSettings import StoreManager
from utils import get_video_frames
from fastapi import FastAPI, Form, HTTPException
from utils import encode_image
from datetime import datetime
from loguru import logger


app = FastAPI()
STORE_MANAGER = StoreManager(raise_exception=False)
video_manager = VideoManager()


def add_frame_index_and_explode(df):
    def add_frame_index(row):
        n = len(row['category'])  # Assuming 'category' list length equals number of frames
        row['frame_index'] = list(range(n))
        return row
    
    # Apply the function to add a frame_index column
    df = df.apply(add_frame_index, axis=1)
    # Explode the list columns (and the new frame_index column)
    df_exploded = df.explode(['category', 'confidence', 'frame_index'])
    sorted_frame = df_exploded.sort_values(by='confidence', ascending=False)
    return sorted_frame

def get_per_video_frame(from_date, to_date, site_asset_names):
    remove_restocking = False
    video_status_categories = ['allSKUsClear', 'top3Clear']
    # here we are setting categories to same as video_status_categories (get everything)
    acceptable_categories = video_status_categories
    frame = None
    region_filter = None
    for category in acceptable_categories:
        frame = video_manager.get_filtered_frame(from_date=from_date, to_date=to_date, site_asset_names=site_asset_names, 
                                                        region_filter=region_filter, remove_restocking=remove_restocking, 
                                                        video_status_categories=video_status_categories, allowed_categories=video_status_categories)
        if len(frame) > 0:
            return frame
    return frame

def get_exploded_frame(from_date, to_date, site_asset_names):
    acceptable_categories = ['allSKUsClear', 'top3Clear']
    THRESHOLD_CONF = 0.7
    
    filtered_frame = get_per_video_frame(from_date, to_date, site_asset_names)
    filtered_frame = add_frame_index_and_explode(filtered_frame)

    # Check with correct threshold
    preferred = None
    for category in acceptable_categories:
        preferred = filtered_frame[filtered_frame.category == category]
        preferred = preferred[preferred.confidence > THRESHOLD_CONF]
        if len(preferred) > 0:
            break
    
    if preferred is None or len(preferred) == 0:
        preferred = filtered_frame[filtered_frame.category.isin(acceptable_categories)]

    preferred = preferred.sort_values(by='confidence', ascending=False)
    return preferred
    
def get_sampled_frame(asset_name, from_date, to_date):
    site_asset_names = [asset_name]
    frame = get_exploded_frame(from_date, to_date, site_asset_names)
    return frame

def get_best_image(asset_name, from_date, to_date):
    frame = get_sampled_frame(asset_name, from_date, to_date)
    if frame is None or len(frame) == 0:
        return None
    file_path = frame.iloc[0]['file']
    if file_path is None or not Path(file_path).exists():
        raise FileNotFoundError(f"File not found for asset name: {asset_name} file: {file_path}")
    frame_index = frame.iloc[0]['frame_index']
    video_frames = get_video_frames(file_path)
    image = video_frames[frame_index]
    return image


@app.get("/get_best_image_for_mac_id")
async def select_frame(mac_id: str, from_date: str, to_date: str):
    if to_date >= datetime.now().strftime('%Y-%m-%d'):
        video_manager.reload_frame()
    asset_name = STORE_MANAGER.get_asset_name_from_mac_id(mac_id)
    try:
        image = get_best_image(asset_name, from_date, to_date)
    except FileNotFoundError as e:
        raise HTTPException(status_code=404, detail=f"{str(e)}")
    if image is None:
        return {
            'image': None
        }
    result = {
        'image': encode_image(image)
    }
    return result


@app.get("/get_best_image_for_asset_name")
async def select_frame(asset_name: str, from_date: str, to_date: str):
    if to_date >= datetime.now().strftime('%Y-%m-%d'):
        video_manager.reload_frame()
    try:    
        image = get_best_image(asset_name, from_date, to_date)

    except FileNotFoundError as e:
        raise HTTPException(status_code=404, detail=f"{asset_name}: {str(e)}")
    
    if image is None:
        return {
            'image': None
        }
    result = {
        'image': encode_image(image)
    }
    return result

